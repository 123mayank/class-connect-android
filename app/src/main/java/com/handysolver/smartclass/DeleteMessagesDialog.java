package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.services.AutoLogout;

import java.util.ArrayList;

public class DeleteMessagesDialog extends AppCompatActivity {
    int selected = -1;
    SharedPreferences sharedPreferences;
    ArrayAdapter<Integer> select_numbers_adapter;
    private Boolean press_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DatabaseHelper helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
        setTitle("Delete Messages");
        setContentView(R.layout.activity_delete_messages_dialog);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        final CheckBox delete_all = (CheckBox) findViewById(R.id.delete_all);
        final Spinner select_delete = (Spinner) findViewById(R.id.select_delete);
        long num = helper.getNumRows();
        ArrayList<Integer> ints = new ArrayList<>();
        if (num < 9) {
            for (int i = 1; i <= num; i++) {
                ints.add(i);
            }
        }
        if (num >= 10 && num < 100) {
            for (int i = 1; i <= num; i++) {
                if (i % 5 == 0) {
                    ints.add(i);
                }
            }
        }
        if (num > 100) {
            for (int i = 1; i <= num; i++) {
                if (i % 10 == 0) {
                    ints.add(i);
                }
            }
        }
        select_numbers_adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, ints);
        select_numbers_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_delete.setAdapter(select_numbers_adapter);
        select_delete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!select_delete.isEnabled()) {
                    select_delete.setEnabled(true);
                    select_delete.setClickable(true);
                    delete_all.setEnabled(false);
                    return true;
                }
                return false;
            }
        });
        press_exit = false;
        select_delete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selected = (int) select_delete.getItemAtPosition(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        delete_all.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    select_delete.setEnabled(false);
                    select_delete.setAdapter(select_numbers_adapter);
                } else {
                    select_delete.setEnabled(true);
                    select_delete.setAdapter(select_numbers_adapter);
                }
            }
        });

        Button okay_button = (Button) findViewById(R.id.ok);
        okay_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (delete_all.isChecked()) {
                    helper.deleteAll();
                }
                if (!delete_all.isChecked()) {
                    helper.deleteSelected(selected);
                }
                Intent intent=new Intent(DeleteMessagesDialog.this,Inbox.class);
                startActivity(intent);
            }
        });

        Button cancel_button=(Button)findViewById(R.id.cancel);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
