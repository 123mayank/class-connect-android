package com.handysolver.smartclass;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.handysolver.smartclass.models.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Sanket on 20-10-2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "messages.db";
    public static final String TABLE_NAME = "message";
    public static final String COLUMN_SENDER = "sender";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_TOPIC = "topic";
    public static final String COLUMN_BODY = "body";
    public static final String COLUMN_TYPE = "type";
    SimpleDateFormat db_dateFormat=new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_TOPIC + " TEXT, " + COLUMN_SENDER + " TEXT, " + COLUMN_TIME + " TEXT," + COLUMN_BODY + " TEXT," +COLUMN_TYPE + " TEXT" + ");";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
//        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void addMessage(String topic, String sender, String time, String body, String type)
    {
        String query = "CREATE TABLE IF NOT EXISTS" + TABLE_NAME + "(" + COLUMN_TOPIC + " TEXT, " + COLUMN_SENDER + " TEXT, " + COLUMN_TIME + " TEXT," + COLUMN_BODY + " TEXT," +COLUMN_TYPE + " TEXT" + ");";
        ContentValues values = new ContentValues();
        values.put(COLUMN_TOPIC, topic);
        values.put(COLUMN_SENDER, sender);
        values.put(COLUMN_TYPE, type);
        values.put(COLUMN_BODY, body);
        String[] split_times = time.split(" ");
        try {
            values.put(COLUMN_TIME, db_dateFormat.format(dateFormat.parse(split_times[0]))+" "+split_times[1]+" "+split_times[2]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME,null,values);
        db.close();
    }

    public boolean CheckIsDataAlreadyInDBorNot(String fieldValue, String sender)
    {
        String[] split_times = fieldValue.split(" ");
        boolean time_exists = false;
        boolean sender_exists = false;
        SQLiteDatabase db= getReadableDatabase();
        try {
            fieldValue = db_dateFormat.format(dateFormat.parse(split_times[0]))+" "+split_times[1]+" "+split_times[2];
            Log.d("a",fieldValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String Query = "Select * from " + TABLE_NAME + " where " + COLUMN_TIME +"=\""+fieldValue+"\";" ;
        String Query2 = "Select * from " + TABLE_NAME + " where " + COLUMN_SENDER +"=\""+sender+"\";" ;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0)
        {
            cursor.close();
            time_exists = false;
        }
        else
        {
            cursor.close();
            time_exists = true;
        }
        cursor.close();
        Cursor cursor2 = db.rawQuery(Query2, null);
        if(cursor2.getCount() <= 0)
        {
            cursor2.close();
            sender_exists = false;
        }
        else
        {
            cursor2.close();
            sender_exists = true;
        }
        return sender_exists && time_exists;
    }

    public ArrayList<Message> getTable() {
        ArrayList<Message> res = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE 1 " + "ORDER BY "+COLUMN_TIME+ " ASC";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        try
        {
            while (c.moveToNext())
            {
                String[] split_times = c.getString(2).split(" ");
                Message message = new Message(c.getString(0),c.getString(1),dateFormat.format(db_dateFormat.parse(split_times[0]))+" "+split_times[1]+" "+split_times[2],c.getString(3),c.getString(4));
//                Message message = new Message(c.getString(0),c.getString(1),dateFormat.format(c.getString(2)),c.getString(3),c.getString(4));
                res.add(message);
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        finally
        {
            c.close();
        }

        return res;
    }

    public void dropTable()
    {
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME+";");
    }

    public void deleteAll()
    {
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME+";");
    }

    public void deleteSelected(int i)
    {
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_NAME+ " WHERE "+COLUMN_TIME+ " IN ( SELECT "+COLUMN_TIME+ " FROM "+TABLE_NAME+ " ORDER BY "+COLUMN_TIME+ " ASC LIMIT "+i+")");
    }

    public long getNumRows()
    {
        long num= DatabaseUtils.queryNumEntries(getReadableDatabase(),TABLE_NAME);
        return num;
    }
}
