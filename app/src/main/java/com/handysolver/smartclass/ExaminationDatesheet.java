package com.handysolver.smartclass;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.DateSheet;
import com.handysolver.smartclass.models.TestModel;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExaminationDatesheet extends AppCompatActivity {
    ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
    private ArrayList<TestModel> exams = new ArrayList<>();
    private HashMap<String, Integer> hash_map = new HashMap<>();
//    private ArrayList<String> examNames = new ArrayList<>();
    private ArrayList<String> markedExamNames = new ArrayList<>();
    private HashMap<Integer, String> marks_names = new HashMap<>();
    //    private HashMap<Integer,String> avg_marks_names=new HashMap<>();
//    private HashMap<Integer,String> highest_marks_names=new HashMap<>();
    BarChart marks_chart;
    SwipeRefreshLayout refreshLayout;
    RequestQueue requestQueue;
    float k = 0.0f;
    SharedPreferences sharedPreferences;
    private DateSheet dateSheet;
    private TableLayout tableLayout;
    private ScrollView scrollView;
    private LinearLayout scrollRelative;
    private List<DateSheet> dateSheetList=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examination_datesheet);
        Bundle bundle = getIntent().getExtras();
        final String exam_name = bundle.getString("exam_name");
        final int exam_id = bundle.getInt("id");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(exam_name);
        scrollRelative=(LinearLayout) findViewById(R.id.scroll_relative_date_sheet);
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        requestQueue = Volley.newRequestQueue(this);
        /*refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);*/
        sendRequest(exam_id);
        /*refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sendRequest(exam_id);
            }
        });
        refreshLayout.setRefreshing(true);*/
        Log.d("exam_id","exam id new "+exam_id);
    }
    public void sendRequest(final int exam_id) {
        dateSheetList.clear();
        /*if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }*/
        StringRequest exam_list_request = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.EXAM_TIMETABLE_CONTROLLER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                dateSheet=new DateSheet();
                /*response 1 is the response from server which is first converted to JsonObject which further contains a JSONArray containing the average and number of exams and another JSONArray containing details of individual exams*/
                JSONArray datesheetDetail;
                JSONArray num_exams;
                JSONObject response3;
                try {
                    datesheetDetail = new JSONArray(response1);
                    for (int i = 0; i < datesheetDetail.length(); i++) {
                        JSONObject response = datesheetDetail.getJSONObject(i);
                        if(response.get("date").toString().length()>0){
                            dateSheet.setDate(response.get("date").toString());
                        }
                        if(response.get("day").toString().length()>0){
                            dateSheet.setDay(response.get("day").toString());
                        }
                        if(response.get("subject").toString().length()>0){
                            dateSheet.setSubject(response.get("subject").toString());
                        }
                        if(response.get("from_time").toString().length()>0){
                            dateSheet.setFromTime(response.get("from_time").toString());
                        }
                        if(response.get("to_time").toString().length()>0){
                            dateSheet.setToTime(response.get("to_time").toString());
                        }
                        if(response.get("location").toString().length()>0){
                            dateSheet.setLocation(response.get("location").toString());
                        }
                        setDateSheet(dateSheet);
                        //dateSheetList.add(dateSheet);
                        Log.d("get date","get date sheet data "+dateSheet.getSubject());
                    }
                    //setDateSheet();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*refreshLayout.setRefreshing(false);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /*refreshLayout.setRefreshing(false);*/
                /*String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                try {
                    JSONObject error_json = new JSONObject(error_message);
                    if (error_json.has("status")) {
                        if (error_json.getInt("status") == 444) {
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                        }
                        if (error_json.getInt("status") == 498) {
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                        }
                        if (error_json.getInt("status") == 445) {
                            *//*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*//*
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent = new Intent(ExaminationDetails.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                error.printStackTrace();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = sharedPreferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0) + "&exam_id=" + exam_id;
                    } else {
                        body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null)) + "&exam_id=" + exam_id;
                    }
                }
                return body.getBytes();
            }
        };
        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(exam_list_request);
    }
    private void setDateSheet(DateSheet dateSheet){
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.date_sheet_rows, null);
        if(dateSheet.getDate()!=null){
            TextView textView=(TextView) v.findViewById(R.id.date_view);
            textView.setText(dateSheet.getDate());
        }
        if(dateSheet.getDay()!=null){
            TextView textView=(TextView) v.findViewById(R.id.day_view);
            textView.setText(dateSheet.getDay());
        }
        if(dateSheet.getSubject()!=null){
            TextView textView=(TextView) v.findViewById(R.id.subject_view);
            textView.setText(dateSheet.getSubject());
        }
        if(dateSheet.getFromTime()!=null){
            TextView textView=(TextView) v.findViewById(R.id.from_time_view);
            textView.setText(dateSheet.getFromTime());
        }
        if(dateSheet.getFromTime()!=null){
            TextView textView=(TextView) v.findViewById(R.id.to_time_view);
            textView.setText(dateSheet.getToTime());
        }
        if(dateSheet.getLocation()!=null){
            TextView textView=(TextView) v.findViewById(R.id.location_view);
            textView.setText(dateSheet.getLocation());
        }
        // add view
        scrollRelative.addView(v);

    }
    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
