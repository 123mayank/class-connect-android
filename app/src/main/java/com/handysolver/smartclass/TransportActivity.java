package com.handysolver.smartclass;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.TransportAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Transport;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/*This class handles the examination module, We are populating an array list of type Exam, the class of which can be found in the models folder. Next the list is passed to the ExaminationListAdapter, which takes care of populating the recycler view with the list*/
public class TransportActivity extends AppCompatActivity {
    SharedPreferences preferences;
    SwipeRefreshLayout refreshLayout;
    private ArrayList<Transport> transportList=new ArrayList<>();
    private RecyclerView transportListRecyclerview;
    private TransportAdapter transportAdapter;
    private Boolean press_exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        refreshLayout.setRefreshing(true);
        preferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        transportAdapter=new TransportAdapter(transportList,getApplicationContext());
        transportListRecyclerview=(RecyclerView)findViewById(R.id.transport_recycler_view);
        transportListRecyclerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        transportListRecyclerview.setAdapter(transportAdapter);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sendRequest();
            }
        });
        sendRequest();
    }
    private void sendRequest(){
        transportList.clear();
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest exam_list_request=new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.TRANSPORT_CONTROLLER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                JSONArray response2;
                try{
                    response2=new JSONArray(response1);
                    for(int i=0;i<response2.length();i++)
                    {
                        Transport transport = new Transport();
                        JSONObject response=response2.getJSONObject(i);
                        //get data
                        if(response.get("name").toString().length()>0){
                            transport.setName(response.getString("name"));
                        }
                        if(response.get("number_of_vehicle").toString().length()>0){
                            transport.setNumVehicle(response.getString("number_of_vehicle"));
                        }
                        if(response.get("description").toString().length()>0){
                            transport.setDescription(response.getString("description"));
                        }
                        if(response.get("name").toString().length()>0){
                            transport.setName(response.getString("name"));
                        }
                        if(response.get("route_fare").toString().length()>0){
                            transport.setRouteFare(response.getString("route_fare"));
                        }
                        if(response.get("assigned").toString().length()>0){
                            if(response.getInt("assigned")==0){
                                transport.setAssigned(false);
                            }else{
                                transport.setAssigned(true);
                            }
                        }
                        if(response.get("link").toString().length()>0){
                            transport.setLink(response.getString("link"));
                        }
                        Log.d("getting data","getting all transport data "+response.getString("route_fare"));
                        transportList.add(transport);
                        /*Exam exam=new Exam(response.getString("exam_name"),response.getInt("id"));
                        exam.setStart_date(response.getString("start_date"));
                        exam.setEnd_date(response.getString("end_date"));
                        exams.add(exam);*/
                    }

                   /* RecyclerView examination_list=(RecyclerView)findViewById(R.id.exam_list_recycler_view);
                    examination_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    examination_list.setAdapter(new ExaminationListAdapter(exams,getApplicationContext()));*/
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
                transportAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error",error.toString());
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        })
        {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = preferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + preferences.getInt(GlobalConstants.parent_selected_student, 0);
                    } else {
                        body = "id=" + Integer.valueOf(preferences.getString("id", null));
                    }
                }
                return body.getBytes();
            }
        };
        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(exam_list_request);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

}
