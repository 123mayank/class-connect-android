package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.NoticeAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.NoticeModel;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;

public class NoticeBoardActivity extends AppCompatActivity {

    private ArrayList<NoticeModel> noticeModels = new ArrayList<>();
    SwipeRefreshLayout refreshLayout;
    SharedPreferences sharedPreferences;
    private Boolean press_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_board);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notice Board");
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                noticeModels.clear();
                sendRequest();
            }
        });
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        press_exit = false;
        sendRequest();
    }

    public void sendRequest() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.NOTICEBOARD_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONArray response2 = null;
                        try {
                            response2 = new JSONArray(response1);
                            for (int i = 0; i < response2.length(); i++) {
//                                String marks = "";
//                                String max_marks = "";
//                                int highest_marks = 0;
//                                int average_marks = 0;
                                JSONObject response = response2.getJSONObject(i);
                                String[] links = new String[2];
                                links[0] = "";
                                links[1] = "";
//                                String start_time = response.getString("time_from");
//                                start_time = start_time.substring(0, 5);
//                                String end_time = response.getString("time_to");
//                                end_time = end_time.substring(0, 5);
//                                String exam_time = start_time + "-" + end_time;
                                if (response.getInt("attachment_count") > 0) {
                                    JSONArray attachments = response.getJSONArray("attachment");
                                    for (int j = 0; j < attachments.length(); j++) {
                                        JSONObject attachment = attachments.getJSONObject(j);
                                        String name = attachment.getString("name");
                                        String[] split_name = name.split("\\.");
                                        links[0] = links[0].concat(name + " ");
                                        links[1] = links[1].concat(attachment.getString(split_name[split_name.length - 1]) + " ");
                                    }
                                }
                                noticeModels.add(new NoticeModel(response.getString("notice"), response.getString("title"), response.getString("date"), response.getInt("id"), links));
//                                if (response.has("file_path")) {
//
//                                }
//                                else
//                                {
//                                    noticeModels.add(new NoticeModel(response.getString("notice"), response.getString("title"), response.getString("date"), response.getInt("id"), ""));
//                                }
                            }
                            RecyclerView test_list = (RecyclerView) findViewById(R.id.test_recycler_view);
                            test_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            test_list.setAdapter(new NoticeAdapter(noticeModels, getApplicationContext()));
                            refreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse.data != null) {
                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                            try {
                                JSONObject error_json = new JSONObject(error_message);
                                if (error_json.has("status")) {
                                    if (error_json.getInt("status") == 444) {
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        refreshLayout.setRefreshing(false);
                                    }
                                    if (error_json.getInt("status") == 498) {
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        refreshLayout.setRefreshing(false);
                                    }
                                    if (error_json.getInt("status") == 445) {
                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.apply();
                                        Intent intent = new Intent(NoticeBoardActivity.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            refreshLayout.setRefreshing(false);
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText("Error");
                        }

                        error.printStackTrace();
                    }
                });
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalConstants.should_logout) {
            Intent intent = new Intent(NoticeBoardActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
//    {
//        @Override
//        public byte[] getBody() throws AuthFailureError {
//        SharedPreferences preferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
//        String role = preferences.getString("role", null);
//        String body = "";
//        if (role != null) {
//            if (role.equals(GlobalConstants.parent_role)) {
//                body = "id=" + preferences.getInt(GlobalConstants.parent_selected_student, 0);
//            } else {
//                body = "id=" + Integer.valueOf(getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE).getString("id", null));
//            }
//        }
//        return body.getBytes();
//    }
//    };

}
