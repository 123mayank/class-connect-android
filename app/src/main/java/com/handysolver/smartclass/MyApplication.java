package com.handysolver.smartclass;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.services.AutoLogout;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Sanket on 05-10-2016.
 */
public class MyApplication extends Application {
    DatabaseHelper helper;
    SharedPreferences sharedPreferences;

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    private static boolean activityVisible;


    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getApplicationContext().getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        OneSignal.startInit(this).setNotificationOpenedHandler(new MessageNotificationHandler()).setNotificationReceivedHandler(new MessageNotificationHandler()).init();
        Context context = getApplicationContext();
        Intent intent = new Intent(getApplicationContext(), AutoLogout.class);
        context.startService(intent);
        registerActivityLifecycleCallbacks(AppLifecycleHandler.getInstance());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    /*This method is called when the user opens a push notification. Note that we are getting a string labelled "type" along with the notification data. We are using this string to redirect the user to various activities*/
    class MessageNotificationHandler implements OneSignal.NotificationOpenedHandler, OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            JSONObject data = result.notification.payload.additionalData;
            String type = "";
            try {
                data.length();
                Log.d("response", data.toString());
                type = data.getString("type");
                /*It can be possible that the user received a notification but he was logged out. So we compare to the app's shared preferences strings to decide what to do*/
                if (type.equals(GlobalConstants.attendance_message)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    /*If we have an id in shared preference*/
                    if (sharedPreferences.getString("id", null) != null) {
                        /*If id is there but user didn't select remember me, shared preferences are to be cleared and user redirected to login screen*/
//                        if (!sharedPreferences.getBoolean("should_remember", false)){
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        /*If everything is alright, user is sent to attendance activity as this particular block of code will be executed when the received notification is related to student being marked absent or sick*/
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
//                                Intent i = new Intent(getApplicationContext(), Examination.class);
//                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(i);
                            }
                            Intent i = new Intent(getApplicationContext(), AttendanceTabs.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    }
                    /*No id found, user instantly logged out*/
                    else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
                /*Similar approach is followed below for various notification types which can be viewed in Global Constants class*/
                if (type.equals(GlobalConstants.test_message) || type.equals(GlobalConstants.upcoming_test_message)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
//                                Intent i = new Intent(getApplicationContext(), Examination.class);
//                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(i);
                            }
                            Intent intent = new Intent(getApplicationContext(), Test.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }

                }
                if (type.equals(GlobalConstants.upcoming_holiday_message)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
//                                SharedPreferences.Editor editor = sharedPreferences.edit();
//                                String[] split_titles = title.split("@");
//                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
//                                editor.apply();
                                Intent i = new Intent(getApplicationContext(), Inbox.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.student_role)) {
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("holiday", true);
                                startActivity(intent);
                            }
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }

                }
                if (type.equals(GlobalConstants.notice_message)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
//                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
////                                SharedPreferences.Editor editor = sharedPreferences.edit();
////                                String[] split_titles = title.split("@");
////                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
////                                editor.apply();
//                                Intent i = new Intent(getApplicationContext(), NoticeBoardActivity.class);
//                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(i);
//                            }
//                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.student_role))
//                            {
                            String[] links = new String[2];
                            links[0] = "";
                            links[1] = "";
                            if (data.getInt("attachment_count") > 0) {
                                JSONArray attachments = data.getJSONArray("notice_attachment");
                                for (int j = 0; j < attachments.length(); j++) {
                                    JSONObject attachment = attachments.getJSONObject(j);
                                    String name = attachment.getString("name");
                                    String[] split_name = name.split("\\.");
                                    links[0] = links[0].concat(name + " ");
                                    links[1] = links[1].concat(attachment.getString(split_name[split_name.length - 1]) + " ");
                                }
                            }
                            Intent intent = new Intent(getApplicationContext(), DownloadDialog.class);
                            intent.putExtra("description", data.getString("notice"));
                            intent.putExtra("title", data.getString("notice_title") + "- " + data.getString("notice_date"));
                            String[] split_names = links[0].split(" ");
                            String[] split_links = links[1].split(" ");
                            intent.putExtra("numbers", split_names.length);
                            for (int i = 0; i < split_names.length; i++) {
                                intent.putExtra("name" + i, split_names[i]);
                                intent.putExtra("link" + i, split_links[i]);
                            }
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }

                }
                if (type.equals(GlobalConstants.examination_message) || type.equals(GlobalConstants.upcoming_examination_message)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
                                Intent i = new Intent(getApplicationContext(), Examination.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                            Intent i = new Intent(getApplicationContext(), Examination.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
                if (type.equals(GlobalConstants.assignment_message) || type.equals(GlobalConstants.upcoming_assignment_message)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
                                Intent i = new Intent(getApplicationContext(), Assignment.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                            Intent i = new Intent(getApplicationContext(), Assignment.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
                if (type.equals(GlobalConstants.test_marks)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
                            }
                            if (data.getString("message").contains("marks")) {
                                String[] split_marks_message = data.getString("message").split(" got ");
                                float marks = Float.valueOf(split_marks_message[1].split(" ")[0]);
                                Intent intent = new Intent(getApplicationContext(), ComparisonGraphDialog.class);
                                intent.putExtra("subject_name", data.getString("subject_name"));
                                intent.putExtra("marks", marks);
                                intent.putExtra("average_marks", Float.valueOf(data.getString("class_avg")));
                                intent.putExtra("subject_code", data.getString("code"));
                                intent.putExtra("date", data.getString("date"));
                                intent.putExtra("highest_marks", Float.valueOf(data.getString("highest_mark")));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            else
                            {
                                Intent intent = new Intent(getApplicationContext(), Test.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
                if (type.equals(GlobalConstants.examination_marks)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
                            }
                            if (data.getString("message").contains("marks")) {
                            String[] split_marks_message = data.getString("message").split(" got ");
                            float marks = Float.valueOf(split_marks_message[1].split(" ")[0]);
                            Intent intent = new Intent(getApplicationContext(), ComparisonGraphDialog.class);
                            intent.putExtra("subject_name", data.getString("subject_name"));
                            intent.putExtra("marks", marks);
                            intent.putExtra("average_marks", Float.valueOf(data.getString("class_avg")));
                            intent.putExtra("highest_marks", Float.valueOf(data.getString("highest_mark")));
                            intent.putExtra("subject_code", data.getString("code"));
                            intent.putExtra("date", data.getString("date"));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                            else
                            {
                                Intent intent = new Intent(getApplicationContext(), Examination.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
                if (type.equals(GlobalConstants.assignment_marks)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
                            }
                            if (data.getString("message").contains("marks")) {
                                String[] split_marks_message = data.getString("message").split(" got ");
                                float marks = Float.valueOf(split_marks_message[1].split(" ")[0]);
                                Intent intent = new Intent(getApplicationContext(), ComparisonGraphDialog.class);
                                intent.putExtra("subject_name", data.getString("subject_name"));
                                intent.putExtra("marks", marks);
                                intent.putExtra("average_marks", Float.valueOf(data.getString("class_avg")));
                                intent.putExtra("highest_marks", Float.valueOf(data.getString("highest_mark")));
                                intent.putExtra("subject_code", data.getString("code"));
                                intent.putExtra("date", data.getString("date"));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            else
                            {
                                Intent intent = new Intent(getApplicationContext(), Assignment.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
                if (type.equals(GlobalConstants.timetable_message)) {
                    String title = data.getString("title");
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage(data.getString("title"), data.getString("sender"), data.getString("time"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getString("id", null) != null) {
//                        if (!sharedPreferences.getBoolean("should_remember", false)) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.clear();
//                            editor.apply();
//                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                        }
                        if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                            if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                String[] split_titles = title.split("@");
                                editor.putInt(GlobalConstants.parent_selected_student, Integer.valueOf(split_titles[split_titles.length - 1]));
                                editor.apply();
                                Intent i = new Intent(getApplicationContext(), TimeTableActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                            Intent i = new Intent(getApplicationContext(), TimeTableActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    } else {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
                if (type.equals(GlobalConstants.personal_message_label_string)) {
                    helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
                    helper.addMessage("", data.getString("sender"), data.getString("title"), data.getString("message"), data.getString("type"));
                    if (sharedPreferences.getBoolean("should_remember", false) || AppLifecycleHandler.isApplicationVisible()) {
                        Intent i = new Intent(getApplicationContext(), Inbox.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void notificationReceived(OSNotification notification) {
            if (!AppLifecycleHandler.isApplicationVisible() && !sharedPreferences.getBoolean("should_remember", false)) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
                OneSignal.setSubscription(false);
            }
        }
    }
}
