package com.handysolver.smartclass.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import com.handysolver.smartclass.DatabaseHelper;
import com.handysolver.smartclass.LoginActivity;
import com.handysolver.smartclass.MainActivity;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.onesignal.OneSignal;

public class AutoLogout extends Service {
    DatabaseHelper helper;
    public static CountDownTimer timer;

    public AutoLogout() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
        timer = new CountDownTimer(300 * 1000, 100) {
            public void onTick(long millisUntilFinished) {
                //Some code
                Log.v(""+millisUntilFinished, "Service Started");
            }

            public void onFinish() {
                Log.v("asd", "Call Logout by Service");
                SharedPreferences sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
                if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.apply();
                    if (AppLifecycleHandler.isApplicationVisible()) {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    if (!AppLifecycleHandler.isApplicationVisible()) {
                        GlobalConstants.should_logout = true;
                        OneSignal.setSubscription(false);
                    }
                        // Code for Logout}
                    stopSelf();
                }
            }
        };
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
