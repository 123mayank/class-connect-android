package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CreateMessageDialog extends AppCompatActivity {
    private DatabaseHelper helper;
    int id = -1;
    int grade_id = -1;
    int section_id = -1;
    String recipient_name = null;
    String recipient_group = null;
    String class_name = null;
    SharedPreferences sharedPreferences;
    private ArrayList<Integer> selected_users = new ArrayList<>();
    private ArrayList<String> selected_user_names = new ArrayList<>();
    private Boolean press_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_message_dialog);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final TextView error_view = (TextView) findViewById(R.id.error_view);
        setSupportActionBar(toolbar);
        setTitle("Send Message");
        helper = new DatabaseHelper(this, null, null, 1);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getInt("grade_id") > 0) {
                grade_id = bundle.getInt("grade_id");
                section_id = bundle.getInt("section_id");
                recipient_group = bundle.getString("recipient");
                class_name = bundle.getString("class");
//                getSupportActionBar().setTitle(class_name);
            }
            if (bundle.getIntegerArrayList("selected_users") != null) {
                selected_users = bundle.getIntegerArrayList("selected_users");
                selected_user_names = bundle.getStringArrayList("selected_user_names");
                class_name = bundle.getString("class");
            } else {
                id = bundle.getInt("id");
                recipient_name = bundle.getString("name");
//                getSupportActionBar().setTitle(recipient_name);
            }

            press_exit = false;
        }
//        if(recipient_name !=null)
//        {
//
//        }

        final EditText message_body = (EditText) findViewById(R.id.message_body);
        sharedPreferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        Button okay_button = (Button) findViewById(R.id.ok);
        okay_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message_body.getText().toString().length() > 0) {
                    error_view.setVisibility(View.GONE);
                    if (selected_users.size() == 0) {
                        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL + GlobalConstants.SEND_MESSAGE_CONTROLLER,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response1) {
                                        try {
                                            JSONObject response = new JSONObject(response1);
                                            if (response.getString("status").equals(GlobalConstants.message_status_success)) {
                                                if (recipient_group != null) {
                                                    if (recipient_group.equals(GlobalConstants.student_group)) {
                                                        helper.addMessage(GlobalConstants.personal_message_label_string, "->" + class_name + "(students)", response.getString("time"), message_body.getText().toString(), GlobalConstants.personal_message_label_string);
                                                        Intent intent = new Intent(CreateMessageDialog.this, Inbox.class);
                                                        startActivity(intent);
                                                    }
                                                    if (recipient_group.equals(GlobalConstants.parent_group)) {
                                                        helper.addMessage(GlobalConstants.personal_message_label_string, "->" + class_name + "(parents)", response.getString("time"), message_body.getText().toString(), GlobalConstants.personal_message_label_string);
                                                        Intent intent = new Intent(CreateMessageDialog.this, Inbox.class);
                                                        startActivity(intent);
                                                    }
                                                } else {
                                                    helper.addMessage(GlobalConstants.personal_message_label_string, "->" + recipient_name, response.getString("time"), message_body.getText().toString(), GlobalConstants.personal_message_label_string);
                                                    Intent intent = new Intent(CreateMessageDialog.this, Inbox.class);
                                                    startActivity(intent);
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Log.d("Tag", "onResponse: " + response1);
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        //   Handle Error
                                        error.printStackTrace();
                                        error_view.setText("asd");
                                    }
                                }) {

                            @Override
                            public byte[] getBody() throws AuthFailureError {
                                String body = "";
                                if (recipient_group != null) {
                                    body = "sender_id=" + Integer.valueOf(sharedPreferences.getString("id", null)) + "&receiver_id=" + recipient_group + "&message=" + message_body.getText().toString() + "&grade_id=" + grade_id + "&section_id=" + section_id;
                                } else {
                                    body = "sender_id=" + Integer.valueOf(sharedPreferences.getString("id", null)) + "&receiver_id=" + id + "&message=" + message_body.getText().toString();
                                }
                                return body.getBytes();
                            }
                        };
                        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        Volley.newRequestQueue(getApplicationContext()).add(postRequest);
//                    } else {
//                        error_view.setVisibility(View.VISIBLE);
//                        error_view.setText("Can't send message with no body");
//                    }
                        finish();
                    } else {
                        for (int j = 0; j < selected_users.size(); j++) {
                            final int current_id = selected_users.get(j);
                            final String name = selected_user_names.get(j);
                            StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL + GlobalConstants.SEND_MESSAGE_CONTROLLER,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response1) {
                                            try {
                                                JSONObject response = new JSONObject(response1);
                                                if (response.getString("status").equals(GlobalConstants.message_status_success)) {
                                                    if (recipient_group != null) {
                                                        if (recipient_group.equals(GlobalConstants.student_group)) {
                                                            helper.addMessage(GlobalConstants.personal_message_label_string, "->" + class_name + "(students)", response.getString("time"), message_body.getText().toString(), GlobalConstants.personal_message_label_string);
                                                            Intent intent = new Intent(CreateMessageDialog.this, Inbox.class);
                                                            startActivity(intent);
                                                        }
                                                        if (recipient_group.equals(GlobalConstants.parent_group)) {
                                                            helper.addMessage(GlobalConstants.personal_message_label_string, "->" + class_name + "(parents)", response.getString("time"), message_body.getText().toString(), GlobalConstants.personal_message_label_string);
                                                            Intent intent = new Intent(CreateMessageDialog.this, Inbox.class);
                                                            startActivity(intent);
                                                        }
                                                    } else {
                                                        name.length();
                                                        helper.addMessage(GlobalConstants.personal_message_label_string, "->" + name, response.getString("time"), message_body.getText().toString(), GlobalConstants.personal_message_label_string);
                                                    }
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            Log.d("Tag", "onResponse: " + response1);
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            //   Handle Error
                                            error.printStackTrace();
                                            error_view.setText("asd");
                                        }
                                    }) {

                                @Override
                                public byte[] getBody() throws AuthFailureError {
                                    String body = "";
                                    body = "sender_id=" + Integer.valueOf(sharedPreferences.getString("id", null)) + "&receiver_id=" + current_id + "&message=" + message_body.getText().toString();
                                    return body.getBytes();
                                }
                            };
                            postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            Volley.newRequestQueue(getApplicationContext()).add(postRequest);
                        }
                        Intent intent = new Intent(CreateMessageDialog.this, Inbox.class);
                        startActivity(intent);
                    }
                } else {
                    error_view.setVisibility(View.VISIBLE);
                    error_view.setText("Can't send message with no body");
                }
            }
        });

        Button cancel_button = (Button) findViewById(R.id.cancel);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
