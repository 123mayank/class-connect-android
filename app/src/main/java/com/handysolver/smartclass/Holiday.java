package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.HolidayAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.HolidayModel;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Holiday extends AppCompatActivity {

    RecyclerView holiday_list;
    SwipeRefreshLayout refreshLayout;
    private Boolean press_exit = false;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holiday);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Holidays");
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.GONE);
        holiday_list=(RecyclerView)findViewById(R.id.holiday_recycler);
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresher_layout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /*Note that we have to clear the array lists here else they still contain the classes that were added to them before refreshing*/
                sendRequest();
            }
        });
        sendRequest();
    }

    public void sendRequest() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.HOLIDAY_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        ArrayList<HolidayModel> holidayModels=new ArrayList<>();
                        JSONObject response = null;
                        JSONArray response2=null;
                        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MMM-yyyy");
                        String date=dateFormat.format(Calendar.getInstance().getTime());
                        int k=0;
                        try {
                            response2=new JSONArray(response1);
                            for(int i=0;i<response2.length();i++)
                            {
                                response=response2.getJSONObject(i);
                                String holiday_date=response.getString("date");
                                if(date!=null)
                                {
                                    if(holiday_date.compareTo(date)<1)
                                    {
                                        k++;
                                    }
                                }
                                holidayModels.add(new HolidayModel(response.getString("name"),response.getString("date")));
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                        holiday_list.setAdapter(new HolidayAdapter(getApplicationContext(),holidayModels));
                        holiday_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        refreshLayout.setRefreshing(false);
//                        holiday_list.getLayoutManager().scrollToPosition(k);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        error.printStackTrace();
                    }
                }) {

/*as holidays are same for all, we do not need to send the user id*/
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                String body = "id=" + Integer.valueOf(getActivity().getSharedPreferences(GlobalConstants.user_details_file_name, Context.MODE_PRIVATE).getString("id", null)) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
//                return body.getBytes();
//            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalConstants.should_logout) {
            Intent intent = new Intent(Holiday.this, LoginActivity.class);
            startActivity(intent);
        }
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
