package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.handysolver.smartclass.adapters.TestAdapter;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.TestModel;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExaminationDetails extends AppCompatActivity {
    ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
    private ArrayList<TestModel> exams = new ArrayList<>();
    private HashMap<String, Integer> hash_map = new HashMap<>();
    //    private ArrayList<String> examNames = new ArrayList<>();
    private ArrayList<String> markedExamNames = new ArrayList<>();
    private HashMap<Integer, String> marks_names = new HashMap<>();
    //    private HashMap<Integer,String> avg_marks_names=new HashMap<>();
//    private HashMap<Integer,String> highest_marks_names=new HashMap<>();
    BarChart marks_chart;
    SwipeRefreshLayout refreshLayout;
    RequestQueue requestQueue;
    float k = 0.0f;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        final String exam_name = bundle.getString("exam_name");
        final int exam_id = bundle.getInt("id");
        final int grade_template_id = bundle.getInt("grade_id");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examination_details);
        requestQueue = Volley.newRequestQueue(this);
        marks_chart = (BarChart) findViewById(R.id.marks_chart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(exam_name);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        sendRequest(exam_id,grade_template_id);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                marks_chart.zoom(0, 0, 0, 0);
                marks_names.clear();
//                avg_marks_names.clear();
//                highest_marks_names.clear();
                exams.clear();
                dataSets.clear();
                sendRequest(exam_id,grade_template_id);
            }
        });
        refreshLayout.setRefreshing(true);
//        StringRequest exam_list_request = new StringRequest(Request.Method.POST, "http://dealsinbongo.com/smartclass/api/web/v1/examinations/exam-detail", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response1) {
//                JSONArray response2;
//                try {
//                    response2 = new JSONArray(response1);
//                    for (int i = 0; i < response2.length(); i++) {
//                        JSONObject response = response2.getJSONObject(i);
////                        examNames.add(response.getString("exam_name"));
//                        hash_map.put(response.getString("exam_name"), response.getInt("id"));
//
//                    }
////                    final Spinner spinner = (Spinner) findViewById(R.id.exam_spinner);
////                    ArrayAdapter<String> spinner_adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, examNames);
////                    spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
////                    spinner.setAdapter(spinner_adapter);
////                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
////                        @Override
////                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
////                            String exam_name = spinner.getItemAtPosition(i).toString();
////                            id = hash_map.get(exam_name);
////                            exams.clear();
////                            marks_names.clear();
//////                            avg_marks_names.clear();
//////                            highest_marks_names.clear();
////                            dataSets.clear();
////                            sendRequest(id);
////                        }
////
////                        @Override
////                        public void onNothingSelected(AdapterView<?> adapterView) {
////
////                        }
////                    });
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("error", error.toString());
//                error.printStackTrace();
//            }
//        }) {
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                SharedPreferences preferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
//                String role = preferences.getString("role", null);
//                String body = "";
//                if (role != null) {
//                    if (role.equals(GlobalConstants.parent_role)) {
//                        body = "id=" + preferences.getInt(GlobalConstants.parent_selected_student, 0);
//                    } else {
//                        body = "id=" + Integer.valueOf(getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE).getString("id", null));
//                    }
//                }
//                return body.getBytes();
//            }
//        };
//        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        requestQueue.add(exam_list_request);
    }

    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendRequest(final int exam_id, final int grade_template_id) {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest exam_list_request = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.EXAM_SUBJECT_CONTROLLER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                /*response 1 is the response from server which is first converted to JsonObject which further contains a JSONArray containing the average and number of exams and another JSONArray containing details of individual exams*/
                JSONArray subjectDetails;
                JSONArray num_exams;
                JSONObject response3;
                try {
                    response3 = new JSONObject(response1);
                    subjectDetails = response3.getJSONArray("subjectDetail");
                    for (int i = 0; i < subjectDetails.length(); i++) {
                        JSONObject response = subjectDetails.getJSONObject(i);
//                        String start_time = response.getString("time_from");
//                        start_time = start_time.substring(0, 5);
//                        String end_time = response.getString("time_to");
//                        end_time = end_time.substring(0, 5);
//                        String exam_time = start_time + "-" + end_time;
                        if (response.has("marks") && !response.getString("marks").equals("null")) {
                            String marks1 = response.getString("marks");
                            Log.i("asdeweqw", marks1);
                            final List<BarEntry> entries = new ArrayList<>();
//                            final List<BarEntry> highest_marks_entries = new ArrayList<>();
//                            final List<BarEntry> average_marks_entries = new ArrayList<>();
                            String subject_name = response.getString("examination_subject_name");
                            markedExamNames.add(response.getString("examination_subject_name"));
//                            highest_marks_names.put(response.getInt("highest_mark"), subject_name);
//                            avg_marks_names.put(response.getInt("avg"), subject_name);
                            marks_names.put(response.getInt("marks"), subject_name);
                            int marks = response.getInt("marks");
//                            String max_marks = response.getString("max_marks");
                            entries.add(new BarEntry(k, Float.valueOf(marks)));
//                            highest_marks_entries.add(new BarEntry(k + 3.0f, response.getInt("highest_mark")));
//                            average_marks_entries.add(new BarEntry(k + 0.5f, response.getInt("avg")));
                            BarDataSet marks_set = new BarDataSet(entries, "Your score");
//                            BarDataSet highest_marks_set = new BarDataSet(highest_marks_entries, "Class Highest");
//                            BarDataSet average_marks_set = new BarDataSet(average_marks_entries, "Class Average");
                            marks_set.setColor(Color.parseColor(GlobalConstants.presentColor));
//                            highest_marks_set.setColor(Color.parseColor(GlobalConstants.presentColor));
//                            average_marks_set.setColor(Color.parseColor(GlobalConstants.sickColor));
                            dataSets.add(marks_set);
//                            dataSets.add(highest_marks_set);
//                            dataSets.add(average_marks_set);
                            k += 0.35f;
                            TestModel model = new TestModel(response.getString("room_number"), response.getString("exam_date"), response.getString("from_time") + "-" + response.getString("to_time"), response.getString("examination_subject_name"), marks + "/" + "100",grade_template_id);
                            model.setHighest_marks(response.getInt("highest_mark"));
                            model.setAverage_marks(response.getInt("avg"));
                            model.setComments(response.getString("comments"));
                            model.setGrade(response.getString("grade"));
                            exams.add(model);
                        } else {
                            TestModel model = new TestModel(response.getString("room_number"), response.getString("exam_date"), response.getString("from_time") + "-" + response.getString("to_time"), response.getString("examination_subject_name"), "/",grade_template_id);
                            if (response.has("comments")) {
                                model.setComments(response.getString("comments"));
                            } else {
                                model.setComments("");
                            }
                            if (response.has("grade")) {
                                model.setGrade(response.getString("grade"));
                            } else {
                                model.setGrade("");
                            }
                            exams.add(model);
                        }
                        RecyclerView examination_list = (RecyclerView) findViewById(R.id.exam_recycler_view);
                        examination_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        examination_list.setAdapter(new TestAdapter(getApplicationContext(), exams));
                    }
                    XAxis xAxis = marks_chart.getXAxis();
                    IAxisValueFormatter formatter = new IAxisValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, AxisBase axis) {
                            value = value - 1.8f;
                            if (value >= 0.0 && value % 2.7f == 0.0) {
                                return markedExamNames.get((int) Math.round(value / 2.7f));
                            }
                            return "";
                        }

//                        @Override
//                        public int getDecimalDigits() {
//                            return 0;
//                        }
                    };
                    xAxis.setValueFormatter(formatter);
                    xAxis.setDrawGridLines(false);
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    marks_chart.setDrawGridBackground(false);
                    marks_chart.getAxisLeft().setDrawGridLines(false);
                    marks_chart.getAxisRight().setEnabled(false);
                    BarData barData = new BarData(dataSets);
                    barData.setValueFormatter(new IValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                            String res = marks_names.get(Math.round(value)) + "\n" + value;

//                            if (dataSetIndex % 3 == 1) {
//                                res = marks_names.get(Math.round(value)) + "\n" + value;
////                                    res="asd";
//                            }
//                            if (dataSetIndex % 3 == 2) {
//                                res = marks_names.get(Math.round(value)) + "\n" + value;
//                            }
                            return res;
                        }
                    });
                    barData.setBarWidth(0.2f);
                    marks_chart.setData(barData);
                    marks_chart.setDrawValueAboveBar(true);
//                    if (dataSets.size() > 3)
//                        marks_chart.zoom(2, 0, 0, 0);
//                    else
//                        marks_chart.zoom(0, 0, 0, 0);
//                    marks_chart.zoom(2, 0, 0, 0);
//                    marks_chart.scrollTo((int)k,0);
//                    marks_chart.zoom(2,0,k,0);
                    Description description = new Description();
                    description.setText("");
                    marks_chart.setDescription(description);
                    marks_chart.setDescription(description);
                    marks_chart.invalidate();
                    LegendEntry entry = new LegendEntry();
                    entry.formColor = Color.parseColor(GlobalConstants.presentColor);
                    entry.label = "Your Score - marks in percent";
//                    LegendEntry entry_highest = new LegendEntry();
//                    entry_highest.formColor = Color.parseColor(GlobalConstants.presentColor);
//                    entry_highest.label = "Class Highest";
//                    LegendEntry entry_average = new LegendEntry();
//                    entry_average.formColor = Color.parseColor(GlobalConstants.sickColor);
//                    entry_average.label = "Class Average";
                    ArrayList<LegendEntry> entries = new ArrayList<>();
                    entries.add(entry);
//                    entries.add(entry_highest);
//                    entries.add(entry_average);
                    Legend legend = marks_chart.getLegend();
                    legend.setXEntrySpace(5f);
                    legend.setCustom(entries);
                    if (response3.has("studentData")) {
                        RelativeLayout graph_layout = (RelativeLayout) findViewById(R.id.graph);
                        graph_layout.setVisibility(View.VISIBLE);
                        LinearLayout exam_card = (LinearLayout) findViewById(R.id.exams_number_card);
                        exam_card.setVisibility(View.VISIBLE);
                        num_exams = response3.getJSONArray("studentData");
                        int average = num_exams.getJSONObject(0).getInt("avg");
                        String subjects = num_exams.getJSONObject(0).getString("total_subject");
                        TextView exams = (TextView) findViewById(R.id.num_exams);
                        TextView average_text_view = (TextView) findViewById(R.id.average);
                        average_text_view.setText(average + "");
                        exams.setText(subjects);
                    } else {
                        RelativeLayout graph_layout = (RelativeLayout) findViewById(R.id.graph);
                        graph_layout.setVisibility(View.GONE);
                        LinearLayout exam_card = (LinearLayout) findViewById(R.id.exams_number_card);
                        exam_card.setVisibility(View.GONE);
                    }
                    refreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                try {
                    JSONObject error_json = new JSONObject(error_message);
                    if (error_json.has("status")) {
                        if (error_json.getInt("status") == 444) {
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                        }
                        if (error_json.getInt("status") == 498) {
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                        }
                        if (error_json.getInt("status") == 445) {
                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent = new Intent(ExaminationDetails.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                error.printStackTrace();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = sharedPreferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0) + "&exam_id=" + exam_id;
                    } else {
                        body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null)) + "&exam_id=" + exam_id;
                    }
                }
                return body.getBytes();
            }
        };
        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(exam_list_request);
    }

}
