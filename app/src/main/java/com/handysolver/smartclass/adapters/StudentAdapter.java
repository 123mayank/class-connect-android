package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.handysolver.smartclass.ComparisonGraphDialog;
import com.handysolver.smartclass.DownloadDialog;
import com.handysolver.smartclass.MainActivity;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.AssignmentModel;
import com.handysolver.smartclass.models.StudentModel;

import java.util.ArrayList;

/**
 * Created by Sanket on 16-11-2016.
 */
/*Test and Exam have almost similar views and hence we are using the same TestAdapter for them. Assignment also has a very similar view but we are using a different adapter since we have to show downloadable links in the view as well*/
public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.V> {
    private ArrayList<StudentModel> students = new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;
    private MyClickListener myClickListener;

    public StudentAdapter(ArrayList<StudentModel> students, Context context) {
        this.students = students;
        this.mcontext = context;
        layout = LayoutInflater.from(context);
        this.myClickListener=(StudentAdapter.MyClickListener)context;
    }

    @Override
        /*this inner class defines the various UI elements that are to be populated for a single assignment*/
    public StudentAdapter.V onCreateViewHolder(ViewGroup parent, int viewType) {
        /*Specifying the XML file which contains the layout for each card*/
        View view = layout.inflate(R.layout.student_card, parent, false);
        V viewHolder = new V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final StudentAdapter.V holder, int position) {
        final SharedPreferences preferences = mcontext.getSharedPreferences(GlobalConstants.user_details_file_name, Context.MODE_PRIVATE);
        final StudentModel current = students.get(position);
        holder.name.setText(current.getName());
        holder.grade.setText(current.getGrade());
        if (current.getDisplay_picture() == null) {
            Drawable drawable = mcontext.getResources().getDrawable(R.drawable.sdgf);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            holder.display_picture.setImageBitmap(bitmap);
        } else {
            holder.display_picture.setImageBitmap(current.getDisplay_picture());
        }
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myClickListener.onItemClick(current.getId(),current.getName(), view);

            }
        });
//        if(current.getId()==preferences.getInt(GlobalConstants.parent_selected_student,-1))
//        {
//            holder.cardView.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimary));
//        }
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    static class V extends RecyclerView.ViewHolder  {
        TextView name, grade;
        ImageView display_picture;
        CardView cardView;

        public V(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.guardian_student_name);
            grade = (TextView) itemView.findViewById(R.id.guardian_student_class);
            display_picture = (ImageView) itemView.findViewById(R.id.guardian_student_image);
            cardView = (CardView) itemView.findViewById(R.id.student_card);
        }


    }

//    public void setOnItemClickListener(MyClickListener myClickListener) {
//        this.myClickListener = myClickListener;
//    }

    public interface MyClickListener {
        void onItemClick(int id, String name, View view);
    }
}
