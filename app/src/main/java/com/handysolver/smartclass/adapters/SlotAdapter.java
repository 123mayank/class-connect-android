package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.handysolver.smartclass.R;
import com.handysolver.smartclass.TeacherDetails;
import com.handysolver.smartclass.models.TimeTableSlot;

import java.util.ArrayList;

public class SlotAdapter extends RecyclerView.Adapter<SlotAdapter.V>
{
    private ArrayList<TimeTableSlot>l =new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;
    static class V extends RecyclerView.ViewHolder
    {
        TextView t,b,p;
        View cardView;
        public V(View itemView)
        {
            super(itemView);
            t=(TextView)itemView.findViewById(R.id.subject);
            b=(TextView)itemView.findViewById(R.id.teacher);
            p=(TextView)itemView.findViewById(R.id.time);
            cardView=(View)itemView.findViewById(R.id.card_view);
        }
    }

    public SlotAdapter(Context context,ArrayList<TimeTableSlot>l)
    {
        this.mcontext=context;
        layout=LayoutInflater.from(context);
        this.l=l;
        notifyItemRangeChanged(0,l.size());
    }

    @Override
    public V onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=layout.inflate(R.layout.slot_layout,parent,false);
        V viewHolder=new V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(V holder, int position)
    {
        final TimeTableSlot current =l.get(position);
        holder.t.setText(current.getSubject());
        holder.t.setTextColor(Color.parseColor(current.getFont_color()));
        holder.b.setTextColor(Color.parseColor(current.getFont_color()));
        holder.p.setTextColor(Color.parseColor(current.getFont_color()));
        holder.b.setText(current.getTeacher());
        String[] time=current.getTime().split("-");
        String formatted_time="";
        if(!current.getSubject().equals("No Classes"))
        {
            for (int k = 0; k < time.length; k++) {
                String[] start = time[k].split(":");
                {
                    for (int i = 0; i < start.length; i++) {
                        if (start[i].length() == 1) {
                            start[i] = "0".concat(start[i]);
                        }
                        formatted_time = formatted_time.concat(start[i]);
                        if (i < 1)
                            formatted_time = formatted_time.concat(":");
                    }
                }
                if (k < 1)
                    formatted_time = formatted_time.concat("-");
            }
            if(current.getId().length()>0)
            {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(mcontext,TeacherDetails.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id",Integer.valueOf(current.getId()));
                    intent.putExtra("teacher_name",current.getTeacher());
                    mcontext.startActivity(intent);
                }
            });
        }}
        holder.p.setText(formatted_time);
        holder.cardView.setBackgroundColor(Color.parseColor(current.getBackground_color()));

    }

    @Override
    public int getItemCount()
    {
        return l.size();
    }
}
