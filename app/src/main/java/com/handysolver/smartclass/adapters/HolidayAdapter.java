package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.HolidayModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by hsmm1 on 11/23/16.
 */

public class HolidayAdapter extends RecyclerView.Adapter<HolidayAdapter.View> {

    private ArrayList<HolidayModel>holidayModels=new ArrayList<>();
    private LayoutInflater layoutInflater;
    private Context context;
    @Override
    public View onCreateViewHolder(ViewGroup parent, int viewType) {
        android.view.View view=layoutInflater.inflate(R.layout.holiday_card,parent,false);
        HolidayAdapter.View viewHolder=new HolidayAdapter.View(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(View holder, int position) {
        HolidayModel current=holidayModels.get(position);
        String date_month_year=current.getDate();
        String[]split_date=date_month_year.split("-");
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MMM-yyyy");
        Date date= null;
        try {
            date = dateFormat.parse(dateFormat.format(Calendar.getInstance().getTime()));
            long x = dateFormat.parse(current.getDate()).getTime();
            long y = date.getTime();
            if(x<y)
            {
                Log.i("onBindViewHolder: ",x+" is less than "+y);
                holder.cardView.setBackgroundColor(Color.parseColor(GlobalConstants.subjectColor));
//                holder.month_year.setTextColor(Color.parseColor(GlobalConstants.subjectColor));
//                Log.d(date.toString(),dateFormat.parse(current.getDate()).toString());
//            Log.d("d","diff");
            }
            if(x>=y)
            {
                holder.cardView.setBackgroundColor(context.getResources().getColor(R.color.cardview_light_background));
//                holder.month_year.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.date.setText(split_date[0]);
        holder.month_year.setText(split_date[1]+" "+split_date[2]);
        holder.name.setText(current.getName());


    }

    public HolidayAdapter(Context mcontext, ArrayList<HolidayModel>holidayModels)
    {
        this.context=mcontext;
        layoutInflater=LayoutInflater.from(context);
        this.holidayModels=holidayModels;
        notifyItemRangeChanged(0,holidayModels.size());
    }


    @Override
    public int getItemCount() {
        return holidayModels.size();
    }

    static class View extends RecyclerView.ViewHolder{
        TextView name,date,month_year;
        android.view.View cardView;
        public View(android.view.View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.holiday_name);
            date=(TextView)itemView.findViewById(R.id.test_date);
            month_year=(TextView)itemView.findViewById(R.id.time_year);
            cardView=(android.view.View)itemView.findViewById(R.id.holiday_card);
        }
    }
}
