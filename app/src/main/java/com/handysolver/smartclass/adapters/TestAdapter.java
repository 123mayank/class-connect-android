package com.handysolver.smartclass.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.handysolver.smartclass.ComparisonGraphDialog;
import com.handysolver.smartclass.GradeTemplateActivity;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.TestModel;

import java.util.ArrayList;
/*Used to handle the functionality of the list of cards in Test and Exam view*/
public class TestAdapter extends RecyclerView.Adapter<TestAdapter.V>
{
    private ArrayList<TestModel>tests =new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;
    /*this inner class defines the various UI elements that are to be populated for a single test/exam*/
    static class V extends RecyclerView.ViewHolder
    {
        TextView subject,code,marks,date,max_marks,month_year,time,grade,comments,awaiting_marks;
        View cardView,divider;
        LinearLayout marks_layout;
        public V(View itemView)
        {
            super(itemView);
            divider=(View)itemView.findViewById(R.id.divider);
            subject=(TextView)itemView.findViewById(R.id.subject_name);
            code=(TextView)itemView.findViewById(R.id.subject_code);
            marks=(TextView)itemView.findViewById(R.id.marks);
            max_marks=(TextView)itemView.findViewById(R.id.max_marks);
            date=(TextView)itemView.findViewById(R.id.test_date);
            month_year=(TextView)itemView.findViewById(R.id.time_year);
            time=(TextView)itemView.findViewById(R.id.test_time);
            cardView=(View)itemView.findViewById(R.id.test_card);
            awaiting_marks = (TextView) itemView.findViewById(R.id.not_marked);
            comments = (TextView) itemView.findViewById(R.id.comments);
            grade = (TextView) itemView.findViewById(R.id.grade_text);
            marks_layout=(LinearLayout)itemView.findViewById(R.id.marks_layout);
        }
    }

    public TestAdapter(Context context,ArrayList<TestModel>tests)
    {
        this.mcontext=context;
        layout=LayoutInflater.from(context);
        this.tests=tests;
        notifyItemRangeChanged(0,tests.size());
    }

    @Override
    public V onCreateViewHolder(ViewGroup parent, int viewType)
    {
        /*Specifying the XML file which contains the layout for each card*/
        View view=layout.inflate(R.layout.test_layout,parent,false);
        V viewHolder=new V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(V holder, int position) {
        final TestModel current =tests.get(position);
        holder.subject.setText(current.getSubject_name());
        if(!current.getCode().equals(""))
        holder.code.setText(current.getCode()+", ");
        final String[]split_marks=current.getMarks().split("/");
//        holder.divider.setVisibility(View.GONE);
//        holder.max_marks.setVisibility(View.GONE);
        if(current.getGrade().length()>0)
        {
            holder.comments.setVisibility(View.VISIBLE);
            holder.comments.setText("Grade: "+current.getGrade());
            if(current.getComments().length() == 0)
            {
                SpannableString ss = new SpannableString("Grade: "+current.getGrade()+"\n"+"View grading scheme");
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        Intent intent = new Intent(mcontext, GradeTemplateActivity.class);
                        intent.putExtra("id",current.getGrade_scheme());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mcontext.startActivity(intent);

                    }
                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setFakeBoldText(true);
                        ds.setUnderlineText(true);
                    }
                };
                ss.setSpan(clickableSpan, current.getGrade().length()+7, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.comments.setText(ss);
                holder.comments.setMovementMethod(LinkMovementMethod.getInstance());
//                TextView textView = (TextView) findViewById(R.id.hello);
//                textView.setText(ss);
//                textView.setMovementMethod(LinkMovementMethod.getInstance());
//                textView.setHighlightColor(Color.TRANSPARENT);

            }
        }
        if(current.getComments().length()>0)
        {
            holder.comments.setVisibility(View.VISIBLE);
            String source = "Grade: "+current.getGrade()+"\n"+current.getComments()+"\n"+"View grading scheme";
            SpannableString ss = new SpannableString(source);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    Intent intent = new Intent(mcontext, GradeTemplateActivity.class);
                    intent.putExtra("id",current.getGrade_scheme());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(true);
                    ds.setFakeBoldText(true);
                }
            };
            ss.setSpan(clickableSpan, current.getGrade().length()+current.getComments().length()+9, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.comments.setText(ss);
            holder.comments.setMovementMethod(LinkMovementMethod.getInstance());

        }
        if(split_marks.length>0)
        {
            holder.awaiting_marks.setVisibility(View.GONE);
            holder.marks_layout.setVisibility(View.VISIBLE);
            if (split_marks[0].length() == 1) {
                holder.marks.setPadding(10, 5, 5, 0);
            }
            if (split_marks[0].length() > 3) {
//                RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) holder.marks.getLayoutParams();
//                params.setMargins(0,20,0,0);
//                holder.marks.setLayoutParams(params);
                holder.marks.setTextSize(18);
            }

            holder.marks.setText(split_marks[0]);
            holder.max_marks.setText(split_marks[1]);
            holder.cardView.setBackgroundColor(Color.parseColor(GlobalConstants.marked_card));
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, ComparisonGraphDialog.class);
                    intent.putExtra("subject_name", current.getSubject_name());
                    intent.putExtra("marks",Float.valueOf(split_marks[0]));
                    intent.putExtra("subject_code",current.getCode());
                    intent.putExtra("date",current.getDate());
                    intent.putExtra("average_marks",Float.valueOf(current.getAverage_marks()));
                    intent.putExtra("highest_marks",Float.valueOf(current.getHighest_marks()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });
//            holder.max_marks.setVisibility(View.GONE);
        }
        else
        {
            holder.marks_layout.setVisibility(View.GONE);
        }
//        else
//        {
////            holder.divider.setVisibility(View.GONE);
//        }
            String date_month_year=current.getDate();
            String[]split_date=date_month_year.split("-");
            holder.date.setText(split_date[0]);
            holder.month_year.setText(split_date[1]+" "+split_date[2]);
            holder.time.setText(current.getTime());
    }

    @Override
    public int getItemCount()
    {
        return tests.size();
    }
}
