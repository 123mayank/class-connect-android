package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.handysolver.smartclass.ExaminationDatesheet;
import com.handysolver.smartclass.ExaminationDetails;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.models.Exam;

import java.util.ArrayList;

/**
 * Created by Sanket on 08-11-2016.
 */
public class ExaminationListAdapter extends RecyclerView.Adapter<ExaminationListAdapter.ViewClass> {
    private ArrayList<Exam>exams=new ArrayList<>();
    private Context mcontext;
    private LayoutInflater layoutInflater;

    public ExaminationListAdapter(ArrayList<Exam> exams, Context montext)
    {
        layoutInflater=LayoutInflater.from(montext);
        this.exams = exams;
        this.mcontext = montext;
    }


    @Override
    public ExaminationListAdapter.ViewClass onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewClass(layoutInflater.inflate(R.layout.exam_card,parent,false));
    }

    @Override
    public void onBindViewHolder(ExaminationListAdapter.ViewClass holder, int position)
    {
        final Exam current_exam=exams.get(position);
        holder.name.setText(current_exam.getExam_name());
        String to_date_month_year=current_exam.getEnd_date();
        String[]to_split_date=to_date_month_year.split("-");
        holder.to_date.setText(to_split_date[0]);
        holder.to_month_year.setText(to_split_date[1]+" "+to_split_date[2]);
        String from_date_month_year=current_exam.getStart_date();
        String[]from_split_date=from_date_month_year.split("-");
        holder.from_date.setText(from_split_date[0]);
        holder.from_month_year.setText(from_split_date[1]+" "+from_split_date[2]);
        /*holder.exam_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, ExaminationDetails.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",current_exam.getId());
                intent.putExtra("exam_name",current_exam.getExam_name());
                mcontext.startActivity(intent);
            }
        });*/
        holder.list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, ExaminationDetails.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",current_exam.getId());
                intent.putExtra("grade_id",current_exam.getModel());
                intent.putExtra("exam_name",current_exam.getExam_name());
                mcontext.startActivity(intent);
            }
        });
        holder.dateSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, ExaminationDatesheet.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id",current_exam.getId());
                intent.putExtra("exam_name",current_exam.getExam_name());
                mcontext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return exams.size();
    }
    static class ViewClass extends RecyclerView.ViewHolder
    {
        TextView name,from_date,from_month_year,to_date,to_month_year;
        CardView exam_card;
        Button dateSheet, list;
        public ViewClass(View itemView)
        {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.exam_name);
            from_date=(TextView)itemView.findViewById(R.id.from_test_date);
            from_month_year=(TextView)itemView.findViewById(R.id.from_time_year);
            to_date=(TextView)itemView.findViewById(R.id.to_test_date);
            to_month_year=(TextView)itemView.findViewById(R.id.to_time_year);
            exam_card=(CardView)itemView.findViewById(R.id.exam_card);
            dateSheet=(Button)itemView.findViewById(R.id.date_sheet);
            list=(Button)itemView.findViewById(R.id.list);
        }
    }
}
