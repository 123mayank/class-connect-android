package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.ConversationActivity;
import com.handysolver.smartclass.CreateMessageDialog;
import com.handysolver.smartclass.ParentStudentListActivity;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Grade;

import java.util.ArrayList;

public class GradeAdapter extends RecyclerView.Adapter<GradeAdapter.V>
{
    private ArrayList<Grade>grades =new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;
    static class V extends RecyclerView.ViewHolder
    {
        TextView name;
        Button students,parents,individual;
        CardView class_card;
        public V(View itemView)
        {
            super(itemView);
            class_card = (CardView)itemView.findViewById(R.id.class_card);
            name=(TextView)itemView.findViewById(R.id.class_name);
            students=(Button)itemView.findViewById(R.id.student_button);
            parents=(Button)itemView.findViewById(R.id.parent_button);
            individual=(Button)itemView.findViewById(R.id.individual_button);

        }
    }

    public GradeAdapter(Context context,ArrayList<Grade>grades)
    {
        this.mcontext=context;
        layout=LayoutInflater.from(context);
        this.grades=grades;
        notifyItemRangeChanged(0,grades.size());
    }

    @Override
    public V onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=layout.inflate(R.layout.class_card,parent,false);
        V viewHolder=new V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final V holder, int position) {
        final Grade current =grades.get(position);
        holder.name.setText(current.getName());
        holder.students.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, CreateMessageDialog.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("class",current.getName());
                intent.putExtra("grade_id",current.getGrade_id());
                intent.putExtra("section_id",current.getSection_id());
                intent.putExtra("recipient", GlobalConstants.student_group);
                mcontext.startActivity(intent);
            }
        });
        holder.parents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, CreateMessageDialog.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("class",current.getName());
                intent.putExtra("grade_id",current.getGrade_id());
                intent.putExtra("section_id",current.getSection_id());
                intent.putExtra("recipient", GlobalConstants.parent_group);
                mcontext.startActivity(intent);
            }
        });
        holder.individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, ParentStudentListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("class",current.getName());
                intent.putExtra("grade_id",current.getGrade_id());
                intent.putExtra("section_id",current.getSection_id());
                intent.putExtra("recipient", GlobalConstants.parent_group);
                mcontext.startActivity(intent);
            }
        });
//        holder.subject.setText(current.getSubject());
//        holder.contact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent=new Intent(mcontext, CreateMessageDialog.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("name",current.getName());
//                intent.putExtra("id",current.getId());
//                mcontext.startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount()
    {
        return grades.size();
    }
}
