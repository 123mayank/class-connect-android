package com.handysolver.smartclass.adapters;

/**
 * Created by OWNER on 17-11-2016.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.handysolver.smartclass.OpenLinkActivity;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Transport;
import java.util.List;

public class TransportAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private List<Transport> transportList;
    private Context context;
    private static final int ASSIGNED=1;
    private static final int ASSIGNED_NOT=0;

    private Thread thread;
    @Override
    public int getItemViewType(int position) {
        int returnInt=0;
        Transport transportModel=transportList.get(position);
        if(transportModel.getAssigned()){
            returnInt=1;
        }else{
            returnInt=0;
        }

        return returnInt;
    }
    public TransportAdapter(List<Transport> transportList, Context context) {
        this.transportList = transportList;
        this.context=context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case ASSIGNED:
                Log.d("viewer","viewer assigned ");
                View viewAssigned = inflater.inflate(R.layout.transport_card_assigned, viewGroup, false);
                viewHolder= new ViewHolderAssigned(viewAssigned);
                break;
            case ASSIGNED_NOT:
                Log.d("viewer","viewer assigned not ");
                View viewAssignedNot = inflater.inflate(R.layout.transport_card_assigned_not, viewGroup, false);
                viewHolder=new ViewHolderAssignedNot(viewAssignedNot);
                break;
            default:
                Log.d("viewer","viewer default ");
                View viewAssignedDefualt = inflater.inflate(R.layout.transport_card_assigned, viewGroup, false);
                viewHolder= new ViewHolderAssigned(viewAssignedDefualt);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()){
            case ASSIGNED:
                ViewHolderAssigned viewHolderAssigned=(ViewHolderAssigned)viewHolder;
                configureAssignedView(viewHolderAssigned,position);
                break;
            case ASSIGNED_NOT:
                Log.d("g","getting recipient text");
                ViewHolderAssignedNot viewHolderAssignedNot=(ViewHolderAssignedNot)viewHolder;
                configureAssignedNotView(viewHolderAssignedNot,position);
                break;
        }


    }
    private void configureAssignedView(ViewHolderAssigned viewHolderAssigned, int position) {
        final Transport transportModel=transportList.get(position);
        Log.d("he","get it not "+transportModel.getDescription());
        if(transportModel.getName()!=null){
            viewHolderAssigned.getName().setText(transportModel.getName());
        }
        if(transportModel.getNumVehicle()!=null){
            viewHolderAssigned.getVehicleName().setText(transportModel.getNumVehicle());
        }
        if(transportModel.getDescription()!=null){
            viewHolderAssigned.getDescription().setText(transportModel.getDescription());
        }
        if(transportModel.getRouteFare()!=null){
            viewHolderAssigned.getRouteFare().setText(transportModel.getRouteFare());
        }
        if(transportModel.getLink()!=null){
            viewHolderAssigned.getLink().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendIntent(transportModel.getLink(), transportModel.getName());
                }
            });
        }
    }
    private void configureAssignedNotView(ViewHolderAssignedNot viewHolderAssignedNot, int position) {
        final Transport transportModel=transportList.get(position);
        Log.d("he","get it not on"+transportModel.getDescription());
        if(transportModel.getName()!=null){
            viewHolderAssignedNot.getName().setText(transportModel.getName());
        }
        if(transportModel.getNumVehicle()!=null){
            viewHolderAssignedNot.getVehicleName().setText(transportModel.getNumVehicle());
        }
        if(transportModel.getDescription()!=null){
            viewHolderAssignedNot.getDescription().setText(transportModel.getDescription());
        }
        if(transportModel.getRouteFare()!=null){
            viewHolderAssignedNot.getRouteFare().setText(transportModel.getRouteFare());
        }
        if(transportModel.getLink()!=null){
            viewHolderAssignedNot.getLink().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendIntent(transportModel.getLink(), transportModel.getName());
                }
            });
        }

    }
    private void sendIntent(String link, String title){
        Intent intent = new Intent(context, OpenLinkActivity.class);
        Bundle extras = new Bundle();
        extras.putString(GlobalConstants.ROUTE_LINK, link);
        extras.putString(GlobalConstants.ROUTE_TITLE, title);
        intent.putExtras(extras);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    private class ViewHolderAssigned extends RecyclerView.ViewHolder {

        private TextView name,vehicleName,description,routeFare,link;

        private ViewHolderAssigned(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            vehicleName=(TextView)itemView.findViewById(R.id.vehicle_name);
            description=(TextView)itemView.findViewById(R.id.description);
            routeFare=(TextView)itemView.findViewById(R.id.route_fare);
            link=(TextView)itemView.findViewById(R.id.link);
        }
        public TextView getName() {
            return name;
        }
        public TextView getVehicleName() {
            return vehicleName;
        }
        public TextView getDescription() {
            return description;
        }
        public TextView getRouteFare() {
            return routeFare;
        }
        public TextView getLink() {
            return link;
        }

    }
    /*ViewHolder for Sender video*/
    private class ViewHolderAssignedNot extends RecyclerView.ViewHolder {

        private TextView name,vehicleName,description,routeFare,link;

        private ViewHolderAssignedNot(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            vehicleName=(TextView)itemView.findViewById(R.id.vehicle_name);
            description=(TextView)itemView.findViewById(R.id.description);
            routeFare=(TextView)itemView.findViewById(R.id.route_fare);
            link=(TextView)itemView.findViewById(R.id.link);
        }
        public TextView getName() {
            return name;
        }
        public TextView getVehicleName() {
            return vehicleName;
        }
        public TextView getDescription() {
            return description;
        }
        public TextView getRouteFare() {
            return routeFare;
        }
        public TextView getLink() {
            return link;
        }


    }
    @Override
    public int getItemCount() {
        return transportList.size();
    }

}

