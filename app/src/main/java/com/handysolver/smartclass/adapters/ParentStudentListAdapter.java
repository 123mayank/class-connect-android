package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.handysolver.smartclass.CreateMessageDialog;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.StudentModel;

import java.util.ArrayList;

/**
 * Created by hsmm1 on 2/14/17.
 */

public class ParentStudentListAdapter extends RecyclerView.Adapter<ParentStudentListAdapter.V>
{
    private ArrayList<StudentModel> studentModels =new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;
    private MyClickListener myClickListener;

    static class V extends RecyclerView.ViewHolder
    {
        CheckBox student,parent;
        CardView student_parent;
        public V(View itemView)
        {
            super(itemView);
            student_parent = (CardView)itemView.findViewById(R.id.student_parent_card);
            student=(CheckBox) itemView.findViewById(R.id.student_name);
            parent=(CheckBox) itemView.findViewById(R.id.parent_name);
        }
    }

    public ParentStudentListAdapter(Context context,ArrayList<StudentModel>studentModels)
    {
        this.mcontext=context;
        layout=LayoutInflater.from(context);
        this.studentModels=studentModels;
        this.myClickListener = (ParentStudentListAdapter.MyClickListener)context;
        notifyItemRangeChanged(0,studentModels.size());
    }

    @Override
    public ParentStudentListAdapter.V onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=layout.inflate(R.layout.student_parent_card,parent,false);
        ParentStudentListAdapter.V viewHolder=new ParentStudentListAdapter.V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ParentStudentListAdapter.V holder, int position) {
        final StudentModel current =studentModels.get(position);
        holder.student.setText(current.getName());
        holder.student.setTag(current.getId());
        holder.student.setOnCheckedChangeListener(new Listener());
        if(current.getParent_name()!=null) {
            holder.parent.setText(mcontext.getString(R.string.parent_name,current.getParent_name()));
            holder.parent.setTag(current.getParent_id());
            holder.parent.setOnCheckedChangeListener(new Listener());
        }
        else
        {
            holder.parent.setVisibility(View.GONE);
        }
//        holder.subject.setText(current.getSubject());
//        holder.contact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent=new Intent(mcontext, CreateMessageDialog.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("name",current.getName());
//                intent.putExtra("id",current.getId());
//                mcontext.startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount()
    {
        return studentModels.size();
    }

    public interface MyClickListener {
        void add(int id, String name);
        void remove(int id, String name);
    }

    class Listener implements CompoundButton.OnCheckedChangeListener
    {

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if(b)
            {
                compoundButton.setChecked(true);
                myClickListener.add((int)compoundButton.getTag(),compoundButton.getText().toString());
            }
            if(!b)
            {
                compoundButton.setChecked(false);
                myClickListener.remove((int)compoundButton.getTag(),compoundButton.getText().toString());
            }
        }
    }
}
