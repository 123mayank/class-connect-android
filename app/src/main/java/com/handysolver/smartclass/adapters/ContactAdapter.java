package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.ConversationActivity;
import com.handysolver.smartclass.CreateMessageDialog;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.models.Contact;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.V>
{
    private ArrayList<Contact>contacts =new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;
    static class V extends RecyclerView.ViewHolder
    {
        TextView name,subject;
        ImageView display_picture;
        CardView contact;
        public V(View itemView)
        {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.person_name);
            subject=(TextView)itemView.findViewById(R.id.subject);
            display_picture=(ImageView)itemView.findViewById(R.id.person_photo);
            contact=(CardView)itemView.findViewById(R.id.contact_card_view);
        }
    }

    public ContactAdapter(Context context,ArrayList<Contact>contacts)
    {
        this.mcontext=context;
        layout=LayoutInflater.from(context);
        this.contacts=contacts;
        notifyItemRangeChanged(0,contacts.size());
    }

    @Override
    public V onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=layout.inflate(R.layout.contact_card_layout,parent,false);
        V viewHolder=new V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final V holder, int position) {
        final Contact current =contacts.get(position);
        holder.name.setText(current.getName());
        holder.subject.setText(current.getSubject());
        if(current.getImage_url().length()>1)
        {
            ImageRequest teacherImageRequest=new ImageRequest(current.getImage_url(), new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    holder.display_picture.setImageBitmap(response);
                }
            }, 0, 0, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    holder.display_picture.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_profile));
                }
            });
            Volley.newRequestQueue(mcontext).add(teacherImageRequest);
        }
        else
        {
            holder.display_picture.setImageDrawable(mcontext.getResources().getDrawable(R.drawable.ic_profile));
        }
        holder.contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mcontext, CreateMessageDialog.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("name",current.getName());
                intent.putExtra("id",current.getId());
                mcontext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return contacts.size();
    }
}
