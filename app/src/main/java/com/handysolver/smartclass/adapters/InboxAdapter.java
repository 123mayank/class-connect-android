package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Message;

import java.util.ArrayList;

/**
 * Created by Sanket on 20-10-2016.
 */
public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewClass> {

    private ArrayList<Message> messagesList = new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;

    public InboxAdapter(ArrayList<Message> messages, Context context) {
        /*Receiving array list of messages from the activity
        * The list is reversed as the latest message should be on top*/
        for (int i = messages.size() - 1; i >= 0; i--) {
            messagesList.add(messages.get(i));
            this.mcontext = context;
            notifyItemRangeChanged(0, messages.size());
        }
    }

    public InboxAdapter(ArrayList<Message> messages, Context context, String contact) {
        /*In case we are to show the conversation with a specific user in the chat module, this is where we filter our array list of messages so that it only contains the messages from that user*/
        ArrayList<Message> sortedMessages = new ArrayList<>();
        for (int i = messages.size() - 1; i >= 0; i--) {
            if (messages.get(i).getType().equals(GlobalConstants.personal_message_label_string)) {
                if (!messages.get(i).getSender().contains("->")) {
                    if (messages.get(i).getSender().equals(contact)) {
                        sortedMessages.add(messages.get(i));
                    }
                }
                if (messages.get(i).getSender().contains("->")) {
                    String[] split_sender_string = messages.get(i).getSender().split("->");
                    for (String split : split_sender_string) {
                        if (split.equals(contact)) {
                            sortedMessages.add(messages.get(i));
                        }
                    }
                }
            }
        }
        this.messagesList = sortedMessages;
    }

    @Override
    public ViewClass onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewClass viewClass = null;
        layout = LayoutInflater.from(parent.getContext());
        /*the values 0 and 1 are returned by the getItemViewType() method defined below
        * the layout files refer to the XML's containing the UI elements constituting a single sent/received message*/
        switch (viewType) {
            case 0:
                View view = layout.inflate(R.layout.received_message, parent, false);
                viewClass = new ViewClass(view);
                break;


            case 1:
                View view1 = layout.inflate(R.layout.sent_message, parent, false);
                viewClass = new ViewClass(view1);
                break;
        }
        return viewClass;
    }

    @Override
    public void onBindViewHolder(InboxAdapter.ViewClass holder, int position) {
        Message currentMessage = messagesList.get(position);
        /*if the getSender() message returns a string containing '->', it means that the message is sent by the user and hence it is shown in the sent message layout*/
        if (currentMessage.getSender().contains("->")) {
//            holder.sender.setVisibility(View.GONE);
            if (currentMessage.getType().equals(GlobalConstants.personal_message_label_string)) {
                holder.title.setVisibility(View.GONE);
//                holder.sender.setCompoundDrawables(null,null,null,null);
                String[] split_recipient_name = currentMessage.getSender().split("->");
                holder.sender.setText(split_recipient_name[1]);
            }
//            if (currentMessage.getType().equals(GlobalConstants.personal_message_label_string)) {
//                holder.sender.setText(currentMessage.getTopic());
//            }
            String[] split_times = currentMessage.getTime().split(" ");
            String[] split_hours_minutes = split_times[1].split("-");
            String[] split_days_month_year=split_times[0].split("-");
            holder.time.setText(split_days_month_year[0]+"-"+split_days_month_year[1]+"-"+split_days_month_year[2]+", "+split_hours_minutes[0] + ":" + split_hours_minutes[1] + " " + split_times[2]);
            holder.body.setText(Html.fromHtml(currentMessage.getBody()));
        } else {
            if (!currentMessage.getType().equals(GlobalConstants.personal_message_label_string)) {
                holder.sender.setText(currentMessage.getSender());
                holder.title.setText(currentMessage.getTopic());
            }
            if (currentMessage.getType().equals(GlobalConstants.personal_message_label_string)) {
                holder.title.setVisibility(View.GONE);
                holder.sender.setText(currentMessage.getSender());
            }
            String[] split_times = currentMessage.getTime().split(" ");
            String[] split_hours_minutes = split_times[1].split("-");
            String[] split_days_month_year=split_times[0].split("-");
            holder.time.setText(split_days_month_year[0]+"-"+split_days_month_year[1]+"-"+split_days_month_year[2]+", "+split_hours_minutes[0] + ":" + split_hours_minutes[1] + " " + split_times[2]);
            holder.body.setText(Html.fromHtml(currentMessage.getBody()));
        }
    }

    @Override
    public int getItemViewType(int position) {
        /*return 0 if message is a sent message, return 1 if received*/
        if (!messagesList.get(position).getSender().contains("->")) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public long getItemId(int position) {
        /*used to refer to the current item being iterated in the list of messages*/
        return position;
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    static class ViewClass extends RecyclerView.ViewHolder {
        /*This class is defined as per the guidelines of the recycler view which is used to define all the UI elements being populated based on the data sent to the adapter*/
        TextView title, sender, time, body;

        public ViewClass(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.message_title);
            sender = (TextView) itemView.findViewById(R.id.sender_name);
            time = (TextView) itemView.findViewById(R.id.message_time);
            body = (TextView) itemView.findViewById(R.id.message_content);
        }
    }
}
