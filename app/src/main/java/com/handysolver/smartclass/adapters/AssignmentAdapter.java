package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handysolver.smartclass.ComparisonGraphDialog;
import com.handysolver.smartclass.DownloadDialog;
import com.handysolver.smartclass.GradeTemplateActivity;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.AssignmentModel;

import java.util.ArrayList;

/**
 * Created by Sanket on 16-11-2016.
 */
/*Test and Exam have almost similar views and hence we are using the same TestAdapter for them. Assignment also has a very similar view but we are using a different adapter since we have to show downloadable links in the view as well*/
public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.V> {
    private ArrayList<AssignmentModel> assignments = new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;

    public AssignmentAdapter(ArrayList<AssignmentModel> assignments, Context context) {
        this.assignments = assignments;
        this.mcontext = context;
        layout = LayoutInflater.from(context);
    }

    @Override
        /*this inner class defines the various UI elements that are to be populated for a single assignment*/
    public AssignmentAdapter.V onCreateViewHolder(ViewGroup parent, int viewType) {
        /*Specifying the XML file which contains the layout for each card*/
        View view = layout.inflate(R.layout.assignment_layout, parent, false);
        V viewHolder = new V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AssignmentAdapter.V holder, int position) {
        final AssignmentModel current = assignments.get(position);
        holder.subject.setText(current.getSubject_name());
        if (!current.getCode().equals(""))
            holder.code.setText(current.getCode());
        final String[] split_marks = current.getMarks().split("/");
        if (current.getDeadline() != null)
        {
            holder.grade.setVisibility(View.VISIBLE);
            holder.grade.setText(mcontext.getString(R.string.deadline,current.getDeadline().substring(0,5)));
        }
        if(current.getGrade().length()>0)
        {
            holder.comments.setVisibility(View.VISIBLE);
            holder.comments.setText("Grade: "+current.getGrade());
            if(current.getComments().length() == 0)
            {
                SpannableString ss = new SpannableString("Grade: "+current.getGrade()+"\n"+"View grading scheme");
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        Intent intent = new Intent(mcontext, GradeTemplateActivity.class);
                        intent.putExtra("id",current.getGrade_template_id());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mcontext.startActivity(intent);

                    }
                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setFakeBoldText(true);
                        ds.setUnderlineText(true);
                    }
                };
                ss.setSpan(clickableSpan, current.getGrade().length()+7, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.comments.setText(ss);
                holder.comments.setMovementMethod(LinkMovementMethod.getInstance());
//                TextView textView = (TextView) findViewById(R.id.hello);
//                textView.setText(ss);
//                textView.setMovementMethod(LinkMovementMethod.getInstance());
//                textView.setHighlightColor(Color.TRANSPARENT);

            }
        }
        if(current.getComments().length()>0)
        {
            holder.comments.setVisibility(View.VISIBLE);
            String source = "Grade: "+current.getGrade()+"\n"+current.getComments()+"\n"+"View grading scheme";
            SpannableString ss = new SpannableString(source);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    Intent intent = new Intent(mcontext, GradeTemplateActivity.class);
                    intent.putExtra("id",current.getGrade_template_id());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(true);
                    ds.setFakeBoldText(true);
                }
            };
            ss.setSpan(clickableSpan, current.getGrade().length()+current.getComments().length()+9, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.comments.setText(ss);
            holder.comments.setMovementMethod(LinkMovementMethod.getInstance());

        }
        if (split_marks.length > 0) {
            holder.awaiting_marks.setVisibility(View.GONE);
            if (split_marks[0].length() == 1) {
                holder.marks.setPadding(10, 5, 5, 0);
            }
            if (split_marks[0].length() > 3) {
//                RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) holder.marks.getLayoutParams();
//                params.setMargins(0,20,0,0);
//                holder.marks.setLayoutParams(params);
                holder.marks.setTextSize(18);
            }
            holder.marks_layout.setVisibility(View.VISIBLE);
            holder.marks.setText(split_marks[0]);
            holder.cardView.setBackgroundColor(Color.parseColor(GlobalConstants.marked_card));
            holder.max_marks.setText(split_marks[1]);
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, ComparisonGraphDialog.class);
                    intent.putExtra("subject_name", current.getSubject_name());
                    intent.putExtra("marks", Float.valueOf(split_marks[0]));
                    intent.putExtra("average_marks", Float.valueOf(current.getAverage_marks()));
                    intent.putExtra("highest_marks", Float.valueOf(current.getHighest_marks()));
                    intent.putExtra("subject_code",current.getCode());
                    intent.putExtra("date",current.getDate());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });
        } else {
            holder.marks_layout.setVisibility(View.GONE);
        }
        final String names = current.getLinks()[0];
        final String links = current.getLinks()[1];
        if (names.length() > 0||current.getTime().length()>0) {
            holder.button_layout.setVisibility(View.VISIBLE);
            holder.button_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, DownloadDialog.class);
                    String[] split_names = names.split(" ");
                    String[] split_links = links.split(" ");
                    intent.putExtra("description",current.getTime());
                    intent.putExtra("numbers", split_names.length);
                    intent.putExtra("title", current.getSubject_name() + "- " + current.getDate());
                    for (int i = 0; i < split_names.length; i++) {
                        intent.putExtra("name" + i, split_names[i]);
                        intent.putExtra("link" + i, split_links[i]);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });
            holder.download_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, DownloadDialog.class);
                    String[] split_names = names.split(" ");
                    String[] split_links = links.split(" ");
                    intent.putExtra("description",current.getTime());
                    intent.putExtra("numbers", split_names.length);
                    intent.putExtra("title", current.getSubject_name() + "- " + current.getDate());
                    for (int i = 0; i < split_names.length; i++) {
                        intent.putExtra("name" + i, split_names[i]);
                        intent.putExtra("link" + i, split_links[i]);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });
        }

        String date_month_year = current.getDate();
        String[] split_date = date_month_year.split("-");
        holder.date.setText(split_date[0]);
        holder.month_year.setText(split_date[1] + " " + split_date[2]);
//        holder.time.setText(Html.fromHtml(current.getTime()));
        holder.time.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return assignments.size();
    }

    static class V extends RecyclerView.ViewHolder {
        TextView subject, code, marks, date, month_year, time,max_marks,grade,comments,awaiting_marks;
        View cardView;
        Button download_button;
        LinearLayout button_layout, marks_layout;

        public V(View itemView) {
            super(itemView);
            max_marks=(TextView)itemView.findViewById(R.id.max_marks);
            marks_layout = (LinearLayout) itemView.findViewById(R.id.marks_layout);
            subject = (TextView) itemView.findViewById(R.id.subject_name);
            code = (TextView) itemView.findViewById(R.id.subject_code);
            awaiting_marks = (TextView) itemView.findViewById(R.id.not_marked);
            comments = (TextView) itemView.findViewById(R.id.comments);
            grade = (TextView) itemView.findViewById(R.id.grade_text);
            marks = (TextView) itemView.findViewById(R.id.marks);
            date = (TextView) itemView.findViewById(R.id.test_date);
            month_year = (TextView) itemView.findViewById(R.id.time_year);
            time = (TextView) itemView.findViewById(R.id.test_time);
            cardView = (View) itemView.findViewById(R.id.test_card);
            download_button = (Button) itemView.findViewById(R.id.attachment_button);
            button_layout = (LinearLayout) itemView.findViewById(R.id.attachment_layout);
        }
    }
}
