package com.handysolver.smartclass.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handysolver.smartclass.ComparisonGraphDialog;
import com.handysolver.smartclass.DownloadDialog;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.NoticeModel;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Sanket on 16-11-2016.
 */
/*Test and Exam have almost similar views and hence we are using the same TestAdapter for them. Notice also has a very similar view but we are using a different adapter since we have to show downloadable links in the view as well*/
public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.V> {
    private ArrayList<NoticeModel> notices = new ArrayList<>();
    private LayoutInflater layout;
    private Context mcontext;

    public NoticeAdapter(ArrayList<NoticeModel> notices, Context context) {
        this.notices = notices;
        this.mcontext = context;
        layout = LayoutInflater.from(context);
    }

    @Override
        /*this inner class defines the various UI elements that are to be populated for a single assignment*/
    public NoticeAdapter.V onCreateViewHolder(ViewGroup parent, int viewType) {
        /*Specifying the XML file which contains the layout for each card*/
        View view = layout.inflate(R.layout.notice_card, parent, false);
        V viewHolder = new V(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final NoticeAdapter.V holder, int position) {
        final NoticeModel current = notices.get(position);
        holder.subject.setText(current.getTitle());
//        if (!current.getCode().equals(""))
//            holder.code.setText(current.getCode());
//        final String[] split_marks = current.getMarks().split("/");
//        Log.d("test", String.valueOf(current.getAverage_marks()));
//        if (split_marks.length > 0) {
//            if (split_marks[0].length() == 1) {
//                holder.marks.setPadding(10, 5, 5, 0);
//            }
//            if (split_marks[0].length() > 3) {
////                RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) holder.marks.getLayoutParams();
////                params.setMargins(0,20,0,0);
////                holder.marks.setLayoutParams(params);
//                holder.marks.setTextSize(18);
//            }
//            holder.marks_layout.setVisibility(View.VISIBLE);
//            holder.marks.setText(split_marks[0]);
//            holder.cardView.setBackgroundColor(Color.parseColor(GlobalConstants.marked_card));
//            holder.max_marks.setText(split_marks[1]);
//            holder.cardView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(mcontext, ComparisonGraphDialog.class);
//                    intent.putExtra("subject_name", current.getSubject_name());
//                    intent.putExtra("marks", Integer.valueOf(split_marks[0]));
//                    intent.putExtra("average_marks", current.getAverage_marks());
//                    intent.putExtra("highest_marks", current.getHighest_marks());
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    mcontext.startActivity(intent);
//                }
//            });
//        } else {
//            holder.marks_layout.setVisibility(View.GONE);
//        }
//        final String names = current.getLinks()[0];
//        final String links = current.getLinks()[1];
        final String names = current.getLinks()[0];
        final String links = current.getLinks()[1];
        if (names.length() > 0 || current.getNotice().length() > 0) {
            holder.button_layout.setVisibility(View.VISIBLE);
            holder.download_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, DownloadDialog.class);
                    intent.putExtra("description", current.getNotice());
                    intent.putExtra("title", current.getTitle() + "- " + current.getDate());
                    String[] split_names = names.split(" ");
                    String[] split_links = links.split(" ");
                    intent.putExtra("numbers", split_names.length);
                    for (int i = 0; i < split_names.length; i++) {
                        intent.putExtra("name" + i, split_names[i]);
                        intent.putExtra("link" + i, split_links[i]);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });
            holder.button_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, DownloadDialog.class);
                    intent.putExtra("description", current.getNotice());
                    intent.putExtra("title", current.getTitle() + "- " + current.getDate());
                    String[] split_names = names.split(" ");
                    String[] split_links = links.split(" ");
                    intent.putExtra("numbers", split_names.length);
                    for (int i = 0; i < split_names.length; i++) {
                        intent.putExtra("name" + i, split_names[i]);
                        intent.putExtra("link" + i, split_links[i]);
                    }
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });
        }

        String date_month_year = current.getDate();
        String[] split_date = date_month_year.split("-");
        holder.date.setText(split_date[0]);
        holder.month_year.setText(split_date[1] + " " + split_date[2]);
//        holder.time.setText(Html.fromHtml(current.getTime()));
//        holder.time.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return notices.size();
    }

    static class V extends RecyclerView.ViewHolder {
        AutofitTextView subject;
        TextView date, month_year;
        View cardView;
        Button download_button;
        LinearLayout button_layout;

        public V(View itemView) {
            super(itemView);
//            max_marks=(TextView)itemView.findViewById(R.id.max_marks);
//            marks_layout = (LinearLayout) itemView.findViewById(R.id.marks_layout);
            subject = (AutofitTextView) itemView.findViewById(R.id.subject_name);
//            code = (TextView) itemView.findViewById(R.id.subject_code);
//            marks = (TextView) itemView.findViewById(R.id.marks);
            date = (TextView) itemView.findViewById(R.id.test_date);
            month_year = (TextView) itemView.findViewById(R.id.time_year);
//            time = (TextView) itemView.findViewById(R.id.test_time);
            cardView = (View) itemView.findViewById(R.id.notice_card);
            download_button = (Button) itemView.findViewById(R.id.attachment_button);
            button_layout = (LinearLayout) itemView.findViewById(R.id.attachment_layout);
        }
    }
}
