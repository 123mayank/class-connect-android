package com.handysolver.smartclass;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.services.AutoLogout;

public class DownloadDialog extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_dialog);
        Bundle bundle = getIntent().getExtras();
        setTitle(bundle.getString("title"));
        int a = bundle.getInt("numbers");
        sharedPreferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        View layout = (LinearLayout) findViewById(R.id.links_layout);
        if (bundle.getString("description") != null) {
            TextView description = new TextView(this);
            description.setText(Html.fromHtml(bundle.getString(("description"))));
            description.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//            description.setTypeface(Typeface.DEFAULT_BOLD);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.CENTER;
            description.setLayoutParams(layoutParams);
            ((LinearLayout) layout).addView(description);
        }
        if (bundle.getString("name" + "0") != null) {
            if (bundle.getString("name" + "0").length() > 0) {
                TextView attachment = new TextView(this);
                attachment.setText(getResources().getText(R.string.attachments));
                attachment.setTypeface(Typeface.DEFAULT_BOLD);
                ((LinearLayout) layout).addView(attachment);
            }
        }
        for (int i = 0; i < a; i++) {
//            String asd=bundle.getString("")
            TextView file_name = new TextView(this);
            file_name.setClickable(true);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 10);
            file_name.setLayoutParams(params);
            file_name.setMovementMethod(LinkMovementMethod.getInstance());
            String html_string = "<a href='" + bundle.getString("link" + i) + "'>" + bundle.getString("name" + i) + "</a>";
            Log.d("string", html_string);
            file_name.setText(Html.fromHtml("<a href='" + bundle.getString("link" + i) + "'>" + bundle.getString("name" + i) + "</a>"));
            ((LinearLayout) layout).addView(file_name);
        }
        Button ok = new Button(this);
        ok.setText("O.K.");
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        ok.setLayoutParams(layoutParams);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ((LinearLayout) layout).addView(ok);

//        TextView textView =(TextView)findViewById(R.id.url);
//        textView.setClickable(true);
//        textView.setMovementMethod(LinkMovementMethod.getInstance());
//        String text = "<a href='http://www.dealsinbongo.com/smartclass/storage/web/course/5/5-e7zPGICDGH3FiBX4AFwcMFEHJQfvz6R9.jpg'> Google </a>";
//        textView.setText(Html.fromHtml(text));


    }
    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
