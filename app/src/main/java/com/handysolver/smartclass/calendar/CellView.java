package com.handysolver.smartclass.calendar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;

import com.p_v.flexiblecalendar.entity.Event;
import com.p_v.flexiblecalendar.view.BaseCellView;

import java.util.List;

/**
 * Created by Sanket on 31-10-2016.
 */
public class CellView extends BaseCellView {

    private boolean hasEvents;

    public CellView(Context context) {
        super(context);
    }


    public CellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CellView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if(!getStateSet().contains(STATE_SELECTED) && !getStateSet().contains(SELECTED_TODAY) &&
                getStateSet().contains(STATE_REGULAR) && hasEvents){
            this.setBackgroundColor(Color.BLUE);
        }
        if(getStateSet().contains(STATE_SELECTED) && hasEvents){
            this.setBackgroundColor(Color.RED);
        }
    }

    @Override
    public void setEvents(List<? extends Event> colorList) {
        this.hasEvents = colorList !=null && !colorList.isEmpty();
        invalidate();
        requestLayout();
    }
}
