package com.handysolver.smartclass.constants;

/**
 * Created by Sanket on 13-10-2016.
 */
public class GlobalConstants {
    public static final String presentColor="#449d44";
    public static final String subjectColor="#5a595b";
    public static final String absentColor="#dd4b39";
    public static final String sickColor="#f39c12";
    public static final String white="#ffffff";
    public static final String black="#000000";
    public static final String marked_card="#2F962E";
    public static final String user_details_file_name="Credentials";
    public static final String user_defaults="Default";
    public static final String attendance_message="Attendance Message";
    public static final String test_message="Test Message";
    public static final String test_marks="Test Marks";
    public static final String examination_message="Examination Message";
    public static final String examination_marks="Examination Marks";
    public static final String assignment_message="Assignment Message";
    public static final String assignment_marks="Assignment Marks";
    public static final String upcoming_holiday_message="Holiday Notification Message";
    public static final String upcoming_test_message="Test Notification Message";
    public static final String upcoming_examination_message="Examination Notification Message";
    public static final String upcoming_assignment_message="Assignment Notification Message";
    public static final String personal_message_label_string="Personal Message";
    public static final String admin_role="administrator";
    public static final String parent_role="parent";
    public static final String student_role="student";
    public static final String superAdmin_role="superAdmin";
    public static final String teacher_role="teacher";
    public static final String parent_selected_student="selected_student_id";
    public static final String parent_selected_student_name="selected_student_name";
    public static final String message_status_success="success";
    public static final String timetable_message="Time Table Message";
    public static final String notice_message="Notice";
    public static final String inbox="inbox";
    public static final String outbox="outbox";
    public static final String student_group="all student";
    public static final String parent_group="all parent";
    public static Boolean should_logout=false;
    /*settings*/
    public static final String TIME_TABLE_UPDATE="timetable_update";
    public static final String ATTENDANCE="attendance";
    public static final String NOTICEBOARD="notice";
    public static final String MESSAGE="message";
    public static final String ASSG_CREATION="assignment_create";
    public static final String ASSG_UPCOMING="assignment_upcoming";
    public static final String ASSG_MARKED="assignment_marked";
    public static final String CLASS_CREATION="test_create";
    public static final String CLASS_UPCOMING="test_upcoming";
    public static final String CLASS_MARKED="test_marked";
    public static final String EXAM_CREATION="exam_create";
    public static final String EXAM_UPCOMING="exam_upcoming";
    public static final String EXAM_MARKED="exam_marked";
    public static final String HOLIDAY_UPCOMING="holiday_upcoming";
    public static final String ROUTE_ID="ROUTE_ID";
    public static final String ROUTE_LINK="ROUTE_LINK";
    public static final String ROUTE_TITLE="ROUTE_TITLE";

    public static final int SWITCH_ON=1;
    public static final int SWITCH_OFF=0;

//    URL's
public static final String BASE_BACKEND_URL="http://dealsinbongo.com/smartclass/api/web/v1/";
public static final String TEACHER_DETAIL_CONTROLLER="users/teacher-detail";
public static final String ASSIGNMENT_DETAIL_CONTROLLER="courses/course-detail";
public static final String ATTENDANCE_DETAIL_CONTROLLER="attendances/attendance-detail";
public static final String ATTENDANCE_COUNT_CONTROLLER="attendances/attendance-count";
public static final String TEACHER_CLASS_CONTROLLER="messages/teacher-class";
public static final String STUDENT_TEACHER_CONTROLLER="messages/student-to-teacher";
public static final String PARENT_TEACHER_CONTROLLER="messages/parent-to-teacher";
public static final String TIMETABLE_CONTROLLER="time-tables/get-timetable";
public static final String TRANSPORT_CONTROLLER="transports/route";
public static final String NOTICEBOARD_CONTROLLER="noticeboards/view-notice";
public static final String PARENT_STUDENT_CONTROLLER="messages/class-student";
public static final String TEST_CONTROLLER="tests/test-detail";
public static final String SEND_MESSAGE_CONTROLLER="messages/send-message";
public static final String STUDENT_CONTROLLER="users/parent-student-list";
public static final String EXAM_TIMETABLE_CONTROLLER="examinations/exam-timetable";
public static final String EXAM_SUBJECT_CONTROLLER="examinations/exam-subject-detail";
public static final String CHANGE_NOTIFICATION_PREFERENCE_CONTROLLER="notifications/change-status";
public static final String GET_NOTIFICATION_PREFERENCE_CONTROLLER="notifications/status";
public static final String GRADE_CONTROLLER="grade-schemes/grade-scheme";
public static final String INBOX_CONTROLLER="messages/";
public static final String EXAM_CONTROLLER="examinations/exam-detail";
public static final String HOLIDAY_CONTROLLER="holidays/holiday-detail";
public static final String LOGIN_CONTROLLER="logins/login";
public static final String PLAYER_ID_CONTROLLER="users/student-player-id";

}
