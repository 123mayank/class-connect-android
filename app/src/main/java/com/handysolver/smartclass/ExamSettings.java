package com.handysolver.smartclass;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ExamSettings extends AppCompatActivity{
    SharedPreferences sharedPreferences;
    Switch tableUpdate, attendence, noticeBoard, message, assg_creation, assg_upcoming, assg_marked,class_creation, class_upcoming, class_marked, exam_creation, exam_upcoming, exam_marked, holiday_upcoming;
    TextView ttu,at,nb,me,as,ac,ua,ma,ct,tc,tu,mt,ex,ce,ue,mex,ho,tv;
    LinearLayout assignment, exam, test;
    RequestQueue requestQueue;
    SwipeRefreshLayout refreshLayout;
    private Boolean press_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        press_exit = false;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Enable/Disable Notifications");

        requestQueue = Volley.newRequestQueue(this);

        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);

        // get switches
        ttu = (TextView)findViewById(R.id.ttu);
        at = (TextView)findViewById(R.id.at);
        nb = (TextView)findViewById(R.id.nb);
        me = (TextView)findViewById(R.id.me);
        as = (TextView)findViewById(R.id.as);
        ac = (TextView)findViewById(R.id.ac);
        ua = (TextView)findViewById(R.id.ua);
        ma = (TextView)findViewById(R.id.ma);
        ct = (TextView)findViewById(R.id.ct);
        tc = (TextView)findViewById(R.id.tc);
        tu = (TextView)findViewById(R.id.tu);
        mt = (TextView)findViewById(R.id.mt);
        ex = (TextView)findViewById(R.id.ex);
        ce = (TextView)findViewById(R.id.ce);
        ue = (TextView)findViewById(R.id.ue);
        mex = (TextView)findViewById(R.id.mex);
        ho = (TextView)findViewById(R.id.ho);
        tv = (TextView)findViewById(R.id.tv);
        assignment = (LinearLayout)findViewById(R.id.assignments_linear);
        test = (LinearLayout)findViewById(R.id.tests_linear);
        exam = (LinearLayout)findViewById(R.id.exams_linear);
        if(sharedPreferences.getString("role","").equals(GlobalConstants.teacher_role)||sharedPreferences.getString("role","").equals(GlobalConstants.admin_role))
        {
            assignment.setVisibility(View.GONE);
            test.setVisibility(View.GONE);
            exam.setVisibility(View.GONE);
            ttu.setVisibility(View.GONE);
            at.setVisibility(View.GONE);
            as.setVisibility(View.GONE);
            ac.setVisibility(View.GONE);
            ua.setVisibility(View.GONE);
            ma.setVisibility(View.GONE);
            ct.setVisibility(View.GONE);
            tc.setVisibility(View.GONE);
            tu.setVisibility(View.GONE);
            mt.setVisibility(View.GONE);
            ex.setVisibility(View.GONE);
            ce.setVisibility(View.GONE);
            ue.setVisibility(View.GONE);
            mex.setVisibility(View.GONE);
        }
        tableUpdate=(Switch) findViewById(R.id.time_table_update);
        tableUpdate.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.TIME_TABLE_UPDATE));

        attendence=(Switch) findViewById(R.id.attendence);
        attendence.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.ATTENDANCE));

        noticeBoard=(Switch) findViewById(R.id.noticeboard);
        noticeBoard.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.NOTICEBOARD));

        message=(Switch) findViewById(R.id.message);
        message.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.MESSAGE));

        assg_creation=(Switch) findViewById(R.id.assg_creation);
        assg_creation.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.ASSG_CREATION));

        assg_upcoming=(Switch) findViewById(R.id.assg_upcoming);
        assg_upcoming.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.ASSG_UPCOMING));

        assg_marked=(Switch) findViewById(R.id.assg_marked);
        assg_marked.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.ASSG_MARKED));

        class_creation=(Switch) findViewById(R.id.class_creation);
        class_creation.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.CLASS_CREATION));

        class_upcoming=(Switch) findViewById(R.id.class_upcoming);
        class_upcoming.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.CLASS_UPCOMING));

        class_marked=(Switch) findViewById(R.id.class_marked);
        class_marked.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.CLASS_MARKED));

        exam_creation=(Switch) findViewById(R.id.exam_creation);
        exam_creation.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.EXAM_CREATION));

        exam_upcoming=(Switch) findViewById(R.id.exam_upcoming);
        exam_upcoming.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.EXAM_UPCOMING));

        exam_marked=(Switch) findViewById(R.id.exam_marked);
        exam_marked.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.EXAM_MARKED));

        holiday_upcoming=(Switch) findViewById(R.id.holiday_upcoming);
        holiday_upcoming.setOnCheckedChangeListener(new SwitchListener(GlobalConstants.HOLIDAY_UPCOMING));

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        refreshLayout.setRefreshing(true);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
        getData();
    }
    private class SwitchListener implements CompoundButton.OnCheckedChangeListener{
        private String id;
        public SwitchListener(String id){
            this.id=id;
        }
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            int value=1;
            if(isChecked) {
                value=1;
                Log.d("You are :", "Checked  "+id);
            }
            else {
                value=0;
                Log.d("You are :", "Not Checked "+id);
            }
            setData(id,value);
        }
    }
    public void setData(final String key, final int value){
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest exam_list_request = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.CHANGE_NOTIFICATION_PREFERENCE_CONTROLLER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                /*response 1 is the response from server which is first converted to JsonObject which further contains a JSONArray containing the average and number of exams and another JSONArray containing details of individual exams*/
                Log.d("success","new success "+response1);
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Log.d("success","new success error "+error.getMessage());
                error.printStackTrace();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "";
                body = "user_id=" + Integer.valueOf(sharedPreferences.getString("id", null))+ "&key=" + key+"&value=" + value;
                return body.getBytes();
            }
        };
        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(exam_list_request);
    }
    public void getData() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest exam_list_request = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.GET_NOTIFICATION_PREFERENCE_CONTROLLER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                /*response 1 is the response from server which is first converted to JsonObject which further contains a JSONArray containing the average and number of exams and another JSONArray containing details of individual exams*/
                JSONArray datesheetDetail;
                JSONArray num_exams;
                JSONObject response3;
                try {
                    datesheetDetail = new JSONArray(response1);
                    for (int i = 0; i < datesheetDetail.length(); i++) {
                        JSONObject response = datesheetDetail.getJSONObject(i);
                        if(!sharedPreferences.getString("role","").equals(GlobalConstants.teacher_role)||!sharedPreferences.getString("role","").equals(GlobalConstants.admin_role))
                        {
                            if(response.get("timetable_update").toString().length()>0){
                                tableUpdate.setVisibility(View.VISIBLE);
                                if(response.getInt("timetable_update")==GlobalConstants.SWITCH_ON){
                                    tableUpdate.setChecked(true);
                                }else{
                                    tableUpdate.setChecked(false);
                                }
                            }
                            if(response.get("attendance").toString().length()>0){
                                attendence.setVisibility(View.VISIBLE);
                                if(response.getInt("attendance")==GlobalConstants.SWITCH_ON){
                                    attendence.setChecked(true);
                                }else{
                                    attendence.setChecked(false);
                                }
                            }
                            if(response.get("assignment_create").toString().length()>0){
                                assg_creation.setVisibility(View.VISIBLE);
                                if(response.getInt("assignment_create")==GlobalConstants.SWITCH_ON){
                                    assg_creation.setChecked(true);
                                }else{
                                    assg_creation.setChecked(false);
                                }
                            }
                            if(response.get("assignment_marked").toString().length()>0){
                                assg_marked.setVisibility(View.VISIBLE);
                                if(response.getInt("assignment_marked")==GlobalConstants.SWITCH_ON){
                                    assg_marked.setChecked(true);
                                }else{
                                    assg_marked.setChecked(false);
                                }
                            }
                            if(response.get("assignment_upcoming").toString().length()>0){
                                assg_upcoming.setVisibility(View.VISIBLE);
                                if(response.getInt("assignment_upcoming")==GlobalConstants.SWITCH_ON){
                                    assg_upcoming.setChecked(true);
                                }else{
                                    assg_upcoming.setChecked(false);
                                }
                            }
                            if(response.get("test_create").toString().length()>0){
                                class_creation.setVisibility(View.VISIBLE);
                                if(response.getInt("test_create")==GlobalConstants.SWITCH_ON){
                                    class_creation.setChecked(true);
                                }else{
                                    class_creation.setChecked(false);
                                }
                            }
                            if(response.get("test_marked").toString().length()>0){
                                class_marked.setVisibility(View.VISIBLE);
                                if(response.getInt("test_marked")==GlobalConstants.SWITCH_ON){
                                    class_marked.setChecked(true);
                                }else{
                                    class_marked.setChecked(false);
                                }
                            }
                            if(response.get("test_upcoming").toString().length()>0){
                                class_upcoming.setVisibility(View.VISIBLE);
                                if(response.getInt("test_upcoming")==GlobalConstants.SWITCH_ON){
                                    class_upcoming.setChecked(true);
                                }else{
                                    class_upcoming.setChecked(false);
                                }
                            }
                            if(response.get("exam_create").toString().length()>0){
                                exam_creation.setVisibility(View.VISIBLE);
                                if(response.getInt("exam_create")==GlobalConstants.SWITCH_ON){
                                    exam_creation.setChecked(true);
                                }else{
                                    exam_creation.setChecked(false);
                                }
                            }
                            if(response.get("exam_marked").toString().length()>0){
                                exam_marked.setVisibility(View.VISIBLE);
                                if(response.getInt("exam_marked")==GlobalConstants.SWITCH_ON){
                                    exam_marked.setChecked(true);
                                }else{
                                    exam_marked.setChecked(false);
                                }
                            }
                            if(response.get("exam_upcoming").toString().length()>0){
                                exam_upcoming.setVisibility(View.VISIBLE);
                                if(response.getInt("exam_upcoming")==GlobalConstants.SWITCH_ON){
                                    exam_upcoming.setChecked(true);
                                }else{
                                    exam_upcoming.setChecked(false);
                                }
                            }
                        }

                        if(response.get("message").toString().length()>0){
                            message.setVisibility(View.VISIBLE);
                            if(response.getInt("message")==GlobalConstants.SWITCH_ON){
                                message.setChecked(true);
                            }else{
                                message.setChecked(false);
                            }
                        }

                        if(response.get("notice").toString().length()>0){
                            noticeBoard.setVisibility(View.VISIBLE);
                            if(response.getInt("notice")==GlobalConstants.SWITCH_ON){
                                noticeBoard.setChecked(true);
                            }else{
                                noticeBoard.setChecked(false);
                            }
                        }


                        if(response.get("holiday_upcoming").toString().length()>0){
                            holiday_upcoming.setVisibility(View.VISIBLE);
                            if(response.getInt("holiday_upcoming")==GlobalConstants.SWITCH_ON){
                                holiday_upcoming.setChecked(true);
                            }else{
                                holiday_upcoming.setChecked(false);
                            }
                        }
                        Log.d("get date","get setting data "+response.toString());
                    }
                    //setDateSheet();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                error.printStackTrace();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "";
                body = "user_id=" + Integer.valueOf(sharedPreferences.getString("id", null));
                return body.getBytes();
            }
        };
        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(exam_list_request);
    }
    public void handleEvent(View view){

    }
    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

}
