package com.handysolver.smartclass;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.ExaminationListAdapter;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Exam;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
/*This class handles the examination module, We are populating an array list of type Exam, the class of which can be found in the models folder. Next the list is passed to the ExaminationListAdapter, which takes care of populating the recycler view with the list*/
public class Examination extends AppCompatActivity {
    SharedPreferences preferences;
    private ArrayList<Exam>exams=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examination);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        preferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        StringRequest exam_list_request=new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.EXAM_CONTROLLER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                JSONArray response2;
                try{
                    response2=new JSONArray(response1);
                    for(int i=0;i<response2.length();i++)
                    {
                        JSONObject response=response2.getJSONObject(i);
                        Exam exam=new Exam(response.getString("exam_name"),response.getInt("id"),response.getInt("grade_template_id"));
                        exam.setStart_date(response.getString("start_date"));
                        exam.setEnd_date(response.getString("end_date"));
                        exams.add(exam);
                    }
                    RecyclerView examination_list=(RecyclerView)findViewById(R.id.exam_list_recycler_view);
                    examination_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    examination_list.setAdapter(new ExaminationListAdapter(exams,getApplicationContext()));
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error",error.toString());
                error.printStackTrace();
            }
        })
        {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = preferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + preferences.getInt(GlobalConstants.parent_selected_student, 0);
                    } else {
                        body = "id=" + Integer.valueOf(preferences.getString("id", null));
                    }
                }
                return body.getBytes();
            }
        };
        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(exam_list_request);
    }
    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
