package com.handysolver.smartclass;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.ParentStudentListAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.StudentModel;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParentStudentListActivity extends AppCompatActivity implements ParentStudentListAdapter.MyClickListener{
    SwipeRefreshLayout refreshLayout;
    ArrayList<Integer> selected_users = new ArrayList<>();
    ArrayList<String> selected_user_names = new ArrayList<>();
    ArrayList<StudentModel> studentModels = new ArrayList<>();
    private int grade_id;
    private int section_id;
    private String grade;
    Context context;
    private Boolean press_exit = false;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_student_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                selected_users.clear();
                selected_user_names.clear();
                studentModels.clear();
                sendRequest();
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selected_users.size()>0)
                {
                Intent intent = new Intent(ParentStudentListActivity.this,CreateMessageDialog.class);
                intent.putIntegerArrayListExtra("selected_users",selected_users);
                intent.putStringArrayListExtra("selected_user_names",selected_user_names);
                intent.putExtra("class",grade);
                startActivity(intent);
            }
            else
                {
                    Snackbar.make(view,"Select at least 1 recipient",Snackbar.LENGTH_LONG).show();
                }
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            grade_id = bundle.getInt("grade_id");
            section_id = bundle.getInt("section_id");
            grade = bundle.getString("class");
            getSupportActionBar().setTitle(grade);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        sendRequest();
    }

    public void sendRequest() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.PARENT_STUDENT_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONArray response2 = null;
                        try {
                            response2 = new JSONArray(response1);
                            for (int i = 0; i < response2.length(); i++) {
                                JSONObject response = response2.getJSONObject(i);
                                StudentModel studentModel = new StudentModel(response.getString("first_name") + " " + response.getString("last_name"), "");
                                studentModel.setId(response.getInt("id"));
                                if(response.has("guardian_id")) {
                                    studentModel.setParent_id(response.getInt("guardian_id"));
                                    studentModel.setParent_name(response.getString("guardian_first_name") + " " + response.getString("guardian_last_name"));
                                }
                                studentModels.add(studentModel);
                            }
                            RecyclerView contact_list = (RecyclerView) findViewById(R.id.parent_student_list);
                            contact_list.setAdapter(new ParentStudentListAdapter(context, studentModels));
                            contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            refreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        if (error.networkResponse != null) {
//                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
//                            try {
//                                JSONObject error_json = new JSONObject(error_message);
//                                if (error_json.has("status")) {
//                                    if (error_json.getInt("status") == 444) {
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                    }
//                                    if (error_json.getInt("status") == 498) {
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                    }
//                                    if (error_json.getInt("status") == 445) {
//                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                                        editor.clear();
//                                        editor.apply();
//                                        Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
                        refreshLayout.setRefreshing(false);
                        error.printStackTrace();
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {

//                        body = "id=" + getSharedPreferences(GlobalConstants.user_details_file_name,MODE_PRIVATE).getInt(GlobalConstants.parent_selected_student, 0);
                String body = "class_id=" + grade_id +"&section_id="+section_id;
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);
    }

    @Override
    public void add(int id, String name) {
        if(!selected_users.contains(id)) {
            selected_users.add(id);
        }
        if(!selected_user_names.contains(name))
        {
        selected_user_names.add(name);
    }}

    @Override
    public void remove(int id, String name) {
        selected_user_names.remove(name);
        selected_users.remove(Integer.valueOf(id));
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalConstants.should_logout) {
            Intent intent = new Intent(ParentStudentListActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
