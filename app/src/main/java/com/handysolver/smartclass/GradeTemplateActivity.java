package com.handysolver.smartclass;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.DateSheet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GradeTemplateActivity extends AppCompatActivity {

    ArrayList<DateSheet> grade_details = new ArrayList<>();
    SharedPreferences sharedPreferences;
    private LinearLayout scrollRelative;
    Button okay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade_template);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        scrollRelative=(LinearLayout) findViewById(R.id.scroll_relative_date_sheet);
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        setTitle("Loading....");
        okay = (Button)findViewById(R.id.ok);
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
//        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//        fab.setVisibility(View.GONE);

        if(getIntent().getExtras().get("id") != null)
        {
            sendRequest(getIntent().getExtras().getInt("id"));
        }
    }

    public void sendRequest(final int exam_id) {
        grade_details.clear();
        /*if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }*/
        StringRequest exam_list_request = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.GRADE_CONTROLLER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                DateSheet dateSheet=new DateSheet();
                JSONArray grade_detail;
                JSONArray num_exams;
                JSONObject response3;
                try {
                    response3 = new JSONObject(response1);
                    grade_detail = response3.getJSONArray("grade_detail");
                    setTitle(response3.getString("grade_scheme_name"));
                    for (int i = 0; i < grade_detail.length(); i++) {
                        JSONObject response = grade_detail.getJSONObject(i);
                        if(response.get("name").toString().length()>0){
                            dateSheet.setDate(response.get("name").toString());
                        }
                        if(response.get("from").toString().length()>0 && response.get("to").toString().length()>0){
                            dateSheet.setDay(response.get("from").toString()+"-"+response.get("to").toString());
                        }
                        if(response.get("comment").toString().length()>0){
                            dateSheet.setSubject(response.get("comment").toString());
                        }
                        setDateSheet(dateSheet);
                        if(i == grade_detail.length()-1)
                        {
                            okay.setVisibility(View.VISIBLE);
                        }
                    }
                    //setDateSheet();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*refreshLayout.setRefreshing(false);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /*refreshLayout.setRefreshing(false);*/
                /*String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                try {
                    JSONObject error_json = new JSONObject(error_message);
                    if (error_json.has("status")) {
                        if (error_json.getInt("status") == 444) {
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                        }
                        if (error_json.getInt("status") == 498) {
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                        }
                        if (error_json.getInt("status") == 445) {
                            *//*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*//*
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText(error_json.getString("message"));
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent = new Intent(ExaminationDetails.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                error.printStackTrace();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = sharedPreferences.getString("role", null);
                String body = "grade_id="+exam_id;
//                if (role != null) {
//                    if (role.equals(GlobalConstants.parent_role)) {
//                        body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0) + "&exam_id=" + exam_id;
//                    } else {
//                        body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null)) + "&exam_id=" + exam_id;
//                    }
//                }
                return body.getBytes();
            }
        };
        exam_list_request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(exam_list_request);
    }

    private void setDateSheet(DateSheet dateSheet){
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.grade_template_rows, null);
        if(dateSheet.getDate()!=null){
            TextView textView=(TextView) v.findViewById(R.id.date_view);
            textView.setText(dateSheet.getDate());
        }
        if(dateSheet.getDay()!=null){
            TextView textView=(TextView) v.findViewById(R.id.day_view);
            textView.setText(dateSheet.getDay());
        }
        if(dateSheet.getSubject()!=null){
            TextView textView=(TextView) v.findViewById(R.id.subject_view);
            textView.setText(dateSheet.getSubject());
        }

        // add view
        scrollRelative.addView(v);

    }

//    private void addButton()
//    {
//        Button ok = new Button(this);
//        ok.setText("O.K.");
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.gravity = Gravity.CENTER;
//        ok.setLayoutParams(layoutParams);
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
//        scrollRelative.addView(ok);
//    }



}
