package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONException;
import org.json.JSONObject;

public class TeacherDetails extends AppCompatActivity {

    String image_url="";
    SharedPreferences sharedPreferences;
    @Override
    public void onBackPressed() {
        Intent i=new Intent(TeacherDetails.this,TimeTableActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i=new Intent(TeacherDetails.this,TimeTableActivity.class);
        startActivity(i);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        final int teacher_id=bundle.getInt("id");
        String teaacher_name=bundle.getString("teacher_name");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_details);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(teaacher_name);
        sharedPreferences = getSharedPreferences(GlobalConstants.user_details_file_name,MODE_PRIVATE);
        TextView teacher_name=(TextView)findViewById(R.id.teacher_name);
        teacher_name.setText(teaacher_name);
        final RequestQueue queue= Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.TEACHER_DETAIL_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONObject response;
                        try {
                            response=new JSONObject(response1);
                            Log.d("r",response1);
                            TextView subject=(TextView)findViewById(R.id.subjects);
                            String subjects=response.getString("subjects").replace(",","\n");
                            subject.setText(subjects);
                            ImageView phone=(ImageView)findViewById(R.id.phone);
                            final TextView phone_number=(TextView)findViewById(R.id.phone_number);
                            phone_number.setText(response.getString("phone"));
                            image_url=response.getString("image");
                            final ImageView teacherImage=(ImageView)findViewById(R.id.teacher_image);
                            if(image_url.length()>1)
                            {
                                ImageRequest teacherImageRequest=new ImageRequest(image_url, new Response.Listener<Bitmap>()
                                {
                                    @Override
                                    public void onResponse(Bitmap response) {
                                        teacherImage.setImageBitmap(response);
                                    }
                                }, 0, 0, null, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error)
                                    {
                                        teacherImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                                    }
                                });
                                queue.add(teacherImageRequest);
                            }
                            else
                            {
                                teacherImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                            }
                            phone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent=new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:"+phone_number.getText()));
                                    startActivity(intent);
                                }
                            });
                            final TextView email_id=(TextView)findViewById(R.id.mail_id);
                            email_id.setText(response.getString("email"));
                            ImageView mail=(ImageView)findViewById(R.id.mail);
                            mail.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i=new Intent(Intent.ACTION_SENDTO);
                                    i.setData(Uri.parse("mailto:"+email_id.getText()));
//                i.putExtra(Intent.EXTRA_EMAIL,email_id.getText());
//                i.setType("message/rfc822");
                                    startActivity(i);
                                }
                            });

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        Log.e("error",error.toString());
                        error.printStackTrace();
                    }
                })
        {


            @Override
            public byte[] getBody() throws AuthFailureError
            {
                String body="id="+teacher_id;
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
        }
    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
