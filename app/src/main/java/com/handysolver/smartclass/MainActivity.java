package com.handysolver.smartclass;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.handysolver.smartclass.adapters.StudentAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.fragments.AttendanceMainActivity;
import com.handysolver.smartclass.fragments.HolidayFragment;
import com.handysolver.smartclass.models.StudentModel;
import com.handysolver.smartclass.services.AutoLogout;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

/*The default activity that opens upon successful login*/
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, StudentAdapter.MyClickListener {
    private MainActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    SwipeRefreshLayout refreshLayout;
    RequestQueue queue;
    private ArrayList<StudentModel> studentModels = new ArrayList<>();
    private RecyclerView.Adapter studentAdapter;
    private Context context;
    TextView select_student;
    NavigationView navigationView;
    DrawerLayout drawer;
    Bitmap bitmap;
    DatabaseHelper helper;
    SharedPreferences sharedPreferences = null;
    private Boolean press_exit;
    String role = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        helper = new DatabaseHelper(getApplicationContext(), null, null, 1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");
        Fabric.with(this, new Crashlytics());
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        role = sharedPreferences.getString("role", null);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        queue = Volley.newRequestQueue(this);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        context = this;
//        refreshLayout.setRefreshing(true);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                if (role.equals(GlobalConstants.student_role)) {
//                    refreshLayout.);
//                }
                if (role.equals(GlobalConstants.parent_role)) {
//                    studentModels.clear();
                    sendRequest();
                }
                if (role.equals(GlobalConstants.teacher_role) || role.equals(GlobalConstants.admin_role)) {
//                    studentModels.clear();
                    refreshLayout.setRefreshing(false);
                }
            }
        });
        if (role.equals(GlobalConstants.student_role)) {
            refreshLayout.setVisibility(View.GONE);
            setupTabs();
        }
        if (role.equals(GlobalConstants.parent_role)) {
            refreshLayout.setVisibility(View.VISIBLE);
            tabLayout.setVisibility(View.GONE);
            sendRequest();
        }
        press_exit = false;
        select_student = (TextView) findViewById(R.id.select_student);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
//                Intent i=new Intent(MainActivity.this,Test.class);
//                startActivity(i);
//            }
//        });
        /*This part handles the drawer layout and it's contents*/
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        /*Here we are setting the header of the drawer to display user name and full name*/
        TextView user = (TextView) header.findViewById(R.id.user_name);
        TextView student_class = (TextView) header.findViewById(R.id.student_class);
        final ImageView profile_picture = (ImageView) header.findViewById(R.id.imageView);
        /*The below code is used to find the role of the logged in user and set the menu items accordingly*/
        if (sharedPreferences != null) {
            String image_url = sharedPreferences.getString("image_url", null);
            if (image_url != null) {
                ImageRequest displayPictureRequest = new ImageRequest(image_url, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(getResources(), response);
                        dr.setCircular(true);
                        profile_picture.setImageDrawable(dr);
                    }
                }, 0, 0, null, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Drawable drawable = getResources().getDrawable(R.drawable.sdgf);
                        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                        dr.setCircular(true);
                        profile_picture.setImageDrawable(dr);
                    }
                });
                Volley.newRequestQueue(this).add(displayPictureRequest);
            }
            if (role != null) {
                if (role.equals(GlobalConstants.student_role)) {
                    navigationView.getMenu().setGroupVisible(R.id.teacher_menu, false);
                    navigationView.getMenu().setGroupVisible(R.id.parent_menu, false);
                }
                if (role.equals(GlobalConstants.parent_role)) {
                    navigationView.getMenu().setGroupVisible(R.id.teacher_menu, false);
                    navigationView.getMenu().setGroupVisible(R.id.student_menu, false);
                    String student_first_name = sharedPreferences.getString(GlobalConstants.parent_selected_student_name, null);
                    if (student_first_name != null) {
//                    navigationView.getMenu().getItem(R.id.nav_student_timetable).setTitle(sharedPreferences.getString(GlobalConstants.parent_selected_student_name,null)+" "+navigationView.getMenu().getItem(R.id.nav_student_timetable).getTitle());
                        navigationView.getMenu().setGroupVisible(R.id.parent_student_menu, true);
                        select_student.setText(student_first_name + " selected");
                        MenuItem timetable_item = navigationView.getMenu().findItem(R.id.nav_student_timetable);
                        MenuItem attendance_item = navigationView.getMenu().findItem(R.id.nav_student_attendance);
                        MenuItem test_item = navigationView.getMenu().findItem(R.id.nav_student_test);
                        MenuItem exam_item = navigationView.getMenu().findItem(R.id.nav_student_exam);
                        MenuItem assignment_item = navigationView.getMenu().findItem(R.id.nav_student_assignment);
                        MenuItem transport_item = navigationView.getMenu().findItem(R.id.nav_transport_parent_student);
                        test_item.setTitle(student_first_name + "'s " + "Test");
                        timetable_item.setTitle(student_first_name + "'s " + "Timetable");
                        attendance_item.setTitle(student_first_name + "'s " + "Attendance");
                        exam_item.setTitle(student_first_name + "'s " + "Exams");
                        assignment_item.setTitle(student_first_name + "'s " + "Assignments");
                        transport_item.setTitle(student_first_name + "'s " + "Transport");
//                    navigationView.getMenu().findItem(R.id.nav_student_attendance).setTitle(sharedPreferences.getString(GlobalConstants.parent_selected_student_name,null)+" "+navigationView.getMenu().findItem(R.id.nav_student_attendance).getTitle());
//                    navigationView.getMenu().findItem(R.id.nav_student_test).setTitle(sharedPreferences.getString(GlobalConstants.parent_selected_student_name,null)+" "+navigationView.getMenu().findItem(R.id.nav_student_test).getTitle());
//                    navigationView.getMenu().findItem(R.id.nav_student_assignment).setTitle(sharedPreferences.getString(GlobalConstants.parent_selected_student_name,null)+" "+navigationView.getMenu().findItem(R.id.nav_student_assignment).getTitle());
//                    navigationView.getMenu().findItem(R.id.nav_student_exam).setTitle(sharedPreferences.getString(GlobalConstants.parent_selected_student_name,null)+" "+navigationView.getMenu().getItem(R.id.nav_student_exam).getTitle());
                    }
                }
                if (role.equals(GlobalConstants.teacher_role) || role.equals(GlobalConstants.admin_role)) {
                    ScrollView teacherAdminMenu = (ScrollView) findViewById(R.id.teacher_admin_menu);
                    teacherAdminMenu.setVisibility(View.VISIBLE);
                    navigationView.getMenu().setGroupVisible(R.id.student_menu, false);
                    navigationView.getMenu().setGroupVisible(R.id.parent_menu, false);
                    tabLayout.setVisibility(View.GONE);
                    refreshLayout.setVisibility(View.GONE);
                    Button home = (Button) findViewById(R.id.home_button);
                    Button message = (Button) findViewById(R.id.messages);
                    Button notice = (Button) findViewById(R.id.noticeboard_button);
                    Button transport = (Button) findViewById(R.id.transport_button);
                    Button holiday = (Button) findViewById(R.id.holiday_button);
                    Button settings = (Button) findViewById(R.id.settings_button);
                    Button logout = (Button) findViewById(R.id.logout_button);
                    if (role.equals(GlobalConstants.admin_role)) {
                        Menu nav_menu = navigationView.getMenu();
                        nav_menu.findItem(R.id.nav_teacher_chat).setVisible(false);
                        message.setVisibility(View.GONE);
                    }
                    home.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(MainActivity.this, FullscreenWebViewActivity.class);
                            startActivity(i);
                        }
                    });
                    message.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (role.equals(GlobalConstants.teacher_role)) {
                                Intent i = new Intent(MainActivity.this, Inbox.class);
                                startActivity(i);
                            }
                        }
                    });
                    notice.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(MainActivity.this, NoticeBoardActivity.class);
                            startActivity(i);
                        }
                    });
                    transport.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(MainActivity.this, TransportActivity.class);
                            startActivity(i);
                        }
                    });
                    holiday.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(MainActivity.this, Holiday.class);
                            startActivity(i);
                        }
                    });
                    settings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(MainActivity.this, ExamSettings.class);
                            startActivity(i);
                        }
                    });
                    logout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.apply();
                            OneSignal.setSubscription(false);
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
            }
            if (sharedPreferences.getString("name", null) != null) {
                user.setText(sharedPreferences.getString("name", null));
                student_class.setText(sharedPreferences.getString("class", null));
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (!AppLifecycleHandler.isApplicationInForeground()) {
//            AutoLogout.timer.cancel();
//        }
//    }

    @Override
    protected void onResume() {
//        String s = "*WT 2017 02 13 1 13:12:14 ";
        super.onResume();
//        int sum = 0;
//        int length;
//        int i;
//        char[]src = s.toCharArray();
//        length = src.length;
//        for(i = 0; i < length ; i++)
//        {
//            sum += src[i];
//            if(sum>256)
//            {
//                sum=sum-256;
//            }
//        }
//        Log.d("total checksum sum=",sum+"");

//
//        long sum = 0;
//        int len = s.length();
//        for (int x = 0; x < len; x = x + 1) {
//            sum = sum + s.charAt(x);
//            if (sum >= 256) {
//                sum = sum - 256;
//            }
//        }
//        Log.d("sum", sum + "");
        if (GlobalConstants.should_logout) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }

//    public void removeDates()
//    {
//        if(getSupportFragmentManager().getFragments()!=null)
//        {
//            Fragment graph_fragment = getSupportFragmentManager().getFragments().get(0);
//            TextView start_date=(TextView)graph_fragment.getView().findViewById(R.id.start_date);
//            TextView end_date=(TextView)graph_fragment.getView().findViewById(R.id.end_date);
//            ImageView start_date_picker=(ImageView)graph_fragment.getView().findViewById(R.id.start_date_picker);
//            ImageView end_date_picker=(ImageView)graph_fragment.getView().findViewById(R.id.end_date_picker);
//            Button send_request=(Button)graph_fragment.getView().findViewById(R.id.send_request);
//            start_date.setVisibility(View.GONE);
//            end_date.setVisibility(View.GONE);
//            start_date_picker.setVisibility(View.GONE);
//            end_date_picker.setVisibility(View.GONE);
//            send_request.setVisibility(View.GONE);
//        }
//    }

    public void setupTabs() {
        mSectionsPagerAdapter = new MainActivity.SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean("holiday")) {
                mViewPager.setCurrentItem(1);
            }
        }
        tabLayout.setupWithViewPager(mViewPager);
        refreshLayout.setRefreshing(false);
//        removeDates();
    }

    public void sendRequest() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL + GlobalConstants.STUDENT_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        studentModels.clear();
                        JSONArray response2;
                        JSONObject response;
                        try {
                            response2 = new JSONArray(response1);
                            for (int i = 0; i < response2.length(); i++) {
                                response = response2.getJSONObject(i);
                                String name = "";
                                if (response.getString("middlename").length() > 1) {
                                    name = response.getString("firstname") + " " + response.getString("middlename") + " " + response.getString("lastname");
                                } else {
                                    name = response.getString("firstname") + " " + response.getString("middlename") + response.getString("lastname");
                                }
                                String grade = response.getString("class") + response.getString("section");
                                final StudentModel studentModel = new StudentModel(name, grade);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
//                                if (sharedPreferences.getString("all_student_ids", null) != null) {
//                                    String[] split_ids = sharedPreferences.getString("all_student_ids", "").split(" ");
//                                    for (int j = 0; j < split_ids.length; j++) {
//                                        if (String.valueOf(response.getInt("id")).equals(split_ids[j])) {
//                                            break;
//                                        }
//                                        if (!String.valueOf(response.getInt("id")).equals(split_ids[j])) {
//                                            if (j == split_ids.length - 1) {
//                                                editor.putString("all_student_ids", sharedPreferences.getString("all_student_ids", "").concat(" " + response.getInt("id")));
//                                            }
//                                        }
//                                    }
//
//                                } else {
//                                    editor.putString("all_student_ids", "" + response.getInt("id"));
//                                }
                                editor.apply();
                                studentModel.setId(response.getInt("id"));
                                ImageRequest teacherImageRequest = new ImageRequest(response.getString("avatar_base_url") + "/" + response.getString("avatar_path"), new Response.Listener<Bitmap>() {
                                    @Override
                                    public void onResponse(Bitmap response) {
                                        studentModel.setDisplay_picture(response);
                                        studentModels.add(studentModel);
                                        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.parent_student_relative_layout);
                                        relativeLayout.setVisibility(View.VISIBLE);
                                        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.student_recycler_view);
                                        studentAdapter = new StudentAdapter(studentModels, context);
                                        recyclerView.setAdapter(studentAdapter);
                                        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                                        refreshLayout.setRefreshing(false);
                                    }
                                }, 0, 0, null, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        studentModels.add(studentModel);
                                        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.parent_student_relative_layout);
                                        relativeLayout.setVisibility(View.VISIBLE);
                                        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.student_recycler_view);
                                        studentAdapter = new StudentAdapter(studentModels, context);
                                        recyclerView.setAdapter(studentAdapter);
                                        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                                        refreshLayout.setRefreshing(false);
//                                        Drawable drawable = getResources().getDrawable(R.drawable.sdgf);
//                                        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
//                                        studentModel.setDisplay_picture(bitmap);
                                    }
                                });
                                queue.add(teacherImageRequest);
                                studentModel.setDisplay_picture(bitmap);

//                                ((StudentAdapter)studentAdapter).setOnItemClickListener(new StudentAdapter.MyClickListener() {
//                                    @Override
//                                    public void onItemClick(int position, View v) {
//                                        Log.d("poi", "onItemClick: ");
//                                    }
//                                });

                            }

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        Log.e("error", error.toString());
                        error.printStackTrace();
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                String body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    public void forceCrash(View view) {
        Log.d("a", "forceCrash: ");
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        /*used to direct the user to different activities based on the option chosen in the drawer menu*/
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_inbox || id == R.id.parent_messages) {
            Intent i = new Intent(MainActivity.this, Inbox.class);
            startActivity(i);
        } else if (id == R.id.nav_timetable) {
            Intent i = new Intent(MainActivity.this, TimeTableActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_attendance) {
            Intent i = new Intent(MainActivity.this, AttendanceTabs.class);
            startActivity(i);
        } else if (id == R.id.nav_test) {
            Intent intent = new Intent(MainActivity.this, Test.class);
            startActivity(intent);
        } else if (id == R.id.nav_exam) {
            Intent intent = new Intent(MainActivity.this, Examination.class);
            startActivity(intent);
        } else if (id == R.id.nav_assignment) {
            Intent intent = new Intent(MainActivity.this, Assignment.class);
            startActivity(intent);
        } else if (id == R.id.nav_student_test) {
            Intent intent = new Intent(MainActivity.this, Test.class);
            startActivity(intent);
        } else if (id == R.id.nav_student_exam) {
            Intent intent = new Intent(MainActivity.this, Examination.class);
            startActivity(intent);
        } else if (id == R.id.nav_student_assignment) {
            Intent intent = new Intent(MainActivity.this, Assignment.class);
            startActivity(intent);
        } else if (id == R.id.nav_student_timetable) {
            Intent i = new Intent(MainActivity.this, TimeTableActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_student_attendance) {
            Intent i = new Intent(MainActivity.this, AttendanceTabs.class);
            startActivity(i);
        } else if (id == R.id.teacher_home) {
            Intent i = new Intent(MainActivity.this, FullscreenWebViewActivity.class);
            startActivity(i);
        }
//        else if (id == R.id.nav_share)
//        {
//
//        }
        else if (id == R.id.nav_parent_notice_board) {
            Intent i = new Intent(MainActivity.this, NoticeBoardActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_holiday || id == R.id.nav_parent_holiday || id == R.id.nav_teacher_holiday) {
            Intent i = new Intent(MainActivity.this, Holiday.class);
            startActivity(i);
        } else if (id == R.id.nav_teacher_notice_board) {
            Intent i = new Intent(MainActivity.this, NoticeBoardActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_notice_board) {
            Intent i = new Intent(MainActivity.this, NoticeBoardActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            OneSignal.setSubscription(false);
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_parent_logout) {
            helper.deleteAll();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            OneSignal.setSubscription(false);
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_teacher_logout) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            OneSignal.setSubscription(false);
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_teacher_chat) {
            if (role.equals(GlobalConstants.teacher_role)) {
                Intent i = new Intent(MainActivity.this, Inbox.class);
                startActivity(i);
            }
        } else if (id == R.id.nav_transport_parent_student || id == R.id.nav_transport_student || id == R.id.nav_transport) {
            Intent i = new Intent(MainActivity.this, TransportActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_parent_manage || id == R.id.nav_manage || id == R.id.nav_teacher_manage) {
            Intent i = new Intent(MainActivity.this, ExamSettings.class);
            startActivity(i);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(int position, String name, View view) {
        String[] names = name.split(" ");
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences.getInt(GlobalConstants.parent_selected_student, -1) != position) {
            editor.putString(GlobalConstants.parent_selected_student_name, names[0]);
            editor.putInt(GlobalConstants.parent_selected_student, position);
            editor.apply();
            select_student.setText(names[0] + " selected");
            navigationView.getMenu().setGroupVisible(R.id.parent_student_menu, true);
            MenuItem timetable_item = navigationView.getMenu().findItem(R.id.nav_student_timetable);
            MenuItem attendance_item = navigationView.getMenu().findItem(R.id.nav_student_attendance);
            MenuItem test_item = navigationView.getMenu().findItem(R.id.nav_student_test);
            MenuItem exam_item = navigationView.getMenu().findItem(R.id.nav_student_exam);
            MenuItem assignment_item = navigationView.getMenu().findItem(R.id.nav_student_assignment);
            MenuItem transport_item = navigationView.getMenu().findItem(R.id.nav_transport_parent_student);
            test_item.setTitle(names[0] + "'s " + "Test");
            timetable_item.setTitle(names[0] + "'s " + "Timetable");
            attendance_item.setTitle(names[0] + "'s " + "Attendance");
            exam_item.setTitle(names[0] + "'s " + "Exams");
            assignment_item.setTitle(names[0] + "'s " + "Assignments");
            transport_item.setTitle(names[0] + "'s " + "Transport");
            drawer.openDrawer(Gravity.LEFT);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new AttendanceMainActivity();
                case 1:
                    return new HolidayFragment();

            }
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "ATTENDANCE";
                case 1:
                    return "HOLIDAYS";
            }
            return null;
        }
    }
}
