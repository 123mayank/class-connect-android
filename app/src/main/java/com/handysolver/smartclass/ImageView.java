package com.handysolver.smartclass;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Time Table");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RequestQueue queue = Volley.newRequestQueue(this);
        final android.widget.ImageView imageView = (android.widget.ImageView)findViewById(R.id.image_view);
        if(getIntent().getExtras()!=null)
        {
            String image = getIntent().getExtras().getString("image");
//            imageView.setImageBitmap(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length));
            ImageRequest teacherImageRequest = new ImageRequest(image, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(final Bitmap response) {

//                    if (relativeLayout.getVisibility() != View.VISIBLE) {
//                        relativeLayout.setVisibility(View.VISIBLE);
//                    }
                    imageView.setImageBitmap(response);
//                    imageView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                            response.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                            byte[] byteArray = stream.toByteArray();
//
//                            Intent in1 = new Intent(TimeTableActivity.this, com.handysolver.smartclass.ImageView.class);
//
//                            in1.putExtra("image",byteArray);
//                            startActivity(in1);
//                        }
//                    });


                }

            }, 0, 0, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                    PhotoViewAttacher attacher = new PhotoViewAttacher(imageView);
                    attacher.update();
                    attacher.setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
                }
            });
            queue.getCache().clear();
            queue.add(teacherImageRequest);

            PhotoViewAttacher attacher = new PhotoViewAttacher(imageView);
            attacher.update();
            attacher.setScaleType(android.widget.ImageView.ScaleType.CENTER_INSIDE);
        }

    }
}
