package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.InboxAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Message;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Inbox extends AppCompatActivity {
    DatabaseHelper helper;
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    SwipeRefreshLayout refreshLayout;
    SharedPreferences preferences;
    private Boolean press_exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        helper = new DatabaseHelper(this, null, null, 1);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sendRequest(GlobalConstants.inbox);
                sendRequest(GlobalConstants.outbox);
                setupView();
            }
        });
        refreshLayout.setRefreshing(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Inbox.this, ChatActivity.class);
                startActivity(intent);
            }
        });

        FloatingActionButton fab_delete = (FloatingActionButton) findViewById(R.id.delete);
        fab_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Inbox.this, DeleteMessagesDialog.class);
                startActivity(intent);
            }
        });
        preferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        press_exit = false;
        sendRequest(GlobalConstants.inbox);
        sendRequest(GlobalConstants.outbox);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalConstants.should_logout) {
            Intent intent = new Intent(Inbox.this, LoginActivity.class);
            startActivity(intent);
        }
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        Intent intent=new Intent(Inbox.this,MainActivity.class);
//        startActivity(intent);
//    }

    public void check(Message m) {
        if (!helper.CheckIsDataAlreadyInDBorNot(m.getTime(),m.getSender())) {
            helper.addMessage(m.getTopic(), m.getSender(), m.getTime(), m.getBody(), m.getType());
        }
    }

    public void setupView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.inbox_messages);
        recyclerView.setLayoutManager(layoutManager);
        InboxAdapter adapter = new InboxAdapter(helper.getTable(), this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        refreshLayout.setRefreshing(false);
    }

    public void sendRequest(final String inbox_outbox)
    {
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.INBOX_CONTROLLER+inbox_outbox,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONArray response2 = null;
                        try {
                            response2 = new JSONArray(response1);
//                            float k = 0.0f;
                            for (int i = 0; i < response2.length(); i++) {
//                                String marks = "";
//                                String max_marks = "";
//                                int highest_marks = 0;
//                                int average_marks = 0;
                                JSONObject response = response2.getJSONObject(i);
                                if(inbox_outbox.equals(GlobalConstants.inbox)) {
                                    Message message = new Message("", response.getString("from_user_name"), response.getString("date"), response.getString("message"), GlobalConstants.personal_message_label_string);
                                    check(message);
                                }
                                if(inbox_outbox.equals(GlobalConstants.outbox)) {
                                    Message message = new Message("", "->"+response.getString("to_user_name"), response.getString("date"), response.getString("message"), GlobalConstants.personal_message_label_string);
                                    check(message);
                                }
//                                String start_time = response.getString("time_from");
//                                start_time = start_time.substring(0, 5);
//                                String end_time = response.getString("time_to");
//                                end_time = end_time.substring(0, 5);
//                                String exam_time = start_time + "-" + end_time;

                            }
                            setupView();
                            refreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        if (error.networkResponse.data != null) {
//                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
//                            try {
//                                JSONObject error_json = new JSONObject(error_message);
//                                if (error_json.has("status")) {
//                                    if (error_json.getInt("status") == 444) {
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                        refreshLayout.setRefreshing(false);
//                                    }
//                                    if (error_json.getInt("status") == 498) {
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                        refreshLayout.setRefreshing(false);
//                                    }
//                                    if (error_json.getInt("status") == 445) {
//                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                        SharedPreferences sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                                        editor.clear();
//                                        editor.apply();
//                                        Intent intent = new Intent(NoticeBoardActivity.this, LoginActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        } else {
                        refreshLayout.setRefreshing(false);
//                            TextView error_view = (TextView) findViewById(R.id.error_view);
//                            error_view.setVisibility(View.VISIBLE);
//                            error_view.setText("Error");
//                        }

                        error.printStackTrace();
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
//                String role = preferences.getString("role", null);
                String body = "";
                body = "id=" + Integer.valueOf(preferences.getString("id", null));
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);
    }

}
