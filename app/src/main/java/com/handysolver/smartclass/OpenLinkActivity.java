package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.TransportAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Transport;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/*This class handles the examination module, We are populating an array list of type Exam, the class of which can be found in the models folder. Next the list is passed to the ExaminationListAdapter, which takes care of populating the recycler view with the list*/
public class OpenLinkActivity extends AppCompatActivity {
    SharedPreferences preferences;
    SwipeRefreshLayout refreshLayout;
    private ArrayList<Transport> transportList = new ArrayList<>();
    private RecyclerView transportListRecyclerview;
    private TransportAdapter transportAdapter;
    private String routeLink, title;
    private Boolean press_exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_link);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        preferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        Intent intent = getIntent();

        Bundle extras = intent.getExtras();
        routeLink = extras.getString(GlobalConstants.ROUTE_LINK);
        title = extras.getString(GlobalConstants.ROUTE_TITLE);

        getSupportActionBar().setTitle(title);
        WebView browser = (WebView) findViewById(R.id.web_view);
        browser.setWebViewClient(new WebViewClient());
        browser.getSettings().setJavaScriptEnabled(true);
        //browser.loadUrl("http://dealsinbongo.com/smartclass/backend/web/transport/route-view?id=20");
        browser.loadUrl(routeLink);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                if (AppLifecycleHandler.isApplicationInForeground()) {
                    AutoLogout.timer.cancel();
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }


}
