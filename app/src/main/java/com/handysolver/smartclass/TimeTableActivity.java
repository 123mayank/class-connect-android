package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.SlotAdapter;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.TimeTableSlot;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;

public class TimeTableActivity extends AppCompatActivity {
    /*The ArrayLists hold classes for individual days, which are passed onto the SlotAdapter constructor as parameter when the timetable is not in image format
     * Note that ArrayList is of type TimeTable slot which acts as a bean class here which stores the subject,teacher name, time, colors(background and font) and teacher-id of a class */
    ArrayList<TimeTableSlot> monday = new ArrayList<>();
    ArrayList<TimeTableSlot> tuesday = new ArrayList<>();
    ArrayList<TimeTableSlot> wednesday = new ArrayList<>();
    ArrayList<TimeTableSlot> thursday = new ArrayList<>();
    ArrayList<TimeTableSlot> friday = new ArrayList<>();
    ArrayList<TimeTableSlot> saturday = new ArrayList<>();
    ArrayList<TimeTableSlot> sunday = new ArrayList<>();
    SwipeRefreshLayout refreshLayout;
    /*If the timetable is in image format, relative layout is displayed. Otherwise, layout and layout 2 are displayed*/
    RelativeLayout relativeLayout;
//    NestedScrollView timetable;
    RelativeLayout layout2;
    SharedPreferences sharedPreferences;
    @Override
    public void onBackPressed() {
        Intent i = new Intent(TimeTableActivity.this, MainActivity.class);
        startActivity(i);
    }
    Boolean should_scroll = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Time Table");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        relativeLayout = (RelativeLayout) findViewById(R.id.image_relative_layout);
//        timetable = (NestedScrollView)findViewById(R.id.timetable_scroll_view);
//        layout = (LinearLayout) findViewById(R.id.days_text);
        layout2 = (RelativeLayout) findViewById(R.id.recycler_relative);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        /*This method is used to send the API request, parse the response and populate the views*/
        sendRequest();
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /*Note that we have to clear the array lists here else they still contain the classes that were added to them before refreshing*/
                monday.clear();
                tuesday.clear();
                wednesday.clear();
                thursday.clear();
                friday.clear();
                saturday.clear();
                sunday.clear();
                sendRequest();
            }
        });
//        timetable.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                return should_scroll;
//            }
//        });
//        final ImageView timetable_image=(ImageView)findViewById(R.id.timetable_image);
//        final RequestQueue queue=Volley.newRequestQueue(this);
//        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://dealsinbongo.com/smartclass/api/web/v1/time-tables/get-timetable",
//               new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response1) {
//                        JSONObject response = null;
//                        try {
//                            response = new JSONObject(response1);
//                            if(response.has("timetable_image"))
//                            {
//                                ImageRequest teacherImageRequest=new ImageRequest(response.getString("timetable_image"), new Response.Listener<Bitmap>()
//                                {
//                                    @Override
//                                    public void onResponse(final Bitmap response)
//                                    {
//                                        LinearLayout layout=(LinearLayout)findViewById(R.id.days_text);
//                                        RelativeLayout layout2=(RelativeLayout)findViewById(R.id.recycler_relative);
//                                        layout.setVisibility(View.GONE);
//                                        layout2.setVisibility(View.GONE);
//                                        timetable_image.setImageBitmap(response);
//                                        PhotoViewAttacher attacher=new PhotoViewAttacher(timetable_image);
//                                        attacher.update();
//                                        Button gallery_button=(Button)findViewById(R.id.gallery_button);
//                                        gallery_button.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view)
//                                            {
////                                                String path = "/Android/data/com.handysolver.smartclass" + "/images";
////
////                                                File dir = new File(path);
////                                                if(!dir.exists())
////                                                    dir.mkdirs();
////                                                File file = new File(dir, "timetable.jpg");
////                                                if(file.exists())
////                                                {
////                                                    Boolean del=file.delete();
////                                                }
////                                                OutputStream os = null;
////                                                try {
////                                                    os = new BufferedOutputStream(new FileOutputStream(file));
////                                                    response.compress(Bitmap.CompressFormat.JPEG,100,os);
////                                                    os.close();
////                                                } catch (IOException e) {
////                                                    e.printStackTrace();
////                                                }
//                                                Intent galleryIntent = new Intent(Intent.ACTION_VIEW, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                                                galleryIntent.setDataAndType(Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(),response,"timetable_image","image of timetable")), "image/*");
//                                                galleryIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                                startActivity(galleryIntent);
//                                            }
//                                        });
//                                    }
//                                }, 0, 0, null, new Response.ErrorListener() {
//                                    @Override
//                                    public void onErrorResponse(VolleyError error)
//                                    {
//                                        timetable_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
//                                    }
//                                });
//                                queue.getCache().clear();
//                                queue.add(teacherImageRequest);
//                            }
//                            else {
//                                RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.image_relative_layout);
//                                relativeLayout.setVisibility(View.GONE);
//                                JSONArray _monday = response.getJSONArray("Monday");
//                                if(_monday.length()>0)
//                                {
//                                    for (int i = 0; i < _monday.length(); i++) {
//                                        JSONObject slot = _monday.getJSONObject(i);
//                                        monday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
//                                    }
//                                    RecyclerView gridViewMonday = (RecyclerView) findViewById(R.id.gridview);
//                                    SlotAdapter mondayAdapter = new SlotAdapter(getApplicationContext(), monday);
//                                    gridViewMonday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewMonday.setAdapter(mondayAdapter);
//                                }
//                                else
//                                {
//                                    monday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
//                                    RecyclerView gridViewMonday = (RecyclerView) findViewById(R.id.gridview);
//                                    SlotAdapter mondayAdapter = new SlotAdapter(getApplicationContext(), monday);
//                                    gridViewMonday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewMonday.setAdapter(mondayAdapter);
////                                TextView empty_monday=(TextView)findViewById(R.id.empty_monday);
////                                empty_monday.setVisibility(View.VISIBLE);
//                                }
//                                JSONArray _tuesday = response.getJSONArray("Tuesday");
//                                if(_tuesday.length()>0)
//                                {
//                                    for (int i = 0; i < _tuesday.length(); i++)
//                                    {
//                                        JSONObject slot = _tuesday.getJSONObject(i);
//                                        tuesday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
//                                    }
//                                    RecyclerView gridViewTuesday = (RecyclerView) findViewById(R.id.gridviewTuesday);
//                                    SlotAdapter tuesdayAdapter = new SlotAdapter(getApplicationContext(), tuesday);
//                                    gridViewTuesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewTuesday.setAdapter(tuesdayAdapter);
//                                } else
//                                {
//                                    tuesday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
//                                    RecyclerView gridViewTuesday = (RecyclerView) findViewById(R.id.gridviewTuesday);
//                                    SlotAdapter tuesdayAdapter = new SlotAdapter(getApplicationContext(), tuesday);
//                                    gridViewTuesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewTuesday.setAdapter(tuesdayAdapter);
////                                TextView empty_tuesday=(TextView)findViewById(R.id.empty_tuesday);
////                                empty_tuesday.setVisibility(View.VISIBLE);
//                                }
//                                    JSONArray _wednesday = response.getJSONArray("Wednesday");
//                                if(_wednesday.length()>0)
//                                {
//                                for (int i = 0; i < _wednesday.length(); i++) {
//                                        JSONObject slot = _wednesday.getJSONObject(i);
//                                        wednesday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
//                                    }
//                                    RecyclerView gridViewWednesday = (RecyclerView) findViewById(R.id.gridviewWednesday);
//                                    SlotAdapter wednesdayAdapter = new SlotAdapter(getApplicationContext(), wednesday);
//                                    gridViewWednesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewWednesday.setAdapter(wednesdayAdapter);
//                                } else {
//                                    wednesday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
//                                    RecyclerView gridViewWednesday = (RecyclerView) findViewById(R.id.gridviewWednesday);
//                                    SlotAdapter wednesdayAdapter = new SlotAdapter(getApplicationContext(), wednesday);
//                                    gridViewWednesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewWednesday.setAdapter(wednesdayAdapter);
//
////                                TextView empty_wednesday=(TextView)findViewById(R.id.empty_wednesday);
////                                empty_wednesday.setVisibility(View.VISIBLE);
//                                }
//                                   JSONArray _thursday = response.getJSONArray("Thursday");
//                                    if(_thursday.length()>0)
//                                    {
//                                    for (int i = 0; i < _thursday.length(); i++) {
//                                        JSONObject slot = _thursday.getJSONObject(i);
//                                        thursday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
//                                    }
//                                    RecyclerView gridViewThursday = (RecyclerView) findViewById(R.id.gridviewThursday);
//                                    SlotAdapter thursdayAdapter = new SlotAdapter(getApplicationContext(), thursday);
//                                    gridViewThursday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewThursday.setAdapter(thursdayAdapter);
//                                } else {
//                                    thursday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
//                                    RecyclerView gridViewThursday = (RecyclerView) findViewById(R.id.gridviewThursday);
//                                    SlotAdapter thursdayAdapter = new SlotAdapter(getApplicationContext(), thursday);
//                                    gridViewThursday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewThursday.setAdapter(thursdayAdapter);
////                              TextView empty_thursday=(TextView)findViewById(R.id.empty_thursday);
////                                empty_thursday.setVisibility(View.VISIBLE);
//                                }
//                                 JSONArray _friday = response.getJSONArray("Friday");
//                                if(_friday.length()>0)
//                                {
//                                    for (int i = 0; i < _friday.length(); i++) {
//                                        JSONObject slot = _friday.getJSONObject(i);
//                                        friday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
//                                    }
//                                    RecyclerView gridViewFriday = (RecyclerView) findViewById(R.id.gridviewFriday);
//                                    SlotAdapter fridayAdapter = new SlotAdapter(getApplicationContext(), friday);
//                                    gridViewFriday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewFriday.setAdapter(fridayAdapter);
//                                } else {
//                                    friday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
//                                    RecyclerView gridViewFriday = (RecyclerView) findViewById(R.id.gridviewFriday);
//                                    SlotAdapter fridayAdapter = new SlotAdapter(getApplicationContext(), friday);
//                                    gridViewFriday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewFriday.setAdapter(fridayAdapter);
////                                TextView empty_friday=(TextView)findViewById(R.id.empty_friday);
////                                empty_friday.setVisibility(View.VISIBLE);
//                                }
//                                JSONArray _saturday = response.getJSONArray("Saturday");
//                                    if(_saturday.length()>0)
//                                    {
//                                    for (int i = 0; i < _saturday.length(); i++) {
//                                        JSONObject slot = _saturday.getJSONObject(i);
//                                        saturday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
//                                    }
//                                    RecyclerView gridViewSaturday = (RecyclerView) findViewById(R.id.gridviewSaturday);
//                                    SlotAdapter saturdayAdapter = new SlotAdapter(getApplicationContext(), saturday);
//                                    gridViewSaturday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewSaturday.setAdapter(saturdayAdapter);
//                                } else {
//                                    saturday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
//                                    RecyclerView gridViewSaturday = (RecyclerView) findViewById(R.id.gridviewSaturday);
//                                    SlotAdapter saturdayAdapter = new SlotAdapter(getApplicationContext(), saturday);
//                                    gridViewSaturday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewSaturday.setAdapter(saturdayAdapter);
////                                TextView empty_saturday=(TextView)findViewById(R.id.empty_saturday);
////                                empty_saturday.setVisibility(View.VISIBLE);
//                                }
//                                 JSONArray _sunday = response.getJSONArray("Sunday");
//                                if(_sunday.length()>0)
//                                {
//                                    for (int i = 0; i < _sunday.length(); i++) {
//                                        JSONObject slot = _sunday.getJSONObject(i);
//                                        sunday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
//                                    }
//                                    RecyclerView gridViewSunday = (RecyclerView) findViewById(R.id.gridviewSunday);
//                                    SlotAdapter sundayAdapter = new SlotAdapter(getApplicationContext(), sunday);
//                                    gridViewSunday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewSunday.setAdapter(sundayAdapter);
//                                } else {
//                                    sunday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
//                                    RecyclerView gridViewSunday = (RecyclerView) findViewById(R.id.gridviewSunday);
//                                    SlotAdapter sundayAdapter = new SlotAdapter(getApplicationContext(), sunday);
//                                    gridViewSunday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                                    gridViewSunday.setAdapter(sundayAdapter);
////                                TextView empty_sunday=(TextView)findViewById(R.id.empty_sunday);
////                                empty_sunday.setVisibility(View.VISIBLE);
//                                }
//                            }
//
//                        } catch (JSONException e1) {
//                            e1.printStackTrace();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        //   Handle Error
//                        Log.d("e",error.toString());
//                        error.printStackTrace();
//                    }
//                })
//                {
//                    @Override
//                    public byte[] getBody() throws AuthFailureError
//                    {
//                        String body="id="+Integer.valueOf(getSharedPreferences(GlobalConstants.user_details_file_name,MODE_PRIVATE).getString("id",null));
//                        return body.getBytes();
//                    }
//                };
//                    postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//                    queue.add(postRequest);

//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                String httpPostBody="username=webmaster&password=webmaster";
//                // usually you'd have a field with some values you'd want to escape, you need to do it yourself if overriding getBody. here's how you do it
//                try {
//                    httpPostBody=httpPostBody+"&randomFieldFilledWithAwkwardCharacters="+ URLEncoder.encode("{{%stuffToBe Escaped/","UTF-8");
//                } catch (UnsupportedEncodingException exception) {
//                    // return null and don't pass any POST string if you encounter encoding error
//                    return null;
//                }
//                return httpPostBody.getBytes();
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/x-www-form-urlencoded");
////                headers.put("User-agent", System.getProperty("http.agent"));
//                return headers;
//            }
//            @Override
//            protected Response<String> parseNetworkResponse(NetworkResponse response) {
//                String responseString = "";
//                if (response != null) {
//                    responseString = String.valueOf(response.statusCode);
//                    // can get more details such as response.headers
//                }
//                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
//            }


//        RequestQueue queue = Volley.newRequestQueue(this);
//        JsonObjectRequest fetch = new JsonObjectRequest(Request.Method.POST, "http://dealsinbongo.com/smartclass/api/web/v1/time-tables/get-user", object, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Log.d("response",response.toString());
//                try
//                {
//                    JSONArray _monday = response.getJSONArray("Monday");
//                    for (int i = 0; i < _monday.length(); i++)
//                    {
//                        JSONObject slot = _monday.getJSONObject(i);
//                        monday.add(new TimeTableSlot(slot.getString("subject"),slot.getString("teacher"),slot.getString("time"),slot.getString("color_background"),slot.getString("color_font")));
//                    }
//                    JSONArray _tuesday = response.getJSONArray("Tuesday");
//                    for (int i = 0; i < _tuesday.length(); i++)
//                    {
//                        JSONObject slot = _tuesday.getJSONObject(i);
//                        tuesday.add(new TimeTableSlot(slot.getString("subject"),slot.getString("teacher"),slot.getString("time"),slot.getString("color_background"),slot.getString("color_font")));
//                    }
//                    JSONArray _wednesday = response.getJSONArray("Wednesday");
//                    for (int i = 0; i < _wednesday.length(); i++)
//                    {
//                        JSONObject slot = _wednesday.getJSONObject(i);
//                        wednesday.add(new TimeTableSlot(slot.getString("subject"),slot.getString("teacher"),slot.getString("time"),slot.getString("color_background"),slot.getString("color_font")));
//                    }
//                    JSONArray _thursday = response.getJSONArray("Thursday");
//                    for (int i = 0; i < _thursday.length(); i++)
//                    {
//                        JSONObject slot = _thursday.getJSONObject(i);
//                        thursday.add(new TimeTableSlot(slot.getString("subject"),slot.getString("teacher"),slot.getString("time"),slot.getString("color_background"),slot.getString("color_font")));
//                    }
//                    JSONArray _friday = response.getJSONArray("Friday");
//                    for (int i = 0; i < _friday.length(); i++)
//                    {
//                        JSONObject slot = _friday.getJSONObject(i);
//                        friday.add(new TimeTableSlot(slot.getString("subject"),slot.getString("teacher"),slot.getString("time"),slot.getString("color_background"),slot.getString("color_font")));
//                    }
//                    JSONArray _saturday = response.getJSONArray("Saturday");
//                    for (int i = 0; i < _saturday.length(); i++)
//                    {
//                        JSONObject slot = _saturday.getJSONObject(i);
//                        saturday.add(new TimeTableSlot(slot.getString("subject"),slot.getString("teacher"),slot.getString("time"),slot.getString("color_background"),slot.getString("color_font")));
//                    }
//                    JSONArray _sunday = response.getJSONArray("Sunday");
//                    for (int i = 0; i < _sunday.length(); i++)
//                    {
//                        JSONObject slot = _sunday.getJSONObject(i);
//                        sunday.add(new TimeTableSlot(slot.getString("subject"),slot.getString("teacher"),slot.getString("time"),slot.getString("color_background"),slot.getString("color_font")));
//                    }
//                    RecyclerView gridViewMonday = (RecyclerView) findViewById(R.id.gridview);
//                    SlotAdapter mondayAdapter = new SlotAdapter(getApplicationContext(), monday);
//                    gridViewMonday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                    gridViewMonday.setAdapter(mondayAdapter);
//                    RecyclerView gridViewTuesday = (RecyclerView) findViewById(R.id.gridviewTuesday);
//                    SlotAdapter tuesdayAdapter = new SlotAdapter(getApplicationContext(), tuesday);
//                    gridViewTuesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                    gridViewTuesday.setAdapter(tuesdayAdapter);
//                    RecyclerView gridViewWednesday = (RecyclerView) findViewById(R.id.gridviewWednesday);
//                    SlotAdapter wednesdayAdapter = new SlotAdapter(getApplicationContext(), wednesday);
//                    gridViewWednesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                    gridViewWednesday.setAdapter(wednesdayAdapter);
//                    RecyclerView gridViewThursday = (RecyclerView) findViewById(R.id.gridviewThursday);
//                    SlotAdapter thursdayAdapter = new SlotAdapter(getApplicationContext(), thursday);
//                    gridViewThursday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                    gridViewThursday.setAdapter(thursdayAdapter);
//                    RecyclerView gridViewFriday = (RecyclerView) findViewById(R.id.gridviewFriday);
//                    SlotAdapter fridayAdapter = new SlotAdapter(getApplicationContext(), friday);
//                    gridViewFriday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                    gridViewFriday.setAdapter(fridayAdapter);
//                    RecyclerView gridViewSaturday = (RecyclerView) findViewById(R.id.gridviewSaturday);
//                    SlotAdapter saturdayAdapter = new SlotAdapter(getApplicationContext(), saturday);
//                    gridViewSaturday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                    gridViewSaturday.setAdapter(saturdayAdapter);
//                    RecyclerView gridViewSunday = (RecyclerView) findViewById(R.id.gridviewSunday);
//                    SlotAdapter sundayAdapter = new SlotAdapter(getApplicationContext(), sunday);
//                    gridViewSunday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
//                    gridViewSunday.setAdapter(sundayAdapter);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("error",error.toString());
//                error.printStackTrace();
//            }
//        }){
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/x-www-form-urlencoded");
//                return headers;
//            }
//        };
////        {
////            @Override
////            public Map<String, Integer> getParams()
////            {
////                SharedPreferences sharedPreferences=getSharedPreferences("Credentials",MODE_PRIVATE);
////                Map<String, Integer> params = new HashMap<>();
////                params.put("id", Integer.valueOf(sharedPreferences.getString("id",null));
////                return params;
////            }
////
////        };
//        queue.add(fetch);
    }

    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendRequest() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        final ImageView timetable_image = (ImageView) findViewById(R.id.timetable_image);
        final RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.TIMETABLE_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONObject response = null;
                        try {
                            response = new JSONObject(response1);
                            /*if the timetable is in image format, the api response will have a string named- "timetable_image" which will contain the image url. This string will not be returned if the timetable is not in image format and we will move to else construct*/
                            if (response.has("timetable_image")) {
                                should_scroll = false;
//                                layout.setVisibility(View.GONE);
                                layout2.setVisibility(View.GONE);
                                relativeLayout.setVisibility(View.VISIBLE);
//                                refreshLayout.setEnabled(false);
                                final String image_url = response.getString("timetable_image");
                                ImageRequest teacherImageRequest = new ImageRequest(response.getString("timetable_image"), new Response.Listener<Bitmap>() {
                                    @Override
                                    public void onResponse(final Bitmap response2) {

                                        if (relativeLayout.getVisibility() != View.VISIBLE) {
                                            relativeLayout.setVisibility(View.VISIBLE);
                                        }
                                        timetable_image.setImageBitmap(response2);
                                        timetable_image.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
//                                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                                                response.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                                                byte[] byteArray = stream.toByteArray();
                                                Intent in1 = new Intent(TimeTableActivity.this, com.handysolver.smartclass.ImageView.class);
                                                in1.putExtra("image",image_url);
//                                                in1.putExtra("image",byteArray);
                                                startActivity(in1);
                                            }
                                        });


//                                        /38\]1`+
                                    }
                                }, 0, 0, null, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        timetable_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                                        PhotoViewAttacher attacher = new PhotoViewAttacher(timetable_image);
                                        attacher.update();
                                        attacher.setScaleType(ImageView.ScaleType.FIT_XY);
                                    }
                                });
                                queue.getCache().clear();
                                queue.add(teacherImageRequest);
                            } else {
                                relativeLayout.setVisibility(View.GONE);
//                                if (layout.getVisibility() != View.VISIBLE) {
//                                    layout.setVisibility(View.VISIBLE);
//                                }
                                if (layout2.getVisibility() != View.VISIBLE) {
                                    layout2.setVisibility(View.VISIBLE);
                                }
                                /*Individual JsonArrays for all 7 days are parsed which contain the corresponding classes. These are added to the corresponding ArrayList for the day*/
                                /*We insert into ArrayList objects of TimeTableSlot class by calling it's constructor for every class in the JsonArray*/
                                /*If the JsonArray for a day has no classes, a single card saying no classes is displayed in the recycler layout for that day*/
                                JSONArray _monday = response.getJSONArray("Monday");
                                if (_monday.length() > 0) {
                                    for (int i = 0; i < _monday.length(); i++) {
                                        JSONObject slot = _monday.getJSONObject(i);
                                        monday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
                                    }
                                    RecyclerView gridViewMonday = (RecyclerView) findViewById(R.id.gridview);
                                    SlotAdapter mondayAdapter = new SlotAdapter(getApplicationContext(), monday);
                                    gridViewMonday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewMonday.setAdapter(mondayAdapter);
                                } else {
                                    monday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
                                    RecyclerView gridViewMonday = (RecyclerView) findViewById(R.id.gridview);
                                    SlotAdapter mondayAdapter = new SlotAdapter(getApplicationContext(), monday);
                                    gridViewMonday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewMonday.setAdapter(mondayAdapter);
//                                TextView empty_monday=(TextView)findViewById(R.id.empty_monday);
//                                empty_monday.setVisibility(View.VISIBLE);
                                }
                                JSONArray _tuesday = response.getJSONArray("Tuesday");
                                if (_tuesday.length() > 0) {
                                    for (int i = 0; i < _tuesday.length(); i++) {
                                        JSONObject slot = _tuesday.getJSONObject(i);
                                        tuesday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
                                    }
                                    RecyclerView gridViewTuesday = (RecyclerView) findViewById(R.id.gridviewTuesday);
                                    SlotAdapter tuesdayAdapter = new SlotAdapter(getApplicationContext(), tuesday);
                                    gridViewTuesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewTuesday.setAdapter(tuesdayAdapter);
                                } else {
                                    tuesday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
                                    RecyclerView gridViewTuesday = (RecyclerView) findViewById(R.id.gridviewTuesday);
                                    SlotAdapter tuesdayAdapter = new SlotAdapter(getApplicationContext(), tuesday);
                                    gridViewTuesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewTuesday.setAdapter(tuesdayAdapter);
//                                TextView empty_tuesday=(TextView)findViewById(R.id.empty_tuesday);
//                                empty_tuesday.setVisibility(View.VISIBLE);
                                }
                                JSONArray _wednesday = response.getJSONArray("Wednesday");
                                if (_wednesday.length() > 0) {
                                    for (int i = 0; i < _wednesday.length(); i++) {
                                        JSONObject slot = _wednesday.getJSONObject(i);
                                        wednesday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
                                    }
                                    RecyclerView gridViewWednesday = (RecyclerView) findViewById(R.id.gridviewWednesday);
                                    SlotAdapter wednesdayAdapter = new SlotAdapter(getApplicationContext(), wednesday);
                                    gridViewWednesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewWednesday.setAdapter(wednesdayAdapter);
                                } else {
                                    wednesday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
                                    RecyclerView gridViewWednesday = (RecyclerView) findViewById(R.id.gridviewWednesday);
                                    SlotAdapter wednesdayAdapter = new SlotAdapter(getApplicationContext(), wednesday);
                                    gridViewWednesday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewWednesday.setAdapter(wednesdayAdapter);

//                                TextView empty_wednesday=(TextView)findViewById(R.id.empty_wednesday);
//                                empty_wednesday.setVisibility(View.VISIBLE);
                                }
                                JSONArray _thursday = response.getJSONArray("Thursday");
                                if (_thursday.length() > 0) {
                                    for (int i = 0; i < _thursday.length(); i++) {
                                        JSONObject slot = _thursday.getJSONObject(i);
                                        thursday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
                                    }
                                    RecyclerView gridViewThursday = (RecyclerView) findViewById(R.id.gridviewThursday);
                                    SlotAdapter thursdayAdapter = new SlotAdapter(getApplicationContext(), thursday);
                                    gridViewThursday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewThursday.setAdapter(thursdayAdapter);
                                } else {
                                    thursday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
                                    RecyclerView gridViewThursday = (RecyclerView) findViewById(R.id.gridviewThursday);
                                    SlotAdapter thursdayAdapter = new SlotAdapter(getApplicationContext(), thursday);
                                    gridViewThursday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewThursday.setAdapter(thursdayAdapter);
//                              TextView empty_thursday=(TextView)findViewById(R.id.empty_thursday);
//                                empty_thursday.setVisibility(View.VISIBLE);
                                }
                                JSONArray _friday = response.getJSONArray("Friday");
                                if (_friday.length() > 0) {
                                    for (int i = 0; i < _friday.length(); i++) {
                                        JSONObject slot = _friday.getJSONObject(i);
                                        friday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
                                    }
                                    RecyclerView gridViewFriday = (RecyclerView) findViewById(R.id.gridviewFriday);
                                    SlotAdapter fridayAdapter = new SlotAdapter(getApplicationContext(), friday);
                                    gridViewFriday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewFriday.setAdapter(fridayAdapter);
                                } else {
                                    friday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
                                    RecyclerView gridViewFriday = (RecyclerView) findViewById(R.id.gridviewFriday);
                                    SlotAdapter fridayAdapter = new SlotAdapter(getApplicationContext(), friday);
                                    gridViewFriday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewFriday.setAdapter(fridayAdapter);
//                                TextView empty_friday=(TextView)findViewById(R.id.empty_friday);
//                                empty_friday.setVisibility(View.VISIBLE);
                                }
                                JSONArray _saturday = response.getJSONArray("Saturday");
                                if (_saturday.length() > 0) {
                                    for (int i = 0; i < _saturday.length(); i++) {
                                        JSONObject slot = _saturday.getJSONObject(i);
                                        saturday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
                                    }
                                    RecyclerView gridViewSaturday = (RecyclerView) findViewById(R.id.gridviewSaturday);
                                    SlotAdapter saturdayAdapter = new SlotAdapter(getApplicationContext(), saturday);
                                    gridViewSaturday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewSaturday.setAdapter(saturdayAdapter);
                                } else {
                                    saturday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
                                    RecyclerView gridViewSaturday = (RecyclerView) findViewById(R.id.gridviewSaturday);
                                    SlotAdapter saturdayAdapter = new SlotAdapter(getApplicationContext(), saturday);
                                    gridViewSaturday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewSaturday.setAdapter(saturdayAdapter);
//                                TextView empty_saturday=(TextView)findViewById(R.id.empty_saturday);
//                                empty_saturday.setVisibility(View.VISIBLE);
                                }
                                JSONArray _sunday = response.getJSONArray("Sunday");
                                if (_sunday.length() > 0) {
                                    for (int i = 0; i < _sunday.length(); i++) {
                                        JSONObject slot = _sunday.getJSONObject(i);
                                        sunday.add(new TimeTableSlot(slot.getString("subject"), slot.getString("teacher"), slot.getString("time"), slot.getString("color_background"), slot.getString("color_font"), slot.getString("teacher_id")));
                                    }
                                    RecyclerView gridViewSunday = (RecyclerView) findViewById(R.id.gridviewSunday);
                                    SlotAdapter sundayAdapter = new SlotAdapter(getApplicationContext(), sunday);
                                    gridViewSunday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewSunday.setAdapter(sundayAdapter);
                                } else {
                                    sunday.add(new TimeTableSlot(getString(R.string.no_data_available), "", "", GlobalConstants.white, GlobalConstants.black, ""));
                                    RecyclerView gridViewSunday = (RecyclerView) findViewById(R.id.gridviewSunday);
                                    SlotAdapter sundayAdapter = new SlotAdapter(getApplicationContext(), sunday);
                                    gridViewSunday.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                                    gridViewSunday.setAdapter(sundayAdapter);
//                                TextView empty_sunday=(TextView)findViewById(R.id.empty_sunday);
//                                empty_sunday.setVisibility(View.VISIBLE);
                                }
                            }
                            refreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*The API might return multiple errors. For each error code, we will perform certain actions accordingly*/
                        if(error.networkResponse!=null)
                        {
                        String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                        try {
                            JSONObject error_json = new JSONObject(error_message);
                            if (error_json.has("status")) {
                                if (error_json.getInt("status") == 444) {
                                    /*When there hasn't been any entry for the timetable in the backend*/
                                    TextView error_view = (TextView) findViewById(R.id.error_view);
                                    error_view.setVisibility(View.VISIBLE);
                                    error_view.setText(error_json.getString("message"));
                                }
                                if (error_json.getInt("status") == 498) {
                                    /*When the user does not correspond to any class or section*/
                                    TextView error_view = (TextView) findViewById(R.id.error_view);
                                    error_view.setVisibility(View.VISIBLE);
                                    error_view.setText(error_json.getString("message"));
                                }
                                if (error_json.getInt("status") == 445) {
                                    /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
                                    TextView error_view = (TextView) findViewById(R.id.error_view);
                                    error_view.setVisibility(View.VISIBLE);
                                    error_view.setText(error_json.getString("message"));
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.clear();
                                    editor.apply();
                                    Intent intent = new Intent(TimeTableActivity.this, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }}
                        error.printStackTrace();
                    }
                }) {
            /*This method sets the body of the post request. In this case, we are sending the user id in params*/
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = sharedPreferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0);
                    } else {
                        body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
                    }
                }
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }
}
