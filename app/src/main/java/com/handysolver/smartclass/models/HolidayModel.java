package com.handysolver.smartclass.models;

/**
 * Created by hsmm1 on 11/23/16.
 */

public class HolidayModel {
    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    private String date;
    private String name;

    public HolidayModel(String name, String date) {
        this.name = name;
        this.date = date;
    }
}
