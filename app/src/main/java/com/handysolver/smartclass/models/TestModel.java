package com.handysolver.smartclass.models;

/**
 * Created by Sanket on 27-10-2016.
 */
public class TestModel {
    private String code,comments,grade;
    private String date;
    private String subject_name;
    private String time;
    private String marks;
    private int grade_scheme;



    private int average_marks,highest_marks;

    public TestModel(String code, String date, String time, String subject_name, String marks, int grade_scheme) {
        this.code = code;
        this.date = date;
        this.time = time;
        this.subject_name = subject_name;
        this.marks = marks;
        this.grade_scheme = grade_scheme;
    }

    public int getGrade_scheme() {
        return grade_scheme;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAverage_marks() {
        return average_marks;
    }

    public void setAverage_marks(int average_marks) {
        this.average_marks = average_marks;
    }

    public int getHighest_marks() {
        return highest_marks;
    }

    public void setHighest_marks(int highest_marks) {
        this.highest_marks = highest_marks;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
