package com.handysolver.smartclass.models;

/**
 * Created by Sanket on 08-11-2016.
 */
public class Exam {
    public String getExam_name() {
        return exam_name;
    }

    public String getExam_location() {
        return exam_location;
    }

    private String exam_name;
    private String start_date;
    private String end_date;
    private String exam_location;
    private int id;
    private int model;

    public int getModel() {
        return model;
    }

    public int getId() {
        return id;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public Exam(String exam_name, int id, int model) {
        this.exam_name = exam_name;
        this.id = id;
        this.exam_location = exam_location;
        this.model = model;
    }
}
