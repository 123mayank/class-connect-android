package com.handysolver.smartclass.models;

/**
 * Created by OWNER on 10-02-2017.
 */

public class ExamSetting {
    private Boolean timeTableUpdate=true;
    private Boolean attendance=true;
    private Boolean noticeBoard=true;
    private Boolean messages=true;
    private Boolean assignmentCreation=true;
    private Boolean assignmentUpcoming=true;
    private Boolean assignmentMarked=true;
    private Boolean classTestCreation=true;
    private Boolean classTestUpcoming=true;
    private Boolean classTestMarked=true;
    private Boolean examinationCreation=true;
    private Boolean examinationUpcoming=true;
    private Boolean examinationMarked=true;
    public ExamSetting(){

    }

    public Boolean getTimeTableUpdate() {
        return timeTableUpdate;
    }

    public void setTimeTableUpdate(Boolean timeTableUpdate) {
        this.timeTableUpdate = timeTableUpdate;
    }

    public Boolean getAttendance() {
        return attendance;
    }

    public void setAttendance(Boolean attendance) {
        this.attendance = attendance;
    }

    public Boolean getNoticeBoard() {
        return noticeBoard;
    }

    public void setNoticeBoard(Boolean noticeBoard) {
        this.noticeBoard = noticeBoard;
    }

    public Boolean getMessages() {
        return messages;
    }

    public void setMessages(Boolean messages) {
        this.messages = messages;
    }

    public Boolean getAssignmentCreation() {
        return assignmentCreation;
    }

    public void setAssignmentCreation(Boolean assignmentCreation) {
        this.assignmentCreation = assignmentCreation;
    }

    public Boolean getAssignmentUpcoming() {
        return assignmentUpcoming;
    }

    public void setAssignmentUpcoming(Boolean assignmentUpcoming) {
        this.assignmentUpcoming = assignmentUpcoming;
    }

    public Boolean getAssignmentMarked() {
        return assignmentMarked;
    }

    public void setAssignmentMarked(Boolean assignmentMarked) {
        this.assignmentMarked = assignmentMarked;
    }

    public Boolean getClassTestCreation() {
        return classTestCreation;
    }

    public void setClassTestCreation(Boolean classTestCreation) {
        this.classTestCreation = classTestCreation;
    }

    public Boolean getClassTestUpcoming() {
        return classTestUpcoming;
    }

    public void setClassTestUpcoming(Boolean classTestUpcoming) {
        this.classTestUpcoming = classTestUpcoming;
    }

    public Boolean getClassTestMarked() {
        return classTestMarked;
    }

    public void setClassTestMarked(Boolean classTestMarked) {
        this.classTestMarked = classTestMarked;
    }

    public Boolean getExaminationCreation() {
        return examinationCreation;
    }

    public void setExaminationCreation(Boolean examinationCreation) {
        this.examinationCreation = examinationCreation;
    }

    public Boolean getExaminationUpcoming() {
        return examinationUpcoming;
    }

    public void setExaminationUpcoming(Boolean examinationUpcoming) {
        this.examinationUpcoming = examinationUpcoming;
    }

    public Boolean getExaminationMarked() {
        return examinationMarked;
    }

    public void setExaminationMarked(Boolean examinationMarked) {
        this.examinationMarked = examinationMarked;
    }
}
