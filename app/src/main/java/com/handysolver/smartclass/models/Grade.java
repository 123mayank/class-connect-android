package com.handysolver.smartclass.models;

/**
 * Created by hsmm1 on 2/14/17.
 */

public class Grade {
    private String name;
    private int grade_id;
    private int section_id;

    public Grade(String name, int grade_id, int section_id) {
        this.name = name;
        this.grade_id = grade_id;
        this.section_id = section_id;
    }

    public String getName() {
        return name;
    }

    public int getGrade_id() {
        return grade_id;
    }

    public int getSection_id() {
        return section_id;
    }
}
