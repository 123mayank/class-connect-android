package com.handysolver.smartclass.models;

/**
 * Created by Sanket on 05-10-2016.
 */
public class TimeTableSlot {
    private String subject;
    private String teacher;
    private String time;
    private String background_color;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    private String font_color;
    public String getFont_color() {
        return font_color;
    }
    public void setFont_color(String font_color) {
        this.font_color = font_color;
    }

    public String getBackground_color() {
        return background_color;
    }

    public void setBackground_color(String background_color) {
        this.background_color = background_color;
    }

    public TimeTableSlot(String subject, String teacher, String time, String background_color, String font_color, String id)
    {
        this.subject = subject;
        this.teacher = teacher;
        this.time = time;
        this.background_color=background_color;
        this.font_color=font_color;
        this.id=id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
