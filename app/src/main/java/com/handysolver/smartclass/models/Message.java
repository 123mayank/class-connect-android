package com.handysolver.smartclass.models;

/**
 * Created by Sanket on 20-10-2016.
 */
public class Message {

    public Message(String topic, String sender, String time, String body, String type)
    {
        this.topic = topic;
        this.sender = sender;
        this.time = time;
        this.body = body;
        this.type=type;
    }

    public String getSender() {
        return sender;
    }


    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    private String topic;
    private String sender;
    private String time;
    private String body;
    private String type;
    public String getType()
    {
        return type;
    }


}
