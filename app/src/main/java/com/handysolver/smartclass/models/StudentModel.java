package com.handysolver.smartclass.models;

import android.graphics.Bitmap;

/**
 * Created by hsmm1 on 11/30/16.
 */

public class StudentModel {
    private String name,grade,parent_name;
    private int id,parent_id;

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bitmap getDisplay_picture() {
        return display_picture;
    }

    public String getName() {
        return name;
    }

    public String getGrade() {
        return grade;
    }

    public void setDisplay_picture(Bitmap display_picture) {
        this.display_picture = display_picture;

    }

    private Bitmap display_picture;

    public StudentModel(String name, String grade) {
        this.name = name;
        this.grade = grade;
    }
}
