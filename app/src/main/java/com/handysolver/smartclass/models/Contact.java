package com.handysolver.smartclass.models;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Sanket on 01-11-2016.
 */
public class Contact {
    private String name, subject, image_url;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contact(String name, String image_url, String subject) {
        this.name = name;
        this.image_url = image_url;
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public String getSubject() {
        return subject;
    }

    public String getImage_url() {
        return image_url;
    }

    public ArrayList<Contact> check(ArrayList<Contact> contacts, Contact contact) {
        ArrayList<Integer>ids = new ArrayList<>();
        for(int i=0;i<contacts.size();i++)
        {
            ids.add(contacts.get(i).getId());
        }
        if(!ids.contains(contact.getId()))
        {
            contacts.add(contact);
            return contacts;
        }
        if(ids.contains(contact.getId()))
        {
            for(int i=0;i<contacts.size();i++)
            {
                if(contact.getId()==contacts.get(i).getId())
                {
                    if(Arrays.asList(contacts.get(i).getSubject().split(",")).contains(contact.getSubject()))
                    {
                        return contacts;
                    }
                    else
                    {
                        contacts.get(i).subject = contacts.get(i).getSubject().concat(","+contact.getSubject());
                        return contacts;
                    }
                }
            }
        }

//        for (int i = 0; i < contacts.size(); i++) {
//            if (contacts.get(i).id == contact.id) {
//                String[] subjects = contacts.get(i).subject.split(",");
//                for (int j=0;j<subjects.length;j++) {
//                    if (!subjects[j].equals(contact.getSubject())) {
//                        if(j==subjects.length-1)
//                        {
//                        contacts.get(i).subject = contacts.get(i).getSubject() + "," + contact.getSubject();
//                        break;
//                        }
//                    }
//                    if(subjects[j].equals(contact.getSubject()))
//                    {
//                        break;
//                    }
//                }
////                break;
//            }
//            else if(i == contacts.size()-1 && ){
//                contacts.add(contact);
//            }
//        }
//        return contacts;
        return contacts;
    }
}
