package com.handysolver.smartclass.models;

/**
 * Created by hsmm1 on 12/8/16.
 */

public class NoticeModel {
    private String date;

    public String[] getLinks() {
        return links;
    }

    private String title;

    public String getNotice() {
        return notice;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    private String notice;

    public String getLink() {
        return link;
    }


    private String link;
    private String[] links;
    private int id;

    public NoticeModel(String notice, String title, String date, int id, String[] link) {
        this.notice = notice;
        this.title = title;
        this.date = date;
        this.id = id;
        this.links=link;
    }
}
