package com.handysolver.smartclass.models;

import java.io.Serializable;

/**
 * Created by OWNER on 13-02-2017.
 */

public class Transport implements Serializable {
    private String name;
    private String numVehicle;
    private String description;
    private String routeFare;
    private Boolean assigned;
    private String link;
    public Transport(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumVehicle() {
        return numVehicle;
    }

    public void setNumVehicle(String numVehicle) {
        this.numVehicle = numVehicle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRouteFare() {
        return routeFare;
    }

    public void setRouteFare(String routeFare) {
        this.routeFare = routeFare;
    }

    public Boolean getAssigned() {
        return assigned;
    }

    public void setAssigned(Boolean assigned) {
        this.assigned = assigned;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
