package com.handysolver.smartclass.models;

/**
 * Created by Sanket on 16-11-2016.
 */
public class AssignmentModel {
    private String marks,date,subject_name,code,description,comments,grade;
    private int average_marks;
    private int highest_marks;
    private int grade_template_id;
    private String[]links;
    private String deadline;

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public AssignmentModel(String marks, String date, String subject_name, String code, String description, String[] links, int grade_template_id) {
        this.marks = marks;
        this.date = date;
        this.subject_name = subject_name;
        this.code = code;
        this.description = description;
        this.links = links;
        this.grade_template_id = grade_template_id;
    }

    public String getMarks() {
        return marks;
    }

    public String getDate() {
        return date;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public String getCode() {
        return code;
    }

    public String getTime() {
        return description;
    }

    public String[] getLinks() {
        return links;
    }

    public int getAverage_marks() {
        return average_marks;
    }

    public void setAverage_marks(int average_marks) {
        this.average_marks = average_marks;
    }

    public int getHighest_marks() {
        return highest_marks;
    }

    public void setHighest_marks(int highest_marks) {
        this.highest_marks = highest_marks;
    }

    public int getGrade_template_id() {
        return grade_template_id;
    }
}
