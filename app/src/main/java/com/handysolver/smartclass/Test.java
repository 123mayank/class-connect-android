package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.handysolver.smartclass.adapters.TestAdapter;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.TestModel;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Test extends AppCompatActivity {
    private ArrayList<TestModel> tests = new ArrayList<>();
    private ArrayList<String> markedTestNames = new ArrayList<>();
    SwipeRefreshLayout refreshLayout;
    Runnable runnable;
    //    static volatile Boolean should_rotate = true;
    private HashMap<Integer, String> marks_names = new HashMap<>();
    //    private HashMap<Integer,String> avg_marks_names=new HashMap<>();
//    private HashMap<Integer,String> highest_marks_names=new HashMap<>();
    BarChart marks_chart;
    SharedPreferences sharedPreferences;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Test.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        marks_chart = (BarChart) findViewById(R.id.test_marks_chart);
        final ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        final BarChart marks_chart = (BarChart) findViewById(R.id.marks_chart);
//        final List<BarEntry> entries = new ArrayList<>();
//        final List<IBarDataSet> dataSets = new ArrayList<>();
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        int index = 0;
//        runnable=new Runnable() {
//            @Override
//            public void run() {
//                refreshLayout.setRefreshing(true);
//            }
//        };
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                marks_chart.zoom(0, 0, 0, 0);
                marks_names.clear();
//                avg_marks_names.clear();
//                highest_marks_names.clear();
                tests.clear();
                dataSets.clear();
                sendRequest();
            }
        });
        sendRequest();
//        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://dealsinbongo.com/smartclass/api/web/v1/tests/test-detail",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response1) {
//                        JSONArray response2 = null;
//                        try {
//                            response2 = new JSONArray(response1);
//                            float k = 0.0f;
//                            for (int i = 0; i < response2.length(); i++) {
//                                JSONObject response = response2.getJSONObject(i);
//                                String start_time = response.getString("time_from");
//                                start_time = start_time.substring(0, 5);
//                                String end_time = response.getString("time_to");
//                                end_time = end_time.substring(0, 5);
//                                String exam_time = start_time + "-" + end_time;
//                                if (response.has("marks")) {
//                                    String marks = response.getString("marks");
//                                    String max_marks = response.getString("max_marks");
//                                    if (!marks.equals("null")) {
//                                        k++;
//                                        entries.add(new BarEntry(k, Float.valueOf(marks) / Float.valueOf(max_marks) * 100));
//                                    }
//                                    tests.add(new TestModel(response.getString("test_code"), response.getString("test_date"), exam_time, response.getString("subject"), marks + "/" + max_marks));
//                                } else {
//                                    tests.add(new TestModel(response.getString("test_code"), response.getString("test_date"), exam_time, response.getString("subject"), "" + "/" + ""));
//                                }
//                            }
//                            BarDataSet barDataSet = new BarDataSet(entries, "asd");
//                            XAxis xAxis = marks_chart.getXAxis();
//                            IAxisValueFormatter formatter = new IAxisValueFormatter() {
//                                @Override
//                                public String getFormattedValue(float value, AxisBase axis) {
//                                    if (value < tests.size()) {
//                                        return tests.get((int) value).getSubject_name();
//                                    }
//                                    return "";
//                                }
//
//
////                                @Override
////                                public int getDecimalDigits() {
////                                    return 0;
////                                }
//                            };
//                            xAxis.setValueFormatter(formatter);
//                            xAxis.setDrawGridLines(false);
//                            marks_chart.setDrawGridBackground(false);
//                            marks_chart.getAxisLeft().setAxisMaxValue(100);
//                            marks_chart.getAxisLeft().setDrawGridLines(false);
//                            marks_chart.getAxisRight().setEnabled(false);
//                            marks_chart.setData(new BarData(barDataSet));
//                            marks_chart.setDrawValueAboveBar(true);
//                            marks_chart.setMaxVisibleValueCount(60);
//                            marks_chart.setDoubleTapToZoomEnabled(false);
//                            Description description = new Description();
//                            description.setText("marks");
//                            marks_chart.setDescription(description);
//                            marks_chart.invalidate();
//
//                            RecyclerView test_list = (RecyclerView) findViewById(R.id.test_recycler_view);
//                            test_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//                            test_list.setAdapter(new TestAdapter(getApplicationContext(), tests));
//                        } catch (JSONException e1) {
//                            e1.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        //   Handle Error
//                        error.printStackTrace();
//                    }
//                }) {
//
//
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                String body = "id=" + Integer.valueOf(getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE).getString("id", null));
//                return body.getBytes();
//            }
//        };
//        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        Volley.newRequestQueue(this).add(postRequest);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendRequest() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
//        final Thread thread=new Thread(new Runnable() {
//            @Override
//            public void run() {
//                refreshLayout.setRefreshing(true);
//            }
//        });
////        refreshLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                if(should_rotate)
//                    refreshLayout.setRefreshing(true);
//                else
//                    refreshLayout.setRefreshing(false);
//            }
//        });
//        final BarChart marks_chart = (BarChart) findViewById(R.id.test_marks_chart);
//        final List<BarEntry> entries = new ArrayList<>();
        /* For each colored bar, there will be a IBarDataSet array list
         * Since we are showing only a single colored bar in this graph, hence a single ArrayList */
        final List<IBarDataSet> dataSets = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.TEST_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONArray response2 = null;
                        try {
                            response2 = new JSONArray(response1);
                            float k = 0.0f;
                            for (int i = 0; i < response2.length(); i++) {
                                JSONObject response = response2.getJSONObject(i);
                                String start_time = response.getString("time_from");
                                start_time = start_time.substring(0, 5);
                                String end_time = response.getString("time_to");
                                end_time = end_time.substring(0, 5);
                                String exam_time = start_time + "-" + end_time;
                                if (response.has("marks")) {
                                    String subject_name = response.getString("subject");
                                    markedTestNames.add(response.getString("subject"));
//                                    highest_marks_names.put(response.getInt("highest_mark"),subject_name);
//                                    avg_marks_names.put(response.getInt("highest_mark"),subject_name);
                                    marks_names.put(response.getInt("marks"), subject_name);
                                    /* an array list of BarEntry objects */
                                    final List<BarEntry> entries = new ArrayList<>();
//                                    final List<BarEntry> highest_marks_entries = new ArrayList<>();
//                                    final List<BarEntry> average_marks_entries = new ArrayList<>();
                                    int marks = response.getInt("marks");
                                    int max_marks = response.getInt("max_marks");
//                            String max_marks = response.getString("max_marks");
                                    /* adding a BarEntry object to the entries ArrayList */
                                    entries.add(new BarEntry(k, Float.valueOf(marks)));
//                                    highest_marks_entries.add(new BarEntry(k + 3.0f, response.getInt("highest_mark")));
//                                    average_marks_entries.add(new BarEntry(k + 4.5f, response.getInt("avg")));
                                    /* Creating a BarDataSet object from the BarEntry ArrayList */
                                    BarDataSet marks_set = new BarDataSet(entries, "Your score");
//                                    BarDataSet highest_marks_set = new BarDataSet(highest_marks_entries, "Class Highest");
//                                    BarDataSet average_marks_set = new BarDataSet(average_marks_entries, "Class Average");

                                    marks_set.setColor(Color.parseColor(GlobalConstants.presentColor));
//                                    highest_marks_set.setColor(Color.parseColor(GlobalConstants.presentColor));
//                                    average_marks_set.setColor(Color.parseColor(GlobalConstants.sickColor));
                                    dataSets.add(marks_set);
//                                    dataSets.add(highest_marks_set);
//                                    dataSets.add(average_marks_set);
                                    k += 0.35f;
                                    TestModel model = new TestModel(response.getString("test_code"), response.getString("test_date"), exam_time, response.getString("subject"), marks + "/" + max_marks,response.getInt("grade_template_id"));
                                    model.setHighest_marks(response.getInt("highest_mark"));
                                    model.setAverage_marks(response.getInt("avg"));
                                    model.setComments(response.getString("comments"));
                                    model.setGrade(response.getString("grade"));
                                    tests.add(model);
                                } else {
                                    TestModel model = new TestModel(response.getString("test_code"), response.getString("test_date"), exam_time, response.getString("subject"), "" + "/" + "",response.getInt("grade_template_id"));
                                    if(response.has("comments")) {
                                        model.setComments(response.getString("comments"));
                                    }
                                    else
                                    {
                                        model.setComments("");
                                    }
                                    if(response.has("grade"))   {
                                        model.setGrade(response.getString("grade"));
                                    }
                                    else
                                    {
                                        model.setGrade("");
                                    }
                                    tests.add(model);
                                }
                            }
//                            BarDataSet barDataSet = new BarDataSet(entries, "asd");
                            XAxis xAxis = marks_chart.getXAxis();
                            IAxisValueFormatter formatter = new IAxisValueFormatter() {
                                @Override
                                public String getFormattedValue(float value, AxisBase axis) {
                                    return "";
                                }

//                        @Override
//                        public int getDecimalDigits() {
//                            return 0;
//                        }
                            };
                            xAxis.setValueFormatter(formatter);
                            xAxis.setDrawGridLines(false);
                            xAxis.setCenterAxisLabels(true);
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            marks_chart.setDrawGridBackground(false);
                            marks_chart.getAxisLeft().setDrawGridLines(false);
                            marks_chart.getAxisRight().setEnabled(false);
                            BarData barData = new BarData(dataSets);
                            barData.setValueFormatter(new IValueFormatter() {
                                @Override
                                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                                    String res = marks_names.get(Math.round(value)) + "\n" + value;

//                                    if(dataSetIndex%3==1)
//                                    {
//                                        res= highest_marks_names.get(Math.round(value))+"\n"+value;
//                                    }
//                                    if(dataSetIndex%3==2)
//                                    {
//                                        res= avg_marks_names.get(Math.round(value))+"\n"+value;
//                                    }
                                    return res;
                                }
                            });
                            barData.setBarWidth(0.2f);
                            marks_chart.setData(barData);
                            if (dataSets.size() > 3)
                                marks_chart.zoom(2, 0, 0, 0);
                            else
                                marks_chart.zoom(0, 0, 0, 0);
                            marks_chart.setDrawValueAboveBar(true);
                            Description description = new Description();
                            description.setPosition(marks_chart.getXChartMax(),marks_chart.getYMax());
                            description.setText("marks in percent");
                            marks_chart.setDescription(description);
                            marks_chart.setDescription(description);
                            marks_chart.setDrawValueAboveBar(true);
                            LegendEntry entry = new LegendEntry();
                            entry.formColor = Color.parseColor(GlobalConstants.presentColor);
                            entry.label = "Your Score - marks in percent";
                            marks_chart.invalidate();
//                            LegendEntry entry_highest = new LegendEntry();
//                            entry_highest.formColor = Color.parseColor(GlobalConstants.presentColor);
//                            entry_highest.label = "Class Highest";
//                            LegendEntry entry_average = new LegendEntry();
//                            entry_average.formColor = Color.parseColor(GlobalConstants.sickColor);
//                            entry_average.label = "Class Average";
                            ArrayList<LegendEntry> entries = new ArrayList<>();
                            entries.add(entry);
//                            entries.add(entry_highest);
//                            entries.add(entry_average);
                            if (markedTestNames.size() > 0) {
                                Legend legend = marks_chart.getLegend();
                                legend.setCustom(entries);
                            }
                            RecyclerView test_list = (RecyclerView) findViewById(R.id.test_recycler_view);
                            test_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            test_list.setAdapter(new TestAdapter(getApplicationContext(), tests));
                            refreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse.data != null) {
                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                            try {
                                JSONObject error_json = new JSONObject(error_message);
                                if (error_json.has("status")) {
                                    if (error_json.getInt("status") == 444) {
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                    }
                                    if (error_json.getInt("status") == 498) {
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                    }
                                    if (error_json.getInt("status") == 445) {
                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.apply();
                                        Intent intent = new Intent(Test.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        error.printStackTrace();
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = sharedPreferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0);
                    } else {
                        body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
                    }
                }
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
    }

}

