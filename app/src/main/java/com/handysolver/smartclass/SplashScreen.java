package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.handysolver.smartclass.constants.GlobalConstants;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        SharedPreferences sharedPreferences=getSharedPreferences(GlobalConstants.user_details_file_name,MODE_PRIVATE);
        if(sharedPreferences!=null)
        {
            /*Here we check if we have an existing user_id stored in the app's storage*/
            if (sharedPreferences.getString("id", null) != null)
            {
                /*If id exists but the user didn't select remember me at time of login*/
                if(!sharedPreferences.getBoolean("should_remember",false))
                {
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.clear();
                    editor.apply();
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                }
                /*If id exists and user selected remember me*/
                if(sharedPreferences.getBoolean("should_remember",false))
                {
                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(i);
                }
            }
            /*If no existing id is found, that means it's a new user and hence redirected to the login screen*/
            else
            {
                Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(i);
            }
        }
        else {
            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(i);
        }
    }
}
