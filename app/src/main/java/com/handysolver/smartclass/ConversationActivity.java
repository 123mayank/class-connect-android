package com.handysolver.smartclass;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.handysolver.smartclass.adapters.InboxAdapter;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Message;
/*This activity is opened for personal chat window for each contact when the contact is selected in ChatActivity*/
    public class ConversationActivity extends AppCompatActivity {
    private DatabaseHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Christopher Alexander");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        helper=new DatabaseHelper(this,null,null,1);
        Message m=new Message("hello","Christopher Alexander","9:08 pm", "This is a test message", GlobalConstants.personal_message_label_string);
        Message m3=new Message("","Christopher Alexander","9:48 pm", "This is a test message", GlobalConstants.personal_message_label_string);
        Message m2=new Message("hi","->Christopher Alexander","9:03 pm", "This is a test message",GlobalConstants.personal_message_label_string);
        Message m4=new Message("hi","->Christopher Alexander","9:09 pm", "This is a test message",GlobalConstants.personal_message_label_string);
        check(m2);
        check(m4);
        check(m3);
        check(m);
        RecyclerView conversation_recycler_view=(RecyclerView)findViewById(R.id.conversation_content);
        conversation_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        conversation_recycler_view.setAdapter(new InboxAdapter(helper.getTable(),this,"Christopher Alexander"));
        final EditText new_message=(EditText)findViewById(R.id.new_message);
        new_message.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= new_message.getRight()- new_message.getTotalPaddingRight())
                    {
                        // your action for drawable click event
                        Log.d("a","a");
                    }
                }
                return false;
            }
        });
    }
    public void check(Message m)
    {
        if(!helper.CheckIsDataAlreadyInDBorNot(m.getTime(),m.getSender()))
        {
//            helper.addMessage(m.getTopic(),m.getSender(),m.getTime(),m.getBody(),m.getType());
        }
    }
}
