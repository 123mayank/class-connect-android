package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.adapters.ContactAdapter;
import com.handysolver.smartclass.adapters.GradeAdapter;
import com.handysolver.smartclass.classes.AppLifecycleHandler;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.Contact;
import com.handysolver.smartclass.models.Grade;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity {
    private ArrayList<Contact> contacts = new ArrayList<>();
    private ArrayList<Grade> grades = new ArrayList<>();
    SwipeRefreshLayout refreshLayout;
    SharedPreferences sharedPreferences;
    private Boolean press_exit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        int index = 0;
//        runnable=new Runnable() {
//            @Override
//            public void run() {
//                refreshLayout.set`Refreshing(true);
//            }
//        };
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                contacts.clear();
                grades.clear();
                sendRequest();
            }
        });
        press_exit = false;
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        refreshLayout.setRefreshing(true);
        sendRequest();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!press_exit) {
            if (AppLifecycleHandler.isApplicationInForeground()) {
                AutoLogout.timer.cancel();
            }
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        press_exit = true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendRequest() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
//        final Thread thread=new Thread(new Runnable() {
//            @Override
//            public void run() {
//                refreshLayout.setRefreshing(true);
//            }
//        });
////        refreshLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                if(should_rotate)
//                    refreshLayout.setRefreshing(true);
//                else
//                    refreshLayout.setRefreshing(false);
//            }
//        });
//        final BarChart marks_chart = (BarChart) findViewById(R.id.test_marks_chart);
//        final List<BarEntry> entries = new ArrayList<>();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role)) {
            getSupportActionBar().setTitle("Select Class");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.TEACHER_CLASS_CONTROLLER,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response1) {
                            JSONArray response2 = null;
                            try {
                                response2 = new JSONArray(response1);
                                for (int i = 0; i < response2.length(); i++) {
                                    JSONObject response = response2.getJSONObject(i);
                                    Grade grade = new Grade(response.getString("class_name") + " " + response.getString("section_name"), response.getInt("class_id"), response.getInt("section_id"));
                                    grades.add(grade);
                                }
                                RecyclerView contact_list = (RecyclerView) findViewById(R.id.contact_list);
                                contact_list.setAdapter(new GradeAdapter(getApplicationContext(), grades));
                                contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                refreshLayout.setRefreshing(false);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse != null) {
//                                String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
//                                try {
//                                    JSONObject error_json = new JSONObject(error_message);
//                                    if (error_json.has("status")) {
//                                        if (error_json.getInt("status") == 444) {
//                                            TextView error_view = (TextView) findViewById(R.id.error_view);
//                                            error_view.setVisibility(View.VISIBLE);
//                                            error_view.setText(error_json.getString("message"));
//                                        }
//                                        if (error_json.getInt("status") == 498) {
//                                            TextView error_view = (TextView) findViewById(R.id.error_view);
//                                            error_view.setVisibility(View.VISIBLE);
//                                            error_view.setText(error_json.getString("message"));
//                                        }
//                                        if (error_json.getInt("status") == 445) {
//                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
//                                            TextView error_view = (TextView) findViewById(R.id.error_view);
//                                            error_view.setVisibility(View.VISIBLE);
//                                            error_view.setText(error_json.getString("message"));
//                                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                                            editor.clear();
//                                            editor.apply();
//                                            Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
//                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                            startActivity(intent);
//                                            finish();
//                                        }
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
                            }
                            error.printStackTrace();
                        }
                    }) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    String body = "";
                    body = "teacher_id=" + Integer.valueOf(sharedPreferences.getString("id", null));
                    return body.getBytes();
                }
            };
            postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(postRequest);
        }
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.student_role)) {
            getSupportActionBar().setTitle("Select Teacher");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.STUDENT_TEACHER_CONTROLLER,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response1) {
                            JSONArray response2 = null;
                            try {
                                response2 = new JSONArray(response1);
                                for (int i = 0; i < response2.length(); i++) {
                                    JSONObject response = response2.getJSONObject(i);
                                    Contact contact = new Contact(response.getString("firstname") + " " + response.getString("lastname"), response.getString("avatar"), response.getString("subject"));
                                    contact.setId(response.getInt("id"));
                                    contacts.add(contact);
                                }
                                RecyclerView contact_list = (RecyclerView) findViewById(R.id.contact_list);
                                contact_list.setAdapter(new ContactAdapter(getApplicationContext(), contacts));
                                contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                refreshLayout.setRefreshing(false);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse != null) {
                                String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                                try {
                                    JSONObject error_json = new JSONObject(error_message);
                                    if (error_json.has("status")) {
                                        if (error_json.getInt("status") == 444) {
                                            TextView error_view = (TextView) findViewById(R.id.error_view);
                                            error_view.setVisibility(View.VISIBLE);
                                            error_view.setText(error_json.getString("message"));
                                        }
                                        if (error_json.getInt("status") == 498) {
                                            TextView error_view = (TextView) findViewById(R.id.error_view);
                                            error_view.setVisibility(View.VISIBLE);
                                            error_view.setText(error_json.getString("message"));
                                        }
                                        if (error_json.getInt("status") == 445) {
                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
                                            TextView error_view = (TextView) findViewById(R.id.error_view);
                                            error_view.setVisibility(View.VISIBLE);
                                            error_view.setText(error_json.getString("message"));
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.clear();
                                            editor.apply();
                                            Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            error.printStackTrace();
                        }
                    }) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    String role = sharedPreferences.getString("role", null);
                    String body = "";
                    if (role != null) {
                        if (role.equals(GlobalConstants.parent_role)) {
                            body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0);
                        } else {
                            body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
                        }
                    }
                    return body.getBytes();
                }
            };
            postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(postRequest);
        }
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.parent_role)) {
            getSupportActionBar().setTitle("Select Teacher");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            StringRequest postRequest = new StringRequest(Request.Method.POST,GlobalConstants.BASE_BACKEND_URL+GlobalConstants.PARENT_TEACHER_CONTROLLER,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response1) {
                            JSONArray response2 = null;
                            try {
                                response2 = new JSONArray(response1);
                                for (int i = 0; i < response2.length(); i++) {
                                    JSONObject response = response2.getJSONObject(i);
                                    Contact contact = new Contact(response.getString("firstname") + " " + response.getString("lastname"), response.getString("avatar"), response.getString("subject"));
                                    contact.setId(response.getInt("id"));
                                    contacts.add(contact);
                                }
                                RecyclerView contact_list = (RecyclerView) findViewById(R.id.contact_list);
                                contact_list.setAdapter(new ContactAdapter(getApplicationContext(), contacts));
                                contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                refreshLayout.setRefreshing(false);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse != null) {
                                String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                                try {
                                    JSONObject error_json = new JSONObject(error_message);
                                    if (error_json.has("status")) {
                                        if (error_json.getInt("status") == 444) {
                                            TextView error_view = (TextView) findViewById(R.id.error_view);
                                            error_view.setVisibility(View.VISIBLE);
                                            error_view.setText(error_json.getString("message"));
                                        }
                                        if (error_json.getInt("status") == 498) {
                                            TextView error_view = (TextView) findViewById(R.id.error_view);
                                            error_view.setVisibility(View.VISIBLE);
                                            error_view.setText(error_json.getString("message"));
                                        }
                                        if (error_json.getInt("status") == 445) {
                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
                                            TextView error_view = (TextView) findViewById(R.id.error_view);
                                            error_view.setVisibility(View.VISIBLE);
                                            error_view.setText(error_json.getString("message"));
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.clear();
                                            editor.apply();
                                            Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            error.printStackTrace();
                        }
                    }) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    String role = sharedPreferences.getString("role", null);
                    String body = "parent_id=" + Integer.valueOf(sharedPreferences.getString("id", null));
//                    if (role != null) {
//                        if (role.equals(GlobalConstants.parent_role)) {
//                            body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0);
//                        } else {
//                            body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
//                        }
//                    }
                    return body.getBytes();
                }
            };
            postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(postRequest);
//            getSupportActionBar().setTitle("Select Teacher");
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            if (sharedPreferences.getString("all_student_ids", null) != null) {
//                final String[] student_ids = sharedPreferences.getString("all_student_ids", null).split(" ");
//                for (int i = 0; i < student_ids.length; i++) {
//                    if (i == student_ids.length - 1) {
//                        nestedRequest(Integer.valueOf(student_ids[i]), true);
//                    } else {
//                        nestedRequest(Integer.valueOf(student_ids[i]), true);
//                    }
//                    StringRequest postRequest = new StringRequest(Request.Method.POST, "http://dealsinbongo.com/smartclass/api/web/v1/messages/student-to-teacher",
//                            new Response.Listener<String>() {
//                                @Override
//                                public void onResponse(String response1) {
//                                    JSONArray response2 = null;
//                                    try {
//                                        response2 = new JSONArray(response1);
//                                        for (int j = 0; j < response2.length(); j++) {
//                                            JSONObject response = response2.getJSONObject(j);
//                                            Contact contact = new Contact(response.getString("firstname") + " " + response.getString("lastname"), response.getString("avatar"), response.getString("subject"));
//                                            contact.setId(response.getInt("id"));
//                                            if(contacts.size()==0)
//                                            {
//                                                contacts.add(contact);
//                                            }
//                                            contacts = contact.check(contacts,contact);
//                                        }
//                                        if (index == student_ids.length - 1) {
//                                            RecyclerView contact_list = (RecyclerView) findViewById(R.id.contact_list);
//                                            contact_list.setAdapter(new ContactAdapter(getApplicationContext(), contacts));
//                                            contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//                                            refreshLayout.setRefreshing(false);
//                                        }
//                                    } catch (JSONException e1) {
//                                        e1.printStackTrace();
//                                    }
//                                }
//                            },
//                            new Response.ErrorListener() {
//                                @Override
//                                public void onErrorResponse(VolleyError error) {
//                                    if (error.networkResponse != null) {
//                                        String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
//                                        try {
//                                            JSONObject error_json = new JSONObject(error_message);
//                                            if (error_json.has("status")) {
//                                                if (error_json.getInt("status") == 444) {
//                                                    TextView error_view = (TextView) findViewById(R.id.error_view);
//                                                    error_view.setVisibility(View.VISIBLE);
//                                                    error_view.setText(error_json.getString("message"));
//                                                }
//                                                if (error_json.getInt("status") == 498) {
//                                                    TextView error_view = (TextView) findViewById(R.id.error_view);
//                                                    error_view.setVisibility(View.VISIBLE);
//                                                    error_view.setText(error_json.getString("message"));
//                                                }
//                                                if (error_json.getInt("status") == 445) {
//                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
//                                                    TextView error_view = (TextView) findViewById(R.id.error_view);
//                                                    error_view.setVisibility(View.VISIBLE);
//                                                    error_view.setText(error_json.getString("message"));
//                                                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                                                    editor.clear();
//                                                    editor.apply();
//                                                    Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
//                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                                    startActivity(intent);
//                                                    finish();
//                                                }
//                                            }
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                    error.printStackTrace();
//                                }
//                            }) {
//                        @Override
//                        public byte[] getBody() throws AuthFailureError {
//                            String role = sharedPreferences.getString("role", null);
//                            String body = "";
//                            if (role != null) {
//                                if (role.equals(GlobalConstants.parent_role)) {
//                                    body = "id=" + Integer.valueOf(student_ids[index]);
//                                } else {
//                                    body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
//                                }
//                            }
//                            return body.getBytes();
//                        }
//                    };
//                    postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
//                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//                    Volley.newRequestQueue(this).add(postRequest);
//                }

            }

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
        }


//    public void nestedRequest(final int id, final boolean last) {
//        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://dealsinbongo.com/smartclass/api/web/v1/messages/student-to-teacher",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response1) {
//                        JSONArray response2 = null;
//                        try {
//                            response2 = new JSONArray(response1);
//                            for (int j = 0; j < response2.length(); j++) {
//                                JSONObject response = response2.getJSONObject(j);
//                                Contact contact = new Contact(response.getString("firstname") + " " + response.getString("lastname"), response.getString("avatar"), response.getString("subject"));
//                                contact.setId(response.getInt("id"));
//                                if (contacts.size() == 0) {
//                                    contacts.add(contact);
//                                }
//                                contacts = contact.check(contacts, contact);
//                            }
//                            if (last) {
//                                RecyclerView contact_list = (RecyclerView) findViewById(R.id.contact_list);
//                                contact_list.setAdapter(new ContactAdapter(getApplicationContext(), contacts));
//                                contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//                                refreshLayout.setRefreshing(false);
//                            }
//                        } catch (JSONException e1) {
//                            e1.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        if (error.networkResponse != null) {
//                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
//                            try {
//                                JSONObject error_json = new JSONObject(error_message);
//                                if (error_json.has("status")) {
//                                    if (error_json.getInt("status") == 444) {
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                    }
//                                    if (error_json.getInt("status") == 498) {
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                    }
//                                    if (error_json.getInt("status") == 445) {
//                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
//                                        TextView error_view = (TextView) findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                                        editor.clear();
//                                        editor.apply();
//                                        Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        error.printStackTrace();
//                    }
//                }) {
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                String role = sharedPreferences.getString("role", null);
//                String body = "";
//                if (role != null) {
//                    if (role.equals(GlobalConstants.parent_role)) {
//                        body = "id=" + id;
//                    } else {
//                        body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
//                    }
//                }
//                return body.getBytes();
//            }
//        };
//        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        Volley.newRequestQueue(this).add(postRequest);
//    }
}
