package com.handysolver.smartclass;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.fragments.AttendanceDetailsFragment;
import com.handysolver.smartclass.fragments.AttendanceStatisticsFragment;
import com.handysolver.smartclass.services.AutoLogout;
import com.p_v.flexiblecalendar.entity.Event;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/*This activity hosts the 2 attendance tabs, titled overview and details. This activity only handles the tab layout and titles of the tabs, along with storing and synchronizing the date range between the 2 tabs. When the date range is changed from the default range, the details tab is updated from this activity. Hence, you might find the same methods- sendRequest() and setListener() in the Activity Details Fragment and this activity. the individual constituents of the tabs are defined in AttendanceStatisticFragment and AttendanceDetailsFragment respectively*/
public class AttendanceTabs extends AppCompatActivity implements AttendanceStatisticsFragment.OnFragmentInteractionListener {
    ImageView left, right;
    CustomCalendarView calendarView;
    TextView monthTextView,error_view;
    /*format is the format in which the date is displayed in the app and api-format is the format for sending date as request parameters to the back-end API URL*/
    public static java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("dd-MM-yyyy");
    public static java.text.SimpleDateFormat api_format = new java.text.SimpleDateFormat("yyyy-MM-dd");
    /*Initially, these strings will store the default date values, which will be overwritten as the user selects his/her own start date using date pickers in the overview tab*/
    public static String default_last_date = format.format(java.util.Calendar.getInstance().getTime());
    static String[] start_date_split = AttendanceTabs.format.format(java.util.Calendar.getInstance().getTime()).split("-");
    public static String default_start_date = "01-01-" + start_date_split[2];
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    SharedPreferences preferences;
    SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_tabs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Attendance");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setupTabs();
            }
        });
        preferences = getSharedPreferences(GlobalConstants.user_details_file_name, MODE_PRIVATE);
        setupTabs();


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (preferences.getString("role", "").equals(GlobalConstants.teacher_role) || preferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(preferences.getString("role","").equals(GlobalConstants.teacher_role)||preferences.getString("role","").equals(GlobalConstants.admin_role))
        {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }}
    }

    public void test() {
        if (getSupportFragmentManager().getFragments() != null) {
            Fragment calendar_fragment = getSupportFragmentManager().getFragments().get(1);
//            left = (ImageView) calendar_fragment.getView().findViewById(R.id.left_arrow);
//            right = (ImageView) calendar_fragment.getView().findViewById(R.id.right_arrow);
//            monthTextView = (TextView) calendar_fragment.getView().findViewById(R.id.month_text_view);
            calendarView = (CustomCalendarView) calendar_fragment.getView().findViewById(R.id.calendar);
            error_view=(TextView)calendar_fragment.getView().findViewById(R.id.error_view);
            sendRequest(this);
        }

    }

    public void setupTabs() {
        // Create the adapter that will return a fragment for each of the two
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        refreshLayout.setRefreshing(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_attendance_tabs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_attendance_tabs, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new AttendanceStatisticsFragment();
                case 1:
                    return new AttendanceDetailsFragment();

            }
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "OVERVIEW";
                case 1:
                    return "DETAILS";
            }
            return null;
        }
    }

    /*This function is called when sending API request from individual tabs*/
    public static String return_api_format_date(String date) {
        String res = "";
        try {
            res = AttendanceTabs.api_format.format(AttendanceTabs.format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }

    public void sendRequest(final Context context) {
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.ATTENDANCE_DETAIL_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONObject response = null;
                        try {
                            response = new JSONObject(response1);
                            final ArrayList<String> presentList = new ArrayList<String>();
                            final ArrayList<String> absentList = new ArrayList<String>();
                            final ArrayList<String> sickList = new ArrayList<String>();
                            JSONArray present = response.getJSONArray("present");
                            JSONArray absent = response.getJSONArray("absent");
                            JSONArray sick = response.getJSONArray("sick");
                            for (int i = 0; i < present.length(); i++) {
                                try {
                                    presentList.add(present.getString(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            for (int i = 0; i < absent.length(); i++) {
                                try {
                                    absentList.add(absent.getString(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            for (int i = 0; i < sick.length(); i++) {
                                try {
                                    sickList.add(sick.getString(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            setListener(presentList, absentList, sickList);


                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        if(error.networkResponse.data!=null)
                        {
                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                            try {
                                JSONObject error_json = new JSONObject(error_message);
                                if (error_json.has("status")) {
                                    if (error_json.getInt("status") == 444) {
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        refreshLayout.setRefreshing(false);
                                    }
                                    if (error_json.getInt("status") == 498) {
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        refreshLayout.setRefreshing(false);
                                    }
//                                    if (error_json.getInt("status") == 445) {
//                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
//
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                        SharedPreferences sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                                        editor.clear();
//                                        editor.apply();
//                                        Intent intent = new Intent(Assignment.this, LoginActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intent);
//                                        finish();
//                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }}
                        else
                        {
                            refreshLayout.setRefreshing(false);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText("Error");
                        }

                        error.printStackTrace();
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = preferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + preferences.getInt(GlobalConstants.parent_selected_student, 0) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
                    } else {
                        body = "id=" + Integer.valueOf(preferences.getString("id", null)) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
                    }
                }
                return body.getBytes();

            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(postRequest);
    }

    public void setListener(final ArrayList<String> presentList, final ArrayList<String> absentList, final ArrayList<String> sickList) {
        final int size = min(presentList.size(), absentList.size(), sickList.size());
        if (calendarView != null) {
            ArrayList decorators = new ArrayList();
            DayDecorator decorator = new DayDecorator() {
                @Override
                public void decorate(DayView cell) {
                    for (int i = 0; i < size; i++) {
                        if (presentList.contains(AttendanceTabs.format.format(cell.getDate()))) {
                            cell.setBackgroundResource(R.drawable.selected_date_shape);
                            ((GradientDrawable)cell.getBackground()).setColor(getResources().getColor(R.color.present));
                        }
                        if (absentList.contains(AttendanceTabs.format.format(cell.getDate()))) {
                            cell.setBackgroundResource(R.drawable.selected_date_shape);
                            ((GradientDrawable)cell.getBackground()).setColor(getResources().getColor(R.color.absent));
                        }
                        if (sickList.contains(AttendanceTabs.format.format(cell.getDate()))) {
                            cell.setBackgroundResource(R.drawable.selected_date_shape);
                            ((GradientDrawable)cell.getBackground()).setColor(getResources().getColor(R.color.sick));
                        }
                    }
                }
            };
            decorators.add(decorator);
            calendarView.setDecorators(decorators);
            Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

//Show Monday as first date of week
            calendarView.setFirstDayOfWeek(Calendar.MONDAY);

//Show/hide overflow days of a month
            calendarView.setShowOverflowDate(false);

//call refreshCalendar to update calendar the view
            calendarView.refreshCalendar(currentCalendar);
//            calendarView.setCalendarView(new FlexibleCalendarView.CalendarView() {
//                @Override
//                public BaseCellView getCellView(int position, View convertView, ViewGroup parent, int cellType) {
//                    BaseCellView cellView = (BaseCellView) convertView;
//                    if (cellView == null) {
//                        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
//                        cellView = (BaseCellView) inflater.inflate(R.layout.square_cell_layout, null);
//                    }
//                    cellView.setTextColor(getResources().getColor(android.R.color.black));
//                    if (cellType == BaseCellView.TODAY) {
//                        cellView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
//                    }
//                    if (cellType == BaseCellView.SELECTED) {
//                        cellView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
//                    }
//                    return cellView;
//                }
//
//                @Override
//                public BaseCellView getWeekdayCellView(int position, View convertView, ViewGroup parent) {
//                    return null;
//                }
//
//                @Override
//                public String getDayOfWeekDisplayValue(int dayOfWeek, String defaultValue) {
//                    return null;
//                }
//            });
//            Calendar cal = Calendar.getInstance();
//
//            cal.set(Integer.valueOf(start_date_split[2]), Integer.valueOf(start_date_split[1]) - 1, 1);
//            monthTextView.setText(cal.getDisplayName(Calendar.MONTH,
//                    Calendar.LONG, Locale.ENGLISH) + " " + Integer.valueOf(start_date_split[2]));
//            calendarView.selectDate(cal);
//            left.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    calendarView.moveToPreviousMonth();
//                }
//            });
//            right.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    calendarView.moveToNextMonth();
//                }
//            });
//            calendarView.setOnMonthChangeListener(
//                    new FlexibleCalendarView.OnMonthChangeListener() {
//                        @Override
//                        public void onMonthChange(int year, int month, int direction) {
//                            Calendar cal = Calendar.getInstance();
//                            cal.set(year, month, 1);
//                            monthTextView.setText(cal.getDisplayName(Calendar.MONTH,
//                                    Calendar.LONG, Locale.ENGLISH) + " " + year);
//                        }
//                    });
//            calendarView.setEventDataProvider(new FlexibleCalendarView.EventDataProvider() {
//                @Override
//                public List<? extends Event> getEventsForTheDay(int year, int month, int day) {
//                    for (int i = 0; i < size; i++) {
//                        if (i < presentList.size()) {
//                            String[] dmy = presentList.get(i).split("-");
//                            if (year == Integer.valueOf(dmy[2]) && month == Integer.valueOf(dmy[1]) - 1 && day == Integer.valueOf(dmy[0])) {
//                                List<CustomEvent> colorLst1 = new ArrayList<>();
//                                colorLst1.add(new CustomEvent(R.color.present));
//                                return colorLst1;
//                            }
//                        }
//                        if (i < absentList.size()) {
//                            String[] dmy = absentList.get(i).split("-");
//                            if (year == Integer.valueOf(dmy[2]) && month == Integer.valueOf(dmy[1]) - 1 && day == Integer.valueOf(dmy[0])) {
//                                List<CustomEvent> colorLst1 = new ArrayList<>();
//                                colorLst1.add(new CustomEvent(R.color.absent));
//                                return colorLst1;
//                            }
//                        }
//                        if (i < sickList.size()) {
//                            String[] dmy = sickList.get(i).split("-");
//                            if (year == Integer.valueOf(dmy[2]) && month == Integer.valueOf(dmy[1]) - 1 && day == Integer.valueOf(dmy[0])) {
//                                List<CustomEvent> colorLst1 = new ArrayList<>();
//                                colorLst1.add(new CustomEvent(R.color.sick));
//                                return colorLst1;
//                            }
//                        }
//                    }
//                    return null;
//                }
//
//            });
//            calendarView.setDisableAutoDateSelection(true);
//            calendarView.refresh();
        }
    }

    public int min(int a, int b, int c) {
        if (a > b && a > c) {
            return a;
        }
        if (b > a && b > c) {
            return b;
        }
        if (c > a && c > b) {
            return c;
        }
        return a;
    }

    class CustomEvent implements Event {
        private int color;

        public CustomEvent(int color) {
            this.color = color;
        }

        @Override
        public int getColor() {
            return color;
        }
    }
}

