package com.handysolver.smartclass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.handysolver.smartclass.adapters.AssignmentAdapter;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.AssignmentModel;
import com.handysolver.smartclass.services.AutoLogout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Assignment extends AppCompatActivity {
    /*Assignment models is the container class for assignments. It is used to define and access all data related to individual assignment*/
    private ArrayList<AssignmentModel> assignmentModels = new ArrayList<>();
    BarChart marks_chart = null;
    final List<BarEntry> entries = new ArrayList<>();
    final List<IBarDataSet> dataSets = new ArrayList<>();
    SwipeRefreshLayout refreshLayout;
    private HashMap<Integer, String> marks_names = new HashMap<>();
    SharedPreferences sharedPreferences;
//    private HashMap<Integer,String> avg_marks_names=new HashMap<>();
//    private HashMap<Integer,String> highest_marks_names=new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        marks_chart = (BarChart) findViewById(R.id.marks_chart);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresher_layout);
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        refreshLayout.setColorSchemeResources(R.color.sick, R.color.present, R.color.absent, R.color.colorPrimary, R.color.colorPrimaryDark);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                marks_chart.zoom(0, 0, 0, 0);
                marks_names.clear();
//                avg_marks_names.clear();
//                highest_marks_names.clear();
                assignmentModels.clear();
                dataSets.clear();
                sendRequest();
            }
        });
        sendRequest();
    }

    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendRequest() {
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.ASSIGNMENT_DETAIL_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONArray response2 = null;
                        try {
                            response2 = new JSONArray(response1);
                            float k = 0.0f;
                            for (int i = 0; i < response2.length(); i++) {
                                String marks = "";
                                String max_marks = "";
                                int highest_marks = 0;
                                int average_marks = 0;
                                String[] links = new String[2];
                                links[0] = "";
                                links[1] = "";
                                JSONObject response = response2.getJSONObject(i);
//                                String start_time = response.getString("time_from");
//                                start_time = start_time.substring(0, 5);
//                                String end_time = response.getString("time_to");
//                                end_time = end_time.substring(0, 5);
//                                String exam_time = start_time + "-" + end_time;
                                if (response.has("marks")) {
                                    marks = response.getString("marks");
                                    max_marks = response.getString("max_mark");
                                    final List<BarEntry> entries = new ArrayList<>();
//                                    final List<BarEntry> highest_marks_entries = new ArrayList<>();
//                                    final List<BarEntry> average_marks_entries = new ArrayList<>();
                                    int _marks = response.getInt("marks");
//                            String max_marks = response.getString("max_marks");
                                    String subject = response.getString("subject");
//                                    marks_chart.zoomIn();
                                    marks_names.put(response.getInt("marks"), subject);
                                    highest_marks = response.getInt("highest_mark");
                                    average_marks = response.getInt("avg");
                                    entries.add(new BarEntry(k, Float.valueOf(_marks)));
//                                    highest_marks_entries.add(new BarEntry(k+3.0f,response.getInt("highest_mark")));
//                                    average_marks_entries.add(new BarEntry(k+4.5f,response.getInt("avg")));
//                                    highest_marks_names.put(response.getInt("highest_mark"),subject);
//                                    avg_marks_names.put(response.getInt("avg"),subject);
                                    BarDataSet marks_set = new BarDataSet(entries, "Your score");
//                                    BarDataSet highest_marks_set=new BarDataSet(highest_marks_entries,"Class Highest");
//                                    BarDataSet average_marks_set=new BarDataSet(average_marks_entries,"Class Average");
                                    marks_set.setColor(Color.parseColor(GlobalConstants.presentColor));
//                                    highest_marks_set.setColor(Color.parseColor(GlobalConstants.presentColor));
//                                    average_marks_set.setColor(Color.parseColor(GlobalConstants.sickColor));
                                    dataSets.add(marks_set);
//                                    dataSets.add(highest_marks_set);
//                                    dataSets.add(average_marks_set);
                                    k += 0.35f;
                                }
                                if (response.getInt("attachment_count") > 0) {
                                    JSONArray attachments = response.getJSONArray("attachment");
                                    for (int j = 0; j < attachments.length(); j++) {
                                        JSONObject attachment = attachments.getJSONObject(j);
                                        String name = attachment.getString("name");
                                        String[] split_name = name.split("\\.");
                                        links[0] = links[0].concat(name + " ");
                                        links[1] = links[1].concat(attachment.getString(split_name[split_name.length - 1]) + " ");
                                    }
                                }
                                AssignmentModel assignmentModel = new AssignmentModel(marks + "/" + max_marks, response.getString("deadline"), response.getString("subject"), response.getString("course_code"), response.getString("description"), links,response.getInt("grade_template_id"));
                                assignmentModel.setAverage_marks(average_marks);
                                assignmentModel.setHighest_marks(highest_marks);
                                if(response.has("comments")) {
                                    assignmentModel.setComments(response.getString("comments"));
                                }
                                else
                                {
                                    assignmentModel.setComments("");
                                }
                                if(response.has("grade"))  {
                                    assignmentModel.setGrade(response.getString("grade"));
                                }
                                else
                                {
                                    assignmentModel.setGrade("");
                                }
                                assignmentModel.setDeadline(response.getString("deadline_time"));
                                assignmentModels.add(assignmentModel);

                            }
                            XAxis xAxis = marks_chart.getXAxis();
                            IAxisValueFormatter formatter = new IAxisValueFormatter() {
                                @Override
                                public String getFormattedValue(float value, AxisBase axis) {
                                    return "";
                                }
                            };
                            xAxis.setValueFormatter(formatter);
                            xAxis.setDrawGridLines(false);
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//                            xAxis.setGranularity(1f);
                            marks_chart.setDrawGridBackground(false);
                            marks_chart.getAxisLeft().setDrawGridLines(false);
                            marks_chart.getAxisRight().setEnabled(false);
                            BarData barData = new BarData(dataSets);
                            barData.setValueFormatter(new IValueFormatter() {
                                @Override
                                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                                    String res = marks_names.get(Math.round(value)) + "\n" + value;
//                                    if(dataSetIndex%3==1)
//                                    {
//                                        res= highest_marks_names.get(Math.round(value))+"\n"+value;
//                                    }
//                                    if(dataSetIndex%3==2)
//                                    {
//                                        res= avg_marks_names.get(Math.round(value))+"\n"+value;
//                                    }
                                    return res;
                                }
                            });
                            barData.setBarWidth(0.2f);
                            marks_chart.setData(barData);
                            marks_chart.setDrawValueAboveBar(true);
                            Description description = new Description();
                            description.setText("");
                            marks_chart.setDescription(description);
                            marks_chart.setDescription(description);
                            marks_chart.setDrawValueAboveBar(true);
                            LegendEntry entry = new LegendEntry();
//                            if(dataSets.size()>3)
//                                marks_chart.zoom(2,0,0,0);
//                            else
//                                marks_chart.zoom(-1,0,0,0);
//                            marks_chart.setFitBars(true);
                            entry.formColor = Color.parseColor(GlobalConstants.presentColor);
                            entry.label = "Your Score - marks in percent";
                            marks_chart.invalidate();
//                            LegendEntry entry_highest = new LegendEntry();
//                            entry_highest.formColor = Color.parseColor(GlobalConstants.presentColor);
//                            entry_highest.label = "Class Highest";
//                            LegendEntry entry_average = new LegendEntry();
//                            entry_average.formColor = Color.parseColor(GlobalConstants.sickColor);
//                            entry_average.label = "Class Average";
                            ArrayList<LegendEntry> entries = new ArrayList<>();
                            entries.add(entry);
//                            entries.add(entry_highest);
//                            entries.add(entry_average);
                            if (marks_names.size() > 0) {
                                Legend legend = marks_chart.getLegend();
                                legend.setCustom(entries);
                            }
                            RecyclerView test_list = (RecyclerView) findViewById(R.id.test_recycler_view);
                            test_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            test_list.setAdapter(new AssignmentAdapter(assignmentModels, getApplicationContext()));
                            refreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse.data != null) {
                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                            try {
                                JSONObject error_json = new JSONObject(error_message);
                                if (error_json.has("status")) {
                                    if (error_json.getInt("status") == 444) {
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        refreshLayout.setRefreshing(false);
                                    }
                                    if (error_json.getInt("status") == 498) {
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        refreshLayout.setRefreshing(false);
                                    }
                                    if (error_json.getInt("status") == 445) {
                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
                                        TextView error_view = (TextView) findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.apply();
                                        Intent intent = new Intent(Assignment.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            refreshLayout.setRefreshing(false);
                            TextView error_view = (TextView) findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText("Error");
                        }

                        error.printStackTrace();
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String role = sharedPreferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + sharedPreferences.getInt(GlobalConstants.parent_selected_student, 0);
                    } else {
                        body = "id=" + Integer.valueOf(sharedPreferences.getString("id", null));
                    }
                }
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(postRequest);
    }
}
