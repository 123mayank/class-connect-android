package com.handysolver.smartclass;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ListMenuItemView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.services.AutoLogout;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ComparisonGraphDialog extends AppCompatActivity {
    private String subject_name,date,code;
    private float marks,average_marks,highest_marks;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparison_graph_dialog);
        Bundle bundle=getIntent().getExtras();
        sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
        if(bundle!=null)
        {
            subject_name=bundle.getString("subject_name");
            code=bundle.getString("subject_code");
            date=bundle.getString("date");
            setTitle(code);
            marks=bundle.getFloat("marks");
            highest_marks=bundle.getFloat("highest_marks");
            average_marks=bundle.getFloat("average_marks");
            /* The Bar Chart Object initialized from the view */
            BarChart barChart=(BarChart)findViewById(R.id.comparison_bar_chart);
            /* An array list of bar data sets objects */
            List<IBarDataSet> barDataSets=new ArrayList<>();
            /* A BarEntry ArrayList for each colored bar */
            ArrayList<BarEntry>marks_entry=new ArrayList<>();
            ArrayList<BarEntry>highest_marks_entry=new ArrayList<>();
            ArrayList<BarEntry>average_marks_entry=new ArrayList<>();
            /* We know beforehand that this view will always have 3 colored bars
              * Hence, a single entry is made in each of the 3 array lists */
            marks_entry.add(new BarEntry(1.5f,marks));
            highest_marks_entry.add(new BarEntry(3.0f,highest_marks));
            average_marks_entry.add(new BarEntry(4.5f,average_marks));
            /* Creating BarDataSet object from each BarEntry ArrayList
            * and setting the color of each BarDataSet object */
            BarDataSet marks_set=new BarDataSet(marks_entry,"Your Marks");
            BarDataSet highest_marks_set=new BarDataSet(highest_marks_entry,"Highest Marks");
            BarDataSet average_marks_set=new BarDataSet(average_marks_entry,"Class Average");
            marks_set.setColor(Color.parseColor(GlobalConstants.presentColor));
            highest_marks_set.setColor(Color.parseColor(GlobalConstants.absentColor));
            average_marks_set.setColor(Color.parseColor(GlobalConstants.sickColor));
            XAxis xAxis = barChart.getXAxis();
            /* Formatting the value of XAxis so that nothing is diplayed on it */
            IAxisValueFormatter formatter = new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    return "";
                }
            };
            xAxis.setValueFormatter(formatter);
            /* The default bar graph view shows grid lines and XAxis above the YAxis we are fixing this below */
            xAxis.setDrawGridLines(false);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            /* appending the BarDataSet object to the BarDataSet ArrayList */
            barDataSets.add(marks_set);
            barDataSets.add(highest_marks_set);
            barDataSets.add(average_marks_set);
            barChart.setDrawGridBackground(false);
            /* The default bar graph view shows grid lines and 2 YAxis we are fixing this below */
            barChart.getAxisLeft().setDrawGridLines(false);
            barChart.getAxisRight().setEnabled(false);
            /* creating BarData object from the BarDataSets array list */
            BarData barData=new BarData(barDataSets);
            /* setting bar width */
            barData.setBarWidth(0.5f);
            /* assigning BarData object to the BarChart instance */
            barChart.setData(barData);
            barChart.setDrawValueAboveBar(true);
            Description description = new Description();
            description.setText("");
            barChart.setDescription(description);
            barChart.setDescription(description);
            barChart.setDrawValueAboveBar(true);
            /* The legend of the BarGraph */
            LegendEntry entry = new LegendEntry();
            entry.formColor = Color.parseColor(GlobalConstants.presentColor);
            entry.label = "Your Score";
            barChart.invalidate();
            LegendEntry entry_highest = new LegendEntry();
            entry_highest.formColor = Color.parseColor(GlobalConstants.absentColor);
            entry_highest.label = "Class Highest";
            LegendEntry entry_average = new LegendEntry();
            entry_average.formColor = Color.parseColor(GlobalConstants.sickColor);
            entry_average.label = "Class Average";
            ArrayList<LegendEntry> entries = new ArrayList<>();
            entries.add(entry);
            entries.add(entry_highest);
            entries.add(entry_average);
        }
        TextView subjectName = (TextView)findViewById(R.id.subject_name);
        subjectName.setText(subject_name);
        TextView subject_date = (TextView)findViewById(R.id.date);
        subject_date.setText(date);
        Button button=(Button)findViewById(R.id.done_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        AutoLogout.timer.cancel();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (sharedPreferences.getString("role", "").equals(GlobalConstants.teacher_role) || sharedPreferences.getString("role", "").equals(GlobalConstants.admin_role)) {
            AutoLogout.timer.cancel();
            AutoLogout.timer.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferences.getString("role","").equals(GlobalConstants.teacher_role)||sharedPreferences.getString("role","").equals(GlobalConstants.admin_role))
        {
            try {
                AutoLogout.timer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }}
    }
}
