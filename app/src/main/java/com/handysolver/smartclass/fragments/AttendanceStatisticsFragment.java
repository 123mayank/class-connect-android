package com.handysolver.smartclass.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.handysolver.smartclass.AttendanceTabs;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
//// * {} interface
// * to handle interaction events.
// * Use the {@link AttendanceStatisticsFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AttendanceStatisticsFragment extends Fragment {
    PieChart pieChart;
    RelativeLayout textdays;
    static TextView to_date, from_date;
    private OnFragmentInteractionListener mListener;
    private DatePickerDialogFragment datePickerDialogFragment;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public AttendanceStatisticsFragment() {
        // Required empty public constructor
    }

    //    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
//     * @return A new instance of fragment AttendanceStatisticsFragment.
//     */
//    // TODO: Rename and change types and number of parameters
    public static AttendanceStatisticsFragment newInstance(String param1, String param2) {
        AttendanceStatisticsFragment fragment = new AttendanceStatisticsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        datePickerDialogFragment = new DatePickerDialogFragment();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attendance_statistics, container, false);
        from_date = (TextView) view.findViewById(R.id.start_date);
        to_date = (TextView) view.findViewById(R.id.end_date);
        textdays=(RelativeLayout) view.findViewById(R.id.overview_in_text);
        final TextView wrong_dates = (TextView) view.findViewById(R.id.wrong_date_warning);
        pieChart = (PieChart) view.findViewById(R.id.pieChart);
        from_date.setText(AttendanceTabs.default_start_date);
        to_date.setText(AttendanceTabs.default_last_date);
        sendRequest();
        Button request_button = (Button) view.findViewById(R.id.send_request);
        request_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (AttendanceTabs.format.parse(from_date.getText().toString()).compareTo(AttendanceTabs.format.parse(to_date.getText().toString())) <= 0) {
                        mListener.test();
                        wrong_dates.setVisibility(View.GONE);
                        sendRequest();
                    } else {
                        wrong_dates.setVisibility(View.VISIBLE);
                        wrong_dates.setText(R.string.wrong_date);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                datePickerDialogFragment.setFlag(DatePickerDialogFragment.FLAG_START_DATE);
                datePickerDialogFragment.show(getFragmentManager(), "Select start date");
            }
        });
        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialogFragment.setFlag(DatePickerDialogFragment.FLAG_END_DATE);
                datePickerDialogFragment.show(getFragmentManager(), "Select end date");
            }
        });

        // Inflate the layout for this fragment
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AttendanceStatisticsFragment.OnFragmentInteractionListener) {
            mListener = (AttendanceStatisticsFragment.OnFragmentInteractionListener) context;
        }
    }

    public void sendRequest() {
        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.refresher_layout);
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(true);
        }
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.ATTENDANCE_COUNT_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONObject response = null;
                        try {
                            response = new JSONObject(response1);
                            int total = response.getInt("total");
//                    int holiday=response.getInt("holiday");
                            int absent = response.getInt("absent");
                            int sick = response.getInt("sick");
                            int present = total - (absent + sick);
                            textdays.setVisibility(View.VISIBLE);
                            TextView present_days = (TextView) getView().findViewById(R.id.days_present);
                            TextView absent_days = (TextView) getView().findViewById(R.id.days_absent);
                            TextView sick_days = (TextView) getView().findViewById(R.id.days_sick);
                            present_days.setText(" " + present);
                            absent_days.setText(" " + absent);
                            sick_days.setText(" " + sick);


//                    pieChart.setCenterText("Overview");
                            List<PieEntry> entries = new ArrayList<>();
                            List<Integer> colors = new ArrayList<>();
//                    entries.add(new PieEntry((float)holiday,"Holiday"));
                            if (present > 0 && sick > 0 || absent > 0 && sick > 0 || present > 0 && absent > 0) {
                                entries.add(new PieEntry(present, "Present"));
                                entries.add(new PieEntry(sick, "Sick"));
                                entries.add(new PieEntry(absent, "Absent"));
//                    colors.add(Color.parseColor("#00FFFF"));
                                colors.add(Color.parseColor(GlobalConstants.presentColor));
                                colors.add(Color.parseColor(GlobalConstants.sickColor));
                                colors.add(Color.parseColor(GlobalConstants.absentColor));
                                PieDataSet set = new PieDataSet(entries, "");
                                set.setColors(colors);
//                    pieChart.setUsePercentValues(true);
                                PieData data = new PieData(set);
                                data.setValueFormatter(new IValueFormatter() {
                                    @Override
                                    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                                        DecimalFormat myFormat = new DecimalFormat("###");
                                        return myFormat.format(value);
                                    }
                                });

//                            data.setHighlightEnabled(true);
                                data.setValueTypeface(Typeface.DEFAULT_BOLD);
                                data.setValueTextSize(14f);
                                pieChart.setData(data);
                                Description description = new Description();
                                description.setText("");
                                pieChart.setDescription(description);
                                pieChart.setPadding(15, 0, 0, 0);
                                pieChart.animateX(2000, Easing.EasingOption.EaseInOutElastic);
//                    pieChart.animateXY(2000,2000, Easing.EasingOption.EaseInCubic, Easing.EasingOption.EaseInCubic);
                                pieChart.invalidate();
                                Legend l = pieChart.getLegend();
                                l.setWordWrapEnabled(true);
                                l.setForm(Legend.LegendForm.CIRCLE); // set what type of form/shape should be used
                                l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
                                l.setTextColor(Color.BLACK);
                                l.setXOffset(0);
                                l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                                l.setTextSize(15f);
                                l.setXEntrySpace(25f); // set the space between the legend entries on the x-axis
                            } else {
                                pieChart.clear();
                                pieChart.invalidate();

                            }
                            refreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        if(error.networkResponse.data!=null)
                        {
                            String error_message = new String(error.networkResponse.data, Charset.forName("utf-8"));
                            try {
                                JSONObject error_json = new JSONObject(error_message);
                                if (error_json.has("status")) {
                                    if (error_json.getInt("status") == 444) {
                                        textdays.setVisibility(View.GONE);
                                        pieChart.clear();
                                        pieChart.setNoDataText((error_json.getString("message")));
                                        refreshLayout.setRefreshing(false);
                                    }
                                    if (error_json.getInt("status") == 498) {
                                        ScrollView scrollView=(ScrollView) getView().findViewById(R.id.main_view);
                                        scrollView.setVisibility(View.GONE);
                                        TextView error_view = (TextView) getView().findViewById(R.id.error_view);
                                        error_view.setVisibility(View.VISIBLE);
                                        error_view.setText(error_json.getString("message"));
                                        refreshLayout.setRefreshing(false);
                                    }
//                                    if (error_json.getInt("status") == 445) {
//                            /*When the user_id no longer exists in the database. This might happen when the user or his/her entire class is removed from the backend. In this case, we will remove all user data and log user out*/
//                                        TextView error_view = (TextView) getView().findViewById(R.id.error_view);
//                                        error_view.setVisibility(View.VISIBLE);
//                                        error_view.setText(error_json.getString("message"));
//                                        SharedPreferences sharedPreferences = getSharedPreferences("Credentials", MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                                        editor.clear();
//                                        editor.apply();
//                                        Intent intent = new Intent(Assignment.this, LoginActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        startActivity(intent);
//                                        finish();
//                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }}
                        else
                        {
                            ScrollView scrollView=(ScrollView) getView().findViewById(R.id.main_view);
                            scrollView.setVisibility(View.GONE);
                            refreshLayout.setRefreshing(false);
                            TextView error_view = (TextView) getView().findViewById(R.id.error_view);
                            error_view.setVisibility(View.VISIBLE);
                            error_view.setText("Error");
                        }

                        error.printStackTrace();
                    }
                }) {


            @Override
            public byte[] getBody() throws AuthFailureError {
                SharedPreferences preferences = getContext().getSharedPreferences(GlobalConstants.user_details_file_name, Context.MODE_PRIVATE);
                String role = preferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + preferences.getInt(GlobalConstants.parent_selected_student, 0) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
                    } else {
                        body = "id=" + Integer.valueOf(getContext().getSharedPreferences(GlobalConstants.user_details_file_name, Context.MODE_PRIVATE).getString("id", null)) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
                    }
                }
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(postRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

//    /**
//     * This interface must be implemented by activities that contain this
//     * fragment to allow an interaction in this fragment to be communicated
//     * to the activity and potentially other fragments contained in that
//     * activity.
//     * <p/>
//     * See the Android Training lesson <a href=
//     * "http://developer.android.com/training/basics/fragments/communicating.html"
//     * >Communicating with Other Fragments</a> for more information.
//     */


    public static class DatePickerDialogFragment extends android.support.v4.app.DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        public static final int FLAG_START_DATE = 0;
        public static final int FLAG_END_DATE = 1;

        private int flag = 0;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void setFlag(int i) {
            flag = i;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            if (flag == FLAG_START_DATE) {
                AttendanceTabs.default_start_date = AttendanceTabs.format.format(calendar.getTime());
                from_date.setText(AttendanceTabs.format.format(calendar.getTime()));
            } else if (flag == FLAG_END_DATE) {
                AttendanceTabs.default_last_date = AttendanceTabs.format.format(calendar.getTime());
                to_date.setText(AttendanceTabs.format.format(calendar.getTime()));
            }
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void test();
    }

}
