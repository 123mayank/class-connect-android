package com.handysolver.smartclass.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.AttendanceTabs;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link AttendanceDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AttendanceDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    ImageView left, right;
    CustomCalendarView calendarView;
    TextView monthTextView;
    private JSONArray present;
    private JSONArray absent;
    private JSONArray sick;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String start_date;
    private String end_date;

    private OnFragmentInteractionListener mListener;

    public AttendanceDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AttendanceDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AttendanceDetailsFragment newInstance(String param1, String param2) {
        AttendanceDetailsFragment fragment = new AttendanceDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

//        RequestQueue queue = Volley.newRequestQueue(getContext());
//        JsonObjectRequest fetch = new JsonObjectRequest(Request.Method.GET, "https://jsonblob.com/api/jsonBlob/57fb7face4b0bcac9f7cb9fb", null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                try
//                {
//                    final ArrayList<Integer> presentList=new ArrayList<Integer>();
//                    final ArrayList<Integer>absentList=new ArrayList<Integer>();
//                    final ArrayList<Integer>sickList=new ArrayList<Integer>();
//                    present=response.getJSONArray("Present");
//                    absent=response.getJSONArray("Absent");
//                    sick=response.getJSONArray("Sick");
//                    for(int i=0;i<present.length();i++)
//                    {
//                        try {
//                            presentList.add(present.getInt(i));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    for(int i=0;i<absent.length();i++)
//                    {
//                        try {
//                            absentList.add(absent.getInt(i));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }for(int i=0;i<sick.length();i++)
//                {
//                    try {
//                        sickList.add(sick.getInt(i));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                    setListener(presentList,absentList,sickList);
//
//
//                } catch (JSONException e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        });
//        queue.add(fetch);
//                try
//                {
//                    final ArrayList<Integer> presentList=new ArrayList<Integer>();
//                    final ArrayList<Integer>absentList=new ArrayList<Integer>();
//                    final ArrayList<Integer>sickList=new ArrayList<Integer>();
//                    present=response.getJSONArray("Present");
//                    absent=response.getJSONArray("Absent");
//                    sick=response.getJSONArray("Sick");
//                    for(int i=0;i<present.length();i++)
//                    {
//                        try {
//                            presentList.add(present.getInt(i));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    for(int i=0;i<absent.length();i++)
//                    {
//                        try {
//                            absentList.add(absent.getInt(i));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }for(int i=0;i<sick.length();i++)
//                {
//                    try {
//                        sickList.add(sick.getInt(i));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                    setListener(presentList,absentList,sickList);
//
//
//                } catch (JSONException e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        });
//        queue.add(fetch);


    }

    public void setListener(final ArrayList<String> presentList, final ArrayList<String> absentList, final ArrayList<String> sickList) {
        final int size = min(presentList.size(), absentList.size(), sickList.size());
        if (calendarView != null) {
            ArrayList decorators = new ArrayList();
            DayDecorator decorator = new DayDecorator() {
                @Override
                public void decorate(DayView cell) {
                    for (int i = 0; i < size; i++) {
                        if (presentList.contains(AttendanceTabs.format.format(cell.getDate()))) {
                            cell.setBackgroundResource(R.drawable.selected_date_shape);
                            ((GradientDrawable)cell.getBackground()).setColor(getResources().getColor(R.color.present));
                        }
                        if (absentList.contains(AttendanceTabs.format.format(cell.getDate()))) {
                            cell.setBackgroundResource(R.drawable.selected_date_shape);
                            ((GradientDrawable)cell.getBackground()).setColor(getResources().getColor(R.color.absent));
                        }
                        if (sickList.contains(AttendanceTabs.format.format(cell.getDate()))) {
                            cell.setBackgroundResource(R.drawable.selected_date_shape);
                            ((GradientDrawable)cell.getBackground()).setColor(getResources().getColor(R.color.sick));
                        }
                    }
                }
            };
            decorators.add(decorator);
            calendarView.setDecorators(decorators);
            Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

//Show Monday as first date of week
            calendarView.setFirstDayOfWeek(Calendar.MONDAY);

//Show/hide overflow days of a month
            calendarView.setShowOverflowDate(false);

//call refreshCalendar to update calendar the view
            calendarView.refreshCalendar(currentCalendar);
//            calendarView.setDisableAutoDateSelection(true);
//            calendarView.setCalendarView(new FlexibleCalendarView.CalendarView() {
//                @Override
//                public BaseCellView getCellView(int position, View convertView, ViewGroup parent, int cellType) {
//                    BaseCellView cellView = (BaseCellView) convertView;
//                    cellView.addState(6);
//                    cellView.addState(7);
//                    cellView.addState(8);
//                    if (cellView == null) {
//                        LayoutInflater inflater = LayoutInflater.from(getContext());
//                        cellView = (BaseCellView) inflater.inflate(R.layout.square_cell_layout, null);
//                    }
//                    if(cellView.getMeasuredState()==6)
//                    {
//                        Log.d(TAG, "getCellView: asd");
//                    }
//
//
//
////                    if (cellType == BaseCellView.TODAY) {
//////                        cellView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
////                    }
////                    if (cellType == BaseCellView.SELECTED) {
//////                        cellView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
////                    }
//                    return cellView;
//                }
//
//                @Override
//                public BaseCellView getWeekdayCellView(int position, View convertView, ViewGroup parent) {
//                    return null;
//                }
//
//                @Override
//                public String getDayOfWeekDisplayValue(int dayOfWeek, String defaultValue) {
//                    return null;
//                }
//            });
//            Calendar cal = Calendar.getInstance();
//            cal.set(calendarView.getSelectedDateItem().getYear(), calendarView.getSelectedDateItem().getMonth(), 1);
//            monthTextView.setText(cal.getDisplayName(Calendar.MONTH,
//                    Calendar.LONG, Locale.ENGLISH) + " " + calendarView.getSelectedDateItem().getYear());
//            calendarView.selectDate(cal);
//            left.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    calendarView.moveToPreviousMonth();
//                }
//            });
//            right.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    calendarView.moveToNextMonth();
//                }
//            });
//            calendarView.setOnMonthChangeListener(
//                    new FlexibleCalendarView.OnMonthChangeListener() {
//                        @Override
//                        public void onMonthChange(int year, int month, int direction) {
//                            Calendar cal = Calendar.getInstance();
//                            cal.set(year, month, 1);
//                            monthTextView.setText(cal.getDisplayName(Calendar.MONTH,
//                                    Calendar.LONG, Locale.ENGLISH) + " " + year);
//                        }
//                    });
//            calendarView.setEventDataProvider(new FlexibleCalendarView.EventDataProvider() {
//                @Override
//                public List<? extends Event> getEventsForTheDay(int year, int month, int day) {
//                    for (int i = 0; i < size; i++) {
//                        if (i < presentList.size()) {
//                            String[] dmy = presentList.get(i).split("-");
//                            if (year == Integer.valueOf(dmy[2]) && month == Integer.valueOf(dmy[1]) - 1 && day == Integer.valueOf(dmy[0])) {
//                                List<CustomEvent> colorLst1 = new ArrayList<>();
//                                colorLst1.add(new CustomEvent(R.color.present));
//                                return colorLst1;
//                            }
//                        }
//                        if (i < absentList.size()) {
//                            String[] dmy = absentList.get(i).split("-");
//                            if (year == Integer.valueOf(dmy[2]) && month == Integer.valueOf(dmy[1]) - 1 && day == Integer.valueOf(dmy[0])) {
//                                List<CustomEvent> colorLst1 = new ArrayList<>();
//                                colorLst1.add(new CustomEvent(R.color.absent));
//                                return colorLst1;
//                            }
//                        }
//                        if (i < sickList.size()) {
//                            String[] dmy = sickList.get(i).split("-");
//                            if (year == Integer.valueOf(dmy[2]) && month == Integer.valueOf(dmy[1]) - 1 && day == Integer.valueOf(dmy[0])) {
//                                List<CustomEvent> colorLst1 = new ArrayList<>();
//                                colorLst1.add(new CustomEvent(R.color.sick));
//                                return colorLst1;
//                            }
//                        }
//                    }
//                    return null;
//                }
//
//            });
//            calendarView.setDisableAutoDateSelection(true);
//            calendarView.refresh();
//        Legend legend=new Legend();
//        int[]colors={Color.parseColor(GlobalConstants.presentColor),Color.parseColor(GlobalConstants.sickColor),Color.parseColor(GlobalConstants.absentColor)};
//        String[]labels={"Present","Sick","Absent"};
//        legend.setCustom(colors,labels);
//        legend.setForm(Legend.LegendForm.CIRCLE);
//        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
//        legend.setTextSize(10f);
//        legend.setXEntrySpace(5f); // set the space between the legendegend entries on the x-axis
//        legend.setYEntrySpace(5f);
        }
    }

    public int min(int a, int b, int c) {
        if (a > b && a > c) {
            return a;
        }
        if (b > a && b > c) {
            return b;
        }
        if (c > a && c > b) {
            return c;
        }
        return a;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendance_details, container, false);
//        left = (ImageView) view.findViewById(R.id.left_arrow);
//        right = (ImageView) view.findViewById(R.id.right_arrow);
//        monthTextView = (TextView) view.findViewById(R.id.month_text_view);
        calendarView = (CustomCalendarView) view.findViewById(R.id.calendar);
        sendRequest(this.getContext());
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void sendRequest(final Context context) {
        StringRequest postRequest = new StringRequest(Request.Method.POST,GlobalConstants.BASE_BACKEND_URL+GlobalConstants.ATTENDANCE_DETAIL_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        JSONObject response = null;
                        try {
                            response = new JSONObject(response1);
                            final ArrayList<String> presentList = new ArrayList<String>();
                            final ArrayList<String> absentList = new ArrayList<String>();
                            final ArrayList<String> sickList = new ArrayList<String>();
                            present = response.getJSONArray("present");
                            absent = response.getJSONArray("absent");
                            sick = response.getJSONArray("sick");
                            for (int i = 0; i < present.length(); i++) {
                                try {
                                    presentList.add(present.getString(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            for (int i = 0; i < absent.length(); i++) {
                                try {
                                    absentList.add(absent.getString(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            for (int i = 0; i < sick.length(); i++) {
                                try {
                                    sickList.add(sick.getString(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            setListener(presentList, absentList, sickList);


                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        error.printStackTrace();
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                SharedPreferences preferences = context.getSharedPreferences(GlobalConstants.user_details_file_name, context.MODE_PRIVATE);
                String role = preferences.getString("role", null);
                String body = "";
                if (role != null) {
                    if (role.equals(GlobalConstants.parent_role)) {
                        body = "id=" + preferences.getInt(GlobalConstants.parent_selected_student, 0) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
                    } else {
                        body = "id=" + Integer.valueOf(context.getSharedPreferences(GlobalConstants.user_details_file_name, context.MODE_PRIVATE).getString("id", null)) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
                    }
                }
                return body.getBytes();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(postRequest);
    }
}

//class CustomEvent implements Event {
//    private int color;
//
//    public CustomEvent(int color) {
//        this.color = color;
//    }
//
//    @Override
//    public int getColor() {
//        return color;
//    }
//}
