package com.handysolver.smartclass.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.handysolver.smartclass.R;
import com.handysolver.smartclass.adapters.HolidayAdapter;
import com.handysolver.smartclass.constants.GlobalConstants;
import com.handysolver.smartclass.models.HolidayModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HolidayFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HolidayFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView holiday_list;
    SwipeRefreshLayout refreshLayout;

    public HolidayFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HolidayFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HolidayFragment newInstance(String param1, String param2) {
        HolidayFragment fragment = new HolidayFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_holiday,container,false);
        holiday_list=(RecyclerView)view.findViewById(R.id.holiday_recycler);
        refreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.refresher_layout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /*Note that we have to clear the array lists here else they still contain the classes that were added to them before refreshing*/
                sendRequest();
            }
        });
        sendRequest();
        return view;
    }

    public void sendRequest() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, GlobalConstants.BASE_BACKEND_URL+GlobalConstants.HOLIDAY_CONTROLLER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        ArrayList<HolidayModel>holidayModels=new ArrayList<>();
                        JSONObject response = null;
                        JSONArray response2=null;
                        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MMM-yyyy");
                        String date=dateFormat.format(Calendar.getInstance().getTime());
                        int k=0;
                        try {
                            response2=new JSONArray(response1);
                            for(int i=0;i<response2.length();i++)
                            {
                                response=response2.getJSONObject(i);
                                String holiday_date=response.getString("date");
                                if(date!=null)
                                {
                                    if(holiday_date.compareTo(date)<1)
                                    {
                                        k++;
                                    }
                                }
                                holidayModels.add(new HolidayModel(response.getString("name"),response.getString("date")));
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                        holiday_list.setAdapter(new HolidayAdapter(getContext(),holidayModels));
                        holiday_list.setLayoutManager(new LinearLayoutManager(getContext()));
                        refreshLayout.setRefreshing(false);
//                        holiday_list.getLayoutManager().scrollToPosition(k);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //   Handle Error
                        error.printStackTrace();
                    }
                }) {

/*as holidays are same for all, we do not need to send the user id*/
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                String body = "id=" + Integer.valueOf(getActivity().getSharedPreferences(GlobalConstants.user_details_file_name, Context.MODE_PRIVATE).getString("id", null)) + "&from_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_start_date) + "&to_date=" + AttendanceTabs.return_api_format_date(AttendanceTabs.default_last_date);
//                return body.getBytes();
//            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(postRequest);
    }

}
